﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;
using System.Data.SqlTypes;

namespace MSServerTestConnection
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = @"Data Source=PC36-6-Z;Initial Catalog=db28pr6;Integrated Security=True";
            //1.Create connection
            //3.from connection take command or select
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();//!!!!!!!!!
                using (SqlCommand command = new SqlCommand("SELECT * FROM [db28pr6].[dbo].[CUSTOMERS]", con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int v1 = reader.GetInt32(0);
                        string v2 = reader["NICK_NAME"].ToString();
                        string v3 = reader.GetString(2);
                        //DateTime v4 = reader.GetDateTime(2);
                        Console.WriteLine("{0} {1} {2}", v1, v2, v3);
                    }
                }
            }
        }
    }
}
