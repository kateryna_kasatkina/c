﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleListTest1
{
    public class MyDouble//: /*IComparable<MyDouble>*/
    {
        private double _d;
        public double D
        {get{ return _d;} set{ _d = value;}}
        private MyDouble() { }
        public MyDouble(float v){_d = (double)v;}
        public MyDouble(double v){_d = v;}
        public override string ToString(){return (String.Format("{0}",_d));}

        public bool Equals(MyDouble other)
        {
            return _d == other._d;
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (ReferenceEquals(obj, this))
                return true;
            if (obj.GetType() != this.GetType())
                return false;
            MyDouble rhs = obj as MyDouble;
            return this._d == rhs._d;//returns true if both fields are equal

        }
        public static bool operator ==(MyDouble lv, MyDouble rv)
        {
            return object.Equals(lv, rv);
        }
        public static bool operator !=(MyDouble lv, MyDouble rv)
        {
            return !object.Equals(lv, rv);
        }
        public override int GetHashCode()
        {
            return _d.GetHashCode();
        }

        //public int CompareTo(MyDouble other)
        //{
        //    return this._d.CompareTo(other._d);
        //}

        public static bool operator <(MyDouble lv, MyDouble rv)
        {
            return lv._d<rv._d;
        }
        public static bool operator >(MyDouble lv, MyDouble rv)
        {
            return lv._d > rv._d;
        }
        
        public static MyDouble operator +(MyDouble b, MyDouble c)
        {
            return new MyDouble( b._d + b._d);
          
        }
        public static MyDouble operator -(MyDouble b, MyDouble c)
        {
            return new MyDouble(b._d - b._d);
        }
    }
}
