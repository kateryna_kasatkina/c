﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleListTest1
{
    class SimpleList : IList
    {
        private object[] _contents = new object[8];
        private int _count;
        public SimpleList()
        {
            _count = 0;
        }

        public void PrintContents()
        {
            Console.WriteLine("List has capacity of {0}" +
                "and currently has {1}(Count) elements.", _contents.Length, _count);
            Console.WriteLine("List contents:");
            for (int i = 0; i < Count; i++)
            {
                Console.Write(" {0}", _contents[i]);
            }
            Console.WriteLine();
        }
        public object this[int index]
        {
            get
            {
                return _contents[index];
            }

            set
            {
                _contents[index] = value;
            }
        }

        public int Count
        {
            get
            {
                return _count;
            }
        }

        public bool IsFixedSize
        {
            get
            {
                return true;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsSynchronized
        {
            get
            {
                return false;
            }
        }

        public object SyncRoot
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public int Add(object value)
        {
            if (_count < _contents.Length)
            {
                _contents[_count] = value;
                _count++;
                return (_count - 1);
            }
            else
            {
                return -1;//изменение размера и перезапись
                /*
                 если при добавлении елемента , оказывается, что массив полностью заполнен, будет создан новый массив размером
                 (n*3)/2+1*/
            }
        }

        public void Clear()
        {
            _count = 0;
        }

        public bool Contains(object value)
        {
            bool inList = false;
            for (int i = 0; i < Count; i++)
            {
                if (_contents[i] == value)
                {
                    inList = true;
                    break;
                }

            }
            return inList;
        }

        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }

        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public int IndexOf(object value)
        {
            int itemIndex = -1;
            for (int i = 0; i < Count; i++)
            {
                if (_contents[i] == value)
                {
                    itemIndex = i;
                    break;
                }
            }
            return itemIndex;
        }

        public void Insert(int index, object value)
        {
            throw new NotImplementedException();
        }

        public void Remove(object value)//delete by value
        {
            RemoveAt(IndexOf(value));
        }

        public void RemoveAt(int index)//delete by index
        {
            if ((index >= 0) && index < Count)
            {
                for (int i = index; i < Count - 1; i++)
                {
                    _contents[i] = _contents[i + 1];

                }
                _count--;
            }
        }
    }

    public class Book
    {
        public string Name { set; get; }
        public override string ToString()
        {
            return Name;
        }
    }
    public class Library:IEnumerable
    {
        private Book[] books;
        public Library()
        {
            books = new Book[]
            {
                new Book {Name="bookkkkkkkkkkkkkkkkk1" },
                new Book {Name="book1" },
                new Book {Name="book1" }

            };
        }

        //public override string ToString()
        //{
        //    return Console.WriteLine(String.Format(); ;
        //}
        public int Lenght
        {
            get { return books.Length; }
        }

        public Book this[int index]
        {
            get { return books[index]; }
            set { books[index] = value; }
        }

        #region implementation IEnumerable-for foreach
        public IEnumerator GetEnumerator()
        {
            return books.GetEnumerator();//returns implementation of our array
        }

        #endregion
    }



    public class FibonacciSequence:IEnumerator,IEnumerable
    {
        int[] fibonacci = new int[] { 1, 1, 2, 3, 5, 8 ,13,21};
        int index = -1;

        #region Implementation IEnumerator
        //no setter foreach only for reading
        
        public object Current
        {
            get
            {
                return fibonacci[index];
            }
        }

       

        public bool MoveNext()
        {
            if (index == fibonacci.Length - 1)
            {
                Reset();
                return false;//no next all were given
            }
            index++;
            return true;//next exists and current point to it 
        }

        public void Reset()
        {
            index = -1;
        }
        #endregion
        #region Implementation IEnumerable
        public IEnumerator GetEnumerator()
        {
            return this;
        }
        #endregion
    }
}
