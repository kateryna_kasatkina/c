﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Action a = () => { Console.WriteLine("Hello"); };
            Thread.Sleep(2000); 
            a();
        }
    }
}
