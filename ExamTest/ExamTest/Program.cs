﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamTest
{
    class Program
    {
        static void Main(string[] args)
        {

            Customers customers = new Customers();
            foreach(Customer c in customers)
            {
                Console.WriteLine(c);
            }

            Console.WriteLine("------------------------");
            customers.print(customers,c=>c.Name== "Petrov Ivan");
        }

        public class Customer
        {
            public string Name { get; set; }
            public string Mail { get; set; }
            //Customer(string name, string mail)
            //{
            //    this.Name = String.Empty;
            //    this.Mail = String.Empty;
            //}
            public override string ToString()
            {
                return String.Format($"Name: {this.Name} , mail:{this.Mail}");
            }
        }

        public class Customers:Customer,IEnumerable
        {
            Customer[] customers;
            public Customers()
            {
                customers = new Customer[]
                {
                    new Customer() {Name="Ivanov Petr",Mail="ivanov@gmai.com" },
                    new Customer() {Name="Petrov Ivan",Mail="petrov@gmai.com" },
                    new Customer() {Name="Sidorov Maks",Mail="sidorov@gmai.com" },
                };

            }

            public IEnumerator GetEnumerator()
            {
                return customers.GetEnumerator();
            }

            public void print(Customers[] list,Func<Customer,bool>op)
            {
                foreach (Customer c in list)
                {
                   if(op(c))
                        Console.WriteLine(c);
                }

                /* static void printArray(int[]array,Func<int,bool>op)
        {
            foreach(int i in array)
                if(op(i))
                Console.Write(i);
        }*/
            }
        }
    }
}
