﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Events
{
    class Program
    {
        //обьявляем делегат для события
        public delegate void MyEventHandler(int a);

        //обьявляем класс события
        public class MyEvent
        {
            public event MyEventHandler SomeEvent;

            //етот метод вызывается для генерации события
            public void OnSomeEvenr(int a)
            {
                if (SomeEvent != null)
                    SomeEvent(a);
            }
        }
            //обработчик сщбытия
            static void Handler(int a)
            {
                Console.WriteLine("Event Happend!"+a);
            }
      
        static void KateHandler(int a)
        {
            Console.WriteLine("Hello Kate!"+a);
        }

        static void Main(string[] args)
        {
            MyEvent evt = new MyEvent();

            //добавляем метод handler() в список события
            evt.SomeEvent += new MyEventHandler(KateHandler);
            evt.SomeEvent += new MyEventHandler(Handler);
            //генерируем событие
            evt.OnSomeEvenr(8);
        }
    }
}
