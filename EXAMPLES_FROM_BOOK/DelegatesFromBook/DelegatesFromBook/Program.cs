﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesFromBook
{
    class Program
    {
        delegate void strMod(ref string str);
        static void Main(string[] args)
        {
            //strMod strOp = new strMod(ReplaceSpacce);
            //string str;

            //str = strOp("this is simple text");
            //Console.WriteLine("Rusulting string: " + str);
            //Console.WriteLine();

            //strOp = new strMod(RemoveSpacces);
            //str = strOp("this is simple text");
            //Console.WriteLine("Rusulting string: " + str);
            //Console.WriteLine();

            //strOp = new strMod(Reverse);
            //str = strOp("this is simple text");
            //Console.WriteLine("Rusulting string: " + str);
            //Console.WriteLine();

            strMod strOp;
            StringOps MyObject = new StringOps();
            strMod replaceSp = new strMod(MyObject.ReplaceSpacce);
            strMod removeSp = new strMod(MyObject.RemoveSpacces);
            strMod reverseStr = new strMod(MyObject.Reverse);
            string str = "this is simple text";

            //multicasting многоадресная передача
            strOp = replaceSp;
            strOp += removeSp;
            strOp += reverseStr;
            //вызов делегата с многоадресной передачай
            strOp(ref str);
            Console.WriteLine(str);



            strOp-= reverseStr;
            Console.WriteLine("-----------------------------");
            str = "this is simple text";
            strOp(ref str);
            Console.WriteLine(str);
        }


        public class StringOps
        {


            public void ReplaceSpacce(ref string a)
            {
                Console.WriteLine("replace spaces on - ");
                 a.Replace(" ", "-");
            }

            public void RemoveSpacces(ref string a)
            {
                string temp = "";
                int i;
                Console.WriteLine("Deleting spaces: ");
                for (i = 0; i < a.Length; i++)
                    if (a[i] != ' ')
                        temp += a[i];
               a= temp;
            }

            public void Reverse(ref string a)
            {
                string temp = "";
                int i, j;
                Console.WriteLine("Revers of string: ");
                for (j = 0, i = a.Length - 1; i >= 0; i--, j++)
                    temp += a[i];
                a = temp;
            }
        }
    }
}
