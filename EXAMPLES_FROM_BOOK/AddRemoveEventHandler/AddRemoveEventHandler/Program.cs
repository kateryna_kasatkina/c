﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddRemoveEventHandler
{
    class Program
    {
        //Создание собственных средств управления списком событий

            //обьявляем делегат для события
            public  delegate void MyEventHandler();

        class MyEvent
        {
            MyEventHandler[] evnt = new MyEventHandler[3];

            public event MyEventHandler SomeEvent
            {
                add
                {
                    int i;
                    for(i=0;i<3;i++)
                    {
                        if(evnt[i]==null)
                        {
                            evnt[i] = value;
                            break;
                        }
                    }
                    if(i==3)
                        Console.WriteLine("Список обработчиков событий полон.");
                }
                remove
                {
                    int i;
                    for(i=0;i<3;i++)
                        if(evnt[i]==value)
                        {
                            evnt[i] = null;
                            break;
                        }
                    if(i==3)
                        Console.WriteLine("Обработчик события не найден.");
                }
            }

            //этот метод вызывается для генерирования событий
            public void OnSomeEvent()
            {
                for (int i = 0; i < 3; i++)
                    if (evnt[i] != null)
                        evnt[i]();
            }
        }


        //Создаем классы которые используют делегат MyEventHandler
        public class W
        {
            public void Whandler()
            {
                Console.WriteLine("Событие получено обьектом W.");
            }
        }
        public class X
        {
            public void Xhandler()
            {
                Console.WriteLine("Событие получено обьектом X.");
            }
        }

        public class Y
        {
            public void Yhandler()
            {
                Console.WriteLine("Событие получено обьектом Z.");
            }
        }
        public class Z
        {
            public void Zhandler()
            {
                Console.WriteLine("Событие получено обьектом X.");
            }
        }
        static void Main(string[] args)
        {
            MyEvent evt = new MyEvent();
            W wOb = new W();
            X xOb = new X();
            Y yOb = new Y();
            Z zOb = new Z();
            //добавляем обработчики в список
            Console.WriteLine("добавляем обработчики в список: ");
            evt.SomeEvent += new MyEventHandler(wOb.Whandler);
            evt.SomeEvent += new MyEventHandler(xOb.Xhandler);
            evt.SomeEvent += new MyEventHandler(yOb.Yhandler);

            //этот обработчик сохранить нельзя , список полон
            evt.SomeEvent += new MyEventHandler(zOb.Zhandler);
            Console.WriteLine();

            evt.OnSomeEvent();
            Console.WriteLine();

            //удаляем обработчик из списка
            Console.WriteLine("Удаляем обработчик xOb.Xhandler");
            evt.SomeEvent -= new MyEventHandler(xOb.Xhandler);
            evt.OnSomeEvent();

            //пытаемся удалить его еще раз
            Console.WriteLine("Попытка повторно удалить обработчик xOb.Xhandler:");
            evt.SomeEvent -= new MyEventHandler(xOb.Xhandler);
            evt.OnSomeEvent();
            Console.WriteLine();

            //теперь добавляем обработчик Zhandler
            evt.SomeEvent += new MyEventHandler(zOb.Zhandler);
            evt.OnSomeEvent();

        }
    }
}
