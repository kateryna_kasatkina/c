﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventHandlerDelegate
{
    class Program
    {
        class MyEvent
        {
            //обьявление использует делегат EventHandler
            public event EventHandler SomeEvent;
            public void OnSomeEvent()
            {
                if (SomeEvent != null)
                    SomeEvent(this, EventArgs.Empty);
            }
        }
        static void handler(object sender,EventArgs e)
        {
            Console.WriteLine("Событие произошло");
            Console.WriteLine("Источником является класс: "+ sender+" . ");
        }
        static void Main(string[] args)
        {
            MyEvent evt = new MyEvent();
            evt.SomeEvent += new EventHandler(handler);
            evt.OnSomeEvent();
        }
    }
}
