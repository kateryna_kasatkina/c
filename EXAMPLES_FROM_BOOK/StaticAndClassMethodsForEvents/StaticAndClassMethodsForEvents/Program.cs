﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticAndClassMethodsForEvents
{
    class Program
    {
        public delegate void MyEventHandler();

        public class MyEvent
        {
            public event MyEventHandler SomeEvent;
            public void OnSomeEvent()
            {
                if (SomeEvent != null)
                    SomeEvent();
            }
        }


        class X
        {
           public int Id;
            public X(int x)
            {
                Id = x;
            }

            //метод екземпляра используемого в качестве обработчика события
            public void Xhandler()
            {
                Console.WriteLine("событие принято обьектом"+Id);
            }
        }

        class Y
        {
            //этот статический метод используется в качестве обработчика событий
            public static void Yhandler()
            {
                Console.WriteLine("Событие получено классом");
            }
        }

        static void Main(string[] args)
        {
            MyEvent evt = new MyEvent();
            X o1 = new X(1);
            X o2 = new X(2);
            X o3 = new X(3);

            evt.SomeEvent += new MyEventHandler(o1.Xhandler);
            evt.SomeEvent += new MyEventHandler(o2.Xhandler);
            evt.SomeEvent += new MyEventHandler(o3.Xhandler);


            evt.OnSomeEvent();

            evt.SomeEvent -= new MyEventHandler(o1.Xhandler);
            evt.SomeEvent -= new MyEventHandler(o2.Xhandler);
            evt.SomeEvent -= new MyEventHandler(o3.Xhandler);
            evt.SomeEvent += new MyEventHandler(Y.Yhandler);


            evt.OnSomeEvent();
        }
    }
}
