﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MulticastEvent
{
    class Program
    {
        //обьявляем делегат для события
        public delegate void MyEventHandler();

        //обьявляем класс события
        class MyEvent
        {
            public event MyEventHandler SomeEvent;

            //етот метод вызывается для генерирования события
            public void OnSomeEvent()
            {
                if (SomeEvent != null)
                    SomeEvent();
            }
        }

        class X
        {
            public void Xhandler()
            {
                Console.WriteLine("Событие полученное обьектом Х.");
            }
        }

        class Y
        {
            public void Yhandler()
            {
                Console.WriteLine("Событие полученное обьектом Y.");
            }
        }

        static void handler()
        {
            Console.WriteLine("Событие полученное классом program");
        }

        static void Main(string[] args)
        {
            MyEvent evt = new MyEvent();
            X xOb = new X();
            Y yOb = new Y();

            //Добавляем обработчики в список события
            evt.SomeEvent += new MyEventHandler(handler);
            evt.SomeEvent += new MyEventHandler(xOb.Xhandler);
            evt.SomeEvent += new MyEventHandler(yOb.Yhandler);

            //генерируем событие
            evt.OnSomeEvent();
            Console.WriteLine();

            //удаляем один обработчик
            evt.SomeEvent -= new MyEventHandler(xOb.Xhandler);
            evt.OnSomeEvent();
        }
    }
}
