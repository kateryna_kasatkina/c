﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcptionsInheritance
{
   public  class RangeArray
    {
        //closed data
        int[] a;
        int lowerBound;
        int upperBound;
        int len;
        //create array with defined size
        public RangeArray(int low, int high)
        {
            high++;
            if (high <= low)
            {
                throw new MyExeption("lower index must be greater or equal to upper index");
            }
            a = new int[high - low];
            len = high - low;
            lowerBound = low;
            upperBound = --high;
        }
            //property Lenght only for reading
            public int Lenght { get { return len; } }
        //indexator for object of RangeArray
        public int this[int index]
        {
            //for reading element of array
            get
            {
                if (ok(index))
                {
                    return a[index - lowerBound];
                }
                else
                    throw new MyExeption("wrong bounds of diapason");
            }
            set
            {
                if(ok(index))
                {
                    a[index - lowerBound] = value;
                }
                else
                    throw new MyExeption("wrong bounds of diapason");
            }
        }

        private bool ok(int index)
        {
            if (index >= lowerBound && index <= upperBound)
                return true;
            return false;
        }
    }
}
