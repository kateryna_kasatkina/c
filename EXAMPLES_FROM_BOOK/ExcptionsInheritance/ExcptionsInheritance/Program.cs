﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcptionsInheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                RangeArray ra = new RangeArray(-5, 5);
                RangeArray ra2 = new RangeArray(1, 10);
                Console.WriteLine("ra.Lenght: ", ra.Lenght);
                for (int i = -5; i <= 5; i++)
                {
                    ra[i] = i;
                    Console.WriteLine(ra[i]);
                }

                Console.WriteLine("ra2.Lenght: ", ra2.Lenght);
                for (int i = 1; i <= 10; i++)
                {
                    ra[i] = i;
                    Console.WriteLine(ra[i]);
                }
            }
            catch (MyExeption exc)
            {
                Console.WriteLine(exc);
            }

            //Console.WriteLine("Generate out of range exception");



            //using wrong constructor
           // Console.WriteLine("using wrong constructor");
            try
            {
                RangeArray ra3 = new RangeArray(100,-10);

            }
           catch(MyExeption exc)
            {
                Console.WriteLine(exc);
            }


            //using wrong index
            //Console.WriteLine("using wrong index") ;
            try
            {
                RangeArray ra3 = new RangeArray(-2, 2);
                for (int i = -2; i <= 2; i++)
                {
                    ra3[i] = i;
                    Console.WriteLine(ra3[i]);
                }
            }
            catch (MyExeption exc)
            {
                Console.WriteLine(exc);
            }
        }
    }
}
