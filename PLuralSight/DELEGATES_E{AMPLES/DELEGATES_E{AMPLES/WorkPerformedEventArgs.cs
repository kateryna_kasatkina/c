﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DELEGATES_E_AMPLES
{
   public  class WorkPerformedEventArgs:EventArgs
    {
        public int Hours { get; set; }
        public WorkType WorkType { get; set; }
        public WorkPerformedEventArgs (int hours,WorkType worktype)
        {
            Hours = hours;
            WorkType = worktype;
        }
    }
}
