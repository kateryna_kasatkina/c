﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DELEGATES_E_AMPLES
{
    class Program
    {
        public delegate int WorkPerformedHandler(int hours, WorkType workType);
        static void Main(string[] args)
        {
            //WorkPerformedHandler del1 = new WorkPerformedHandler(WorkPerformed1);
            //WorkPerformedHandler del2 = new WorkPerformedHandler(WorkPerformed2);
            //WorkPerformedHandler del3 = new WorkPerformedHandler(WorkPerformed3);
            ////del1(5, WorkType.Golf);
            ////del2(10, WorkType.GenerateReports);

            //del1 += del2+del3;
            //int finalhours = del1(10, WorkType.GenerateReports);
            //Console.WriteLine(finalhours);

            //DoWork(del1);

            Console.WriteLine("----------------------------------");
            Worker worker = new Worker();//create an object of Worker class
            //class Worker contains a field (pointer to method) called WorkPerformed(takes "object sender,WorkPerformedEventArgs e"  arguments)
            worker.WorkPerformed += new EventHandler<WorkPerformedEventArgs>(worker_WorkPerformed);//attach method to delegate throught 
            worker.WorkCompleted += new EventHandler(worker_WorkCompleted);
            worker.DoWork(8, WorkType.GenerateReports);
            Console.Read();

        }

        static void worker_WorkCompleted(object sender, EventArgs e)
        {
            Console.WriteLine(" worker is done!");
        }

        static void worker_WorkPerformed(object sender,WorkPerformedEventArgs e)
        {
            Console.WriteLine("Hours worked: "+e.Hours+" "+e.WorkType);
        }

        //static void DoWork(WorkPerformedHandler del)
        //{
        //    del(5, WorkType.GoToMeetings);
        //}

        //private static int WorkPerformed2(int hours, WorkType workType)
        //{
        //    Console.WriteLine("WorkPerformed2 called " + hours.ToString());
        //    return hours + 2;
        //}

        //private static int WorkPerformed1(int hours, WorkType workType)
        //{
        //    Console.WriteLine("WorkPerformed1 called " + hours.ToString());
        //    return hours + 1;
        //}
        //private static int WorkPerformed3(int hours, WorkType workType)
        //{
        //    Console.WriteLine("WorkPerformed3 called " + hours.ToString());
        //    return hours + 3;
        //}
    }
    
    public enum WorkType
    {
        GoToMeetings,
        Golf,
        GenerateReports
    }
}
