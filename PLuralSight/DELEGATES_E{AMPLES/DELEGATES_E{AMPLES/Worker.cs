﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DELEGATES_E_AMPLES
{
   // public delegate int WorkPerformedHandler(object sender, WorkPerformedEventArgs e);
    public  class Worker
    {
        //public event WorkPerformedHandler WorkPerformed;
        public event EventHandler<WorkPerformedEventArgs> WorkPerformed;//указатель на метод OnWorkPerformed
        public event EventHandler WorkCompleted;
        public void DoWork(int hours,WorkType workType)
        {
            for(int i=0;i<hours;i++)
            {
                System.Threading.Thread.Sleep(1000);
                OnWorkPerformed(i + 1, workType);//вызываем метод связанный с делегатом
            }
            OnWorkCompleted();

        }
        protected virtual void OnWorkPerformed(int hours,WorkType workType)
        {
            
            var del = WorkPerformed as EventHandler<WorkPerformedEventArgs>;
            if(del!=null)
            {
                del(this,new WorkPerformedEventArgs(hours,workType));
            }

        }

        protected virtual void OnWorkCompleted()
        {
            if (WorkCompleted != null)
            {
                WorkCompleted(this, EventArgs.Empty);
            }
        }
    }
}
