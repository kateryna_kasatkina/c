﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace classTest.GradesTest.types
{
   
    [TestClass]
    public class TypeTest
    {
        [TestMethod]
        public void UsingArrays()
        {
            float[] grades;
            grades = new float[3];
            AddGrades(grades);
            Assert.AreEqual(89.1f, grades[1]);
        }

        private void AddGrades(float[] grades)
        {
            grades[1]=89.1f;
        }
        [TestMethod]
        public void UpperCaseString()
        {
            string name = "scott";
            name = name.ToUpper();
            Assert.AreEqual("SCOTT", name);
        }
        [TestMethod]
        public void AddDaysToDateTime()
        {
            DateTime dateN = DateTime.Now;//current dateTime
            DateTime date = new DateTime(2015, 1, 1);
            date=date.AddDays(1);
            Assert.AreEqual(2, date.Day);
        }
        [TestMethod]
        public void ReferenceTypesPassByValue()
        {
            GradeBook book1 = new GradeBook();
            GradeBook book2 = book1;
            GiveBookName(ref book2);//change name by reference
            Assert.AreEqual("A Gradebook", book1.Name);
        }
        private void GiveBookName(ref GradeBook book)
        {
            //book = new GradeBook();
            book.Name = "A Gradebook";
        }
        [TestMethod]
        public void StringComparisons()
        {
            string name1 = "Scott";
            string name2 = "scott";
            bool result = String.Equals(name1, name2, StringComparison.InvariantCultureIgnoreCase);
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void IntVariablesHoldValue()
        {
            int x1 = 100;
            int x2 = x1;
           
            Assert.AreEqual(x1, x2);
        }
        [TestMethod]
        public void GradebookVariablesHoldReferences()
        {
            GradeBook g1 = new GradeBook();
            GradeBook g2 = g1;
           
            g1.Name = "kate's grade book";
            Assert.AreEqual(g1.Name, g2.Name);
        }
    }
}
