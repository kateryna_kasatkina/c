﻿using System.Collections;
using System.IO;

namespace classTest
{
    internal interface IGradeTraker:IEnumerable
    {
          void addGrade(float grade);
          GradeStatistic ComputeStatistic();
          void WriteGrades(TextWriter destination);
        string Name { get; set; }
    }
}