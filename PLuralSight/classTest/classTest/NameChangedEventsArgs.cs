﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace classTest
{
    public class NameChangedEventsArgs:EventArgs
    {
        public string ExitingName { get; set; }
        public string NewName { get; set; }
    }
}
