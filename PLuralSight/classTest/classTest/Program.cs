﻿using System;
using System.IO;

namespace classTest
{
    class Program
    {
        static void Main(string[] args)
        {
            IGradeTraker book = CreateGradebook();
            GetBookName(book);

            AddGrades(book);
            SaveGrades(book);
            WriteResults(book);
        }

        private static GradeBook CreateGradebook()
        {
            return new ThrowAwayGradeBook();
        }

        private static void WriteResults(IGradeTraker book)
        {
            GradeStatistic stats = book.ComputeStatistic();

            foreach(float grade in book)
            {
                Console.WriteLine(grade);
            }



            WriteResult("Average", stats.AverageGrade);
            WriteResult("Lowest", stats.LowestGrade);
            WriteResult("Highest", stats.HighestGrade);
            WriteResult("Grade", stats.LetterGrade);
            WriteResult(stats.Description, stats.LetterGrade);
        }

        private static void SaveGrades(IGradeTraker book)
        {
            using (StreamWriter outputFile = File.CreateText("grades.txt"))
            {
                book.WriteGrades(outputFile);
            }
        }

        private static void AddGrades(IGradeTraker book)
        {
            book.addGrade(91);
            book.addGrade(85.4f);
            book.addGrade(75);
        }

        private static void GetBookName(IGradeTraker book)
        {
            try
            {
                Console.WriteLine("Enter a name");
                book.Name = Console.ReadLine();
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
           
        }

        private static void OnNameChanged(object sender, NameChangedEventsArgs args)
        {
            Console.WriteLine($"Grade book changing name from {args.ExitingName} to {args.NewName}");
        }

        static void WriteResult(string description, int result)
        {
            Console.WriteLine(description + ": " + result);
        }
        static void WriteResult(string description, float result)
        {
            //Console.WriteLine("{0}: {1}",description,result);
            Console.WriteLine($"{description}: {result:F2}", description, result);
        }
        static void WriteResult(string description, string result)
        {
            //Console.WriteLine("{0}: {1}",description,result);
            Console.WriteLine($"{description}: {result}", description, result);
        }
    }
}