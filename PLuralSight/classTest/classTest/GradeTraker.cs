﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace classTest
{
    public abstract class GradeTraker:IGradeTraker
    {
        public abstract void addGrade(float grade);
        public abstract GradeStatistic ComputeStatistic();
        public abstract void WriteGrades(TextWriter destination);
        public abstract IEnumerator GetEnumerator();

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Name cannot  be null or empty");
                }


                if (_name != value && NameChanged != null)
                {
                    NameChangedEventsArgs args = new NameChangedEventsArgs();
                    args.ExitingName = _name;
                    args.NewName = value;
                    NameChanged(this, args);
                }
                _name = value;

            }
        }
        public event NameChangedDelegate NameChanged;
        protected string _name;
    }
}
