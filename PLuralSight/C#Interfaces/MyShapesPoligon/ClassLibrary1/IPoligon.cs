﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
   public interface IPoligon
    {
        int NumberOfSides { get; set; }
        int SideLength { get; set; }

        double GetPerimetr();
        double GetArea();

    }
}
