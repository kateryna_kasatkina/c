﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public abstract class AbstractPoligon
    {
        public int NumberOfSides { get; set; }
        public int SideLength { get; set; }
        public AbstractPoligon(int sides, int lenght)
        {
            NumberOfSides = sides;
            SideLength = lenght;
        }
        public double GetPerimetr()
        {
            return NumberOfSides * SideLength;
        }
        public abstract double GetArea();
    }
}
