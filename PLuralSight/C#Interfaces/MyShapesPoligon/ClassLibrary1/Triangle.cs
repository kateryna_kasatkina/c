﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Triangle : AbstractPoligon
    {
        public Triangle(int length):
            base(3, length)
        { }
        public override double GetArea()
        {//SideLength
            return SideLength * SideLength * Math.Sqrt(3) / 4;
        }
    }
}
