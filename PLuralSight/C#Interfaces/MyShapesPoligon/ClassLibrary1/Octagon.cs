﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Octagon : IPoligon
    {
        public int NumberOfSides { get; set; }
        public int SideLength { get ; set; }
        public Octagon(int lenght)
        {
            NumberOfSides = 8;
            SideLength = lenght;
        }

        public double GetArea()
        {
            return SideLength * SideLength * (2 + 2 * Math.Sqrt(2));
        }

        public double GetPerimetr()
        {
            return SideLength * SideLength;
        }
    }
}
