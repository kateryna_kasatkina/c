﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            //var primeMinisters = new Dictionary<string,PrimeMinister>(StringComparer.InvariantCultureIgnoreCase)
            //{
            //    { "JC",new PrimeMinister("James Callagan",1976) },
            //    { "MT",new PrimeMinister("Margared Thatcher",1979) },
            //    { "TB",new PrimeMinister("Tony Blair",1997) }

            //};
            //Console.WriteLine(primeMinisters["tb"]);
            //primeMinisters["JC"] = new PrimeMinister("Jim Callaghan", 1976);
            //primeMinisters.Add("GB", new PrimeMinister("Gordon Brown", 2007));
            //foreach(var pm in primeMinisters.Values)
            //    Console.WriteLine(pm);


            //PrimeMinister pm;
            //bool found = primeMinisters.TryGetValue("DC", out pm);
            //    if(found)
            //    Console.WriteLine("value is: "+pm.ToString()+"\r\n");
            //    else
            //    Console.WriteLine("value was not in the dictionary");





            #region sortedList


            var primeMinisters = new SortedList<string, PrimeMinister>(new UncasedStringComparer())
            {
                { "JC",new PrimeMinister("James Callagan",1976) },
                { "MT",new PrimeMinister("Margared Thatcher",1979) },
                { "TB",new PrimeMinister("Tony Blair",1997) }

            };
            Console.WriteLine(primeMinisters["tb"]);
            primeMinisters["JC"] = new PrimeMinister("Jim Callaghan", 1976);
            primeMinisters.Add("GB", new PrimeMinister("Gordon Brown", 2007));
            foreach (var pm in primeMinisters.Values)
                Console.WriteLine(pm);
            #endregion
        }

        class UncasedStringEqualityComparer : IEqualityComparer<string>
        {
            public bool Equals(string x, string y)
            {
                return x.ToUpper() == y.ToUpper();
            }

            public int GetHashCode(string obj)
            {
                return obj.ToUpper().GetHashCode();
            }
        }


        class UncasedStringComparer : IComparer<string>
        {
            //private int myVar;

            //public int MyProperty
            //{
            //    get { return myVar; }
            //    set { myVar = value; }
            //}

            public int Compare(string x, string y)
            {
                return string.Compare(x, y, StringComparison.InvariantCultureIgnoreCase);
            }
        }
    }
}
