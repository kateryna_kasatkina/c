﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Set
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var bigCities = new HashSet<string>
            (new UncasedStringEqualityComparer())
            {
                "New York",
                "Mancheater",
                "Sheffield",
                "Paris"
            };

            //bool addedSheffield = bigCities.Add("Sheffield");
            //bool addedBeijing = bigCities.Add("Beijing");
            //Console.WriteLine("added Sheffield ? " + addedSheffield);
            //Console.WriteLine("added Beijing ? " + addedBeijing);
            //Console.WriteLine();

            foreach (string city in bigCities)
                Console.WriteLine(city);
            Console.WriteLine();

            //bigCities.Add("SHEFFIELD");
            //bigCities.Add("BEIJING");


            string[] citiesInUk = { "Sheffield", "Ripon", "Truro", "Paris" };

            //bigCities.IntersectWith(citiesInUk);
            //foreach (string city in bigCities)
            //    Console.WriteLine(city);
            //Console.WriteLine();

            //var newCities = bigCities.Intersect(citiesInUk);
            //foreach (string city in newCities)
            //    Console.WriteLine(city);
            //Console.WriteLine();

            bigCities.UnionWith(citiesInUk);
            foreach (string city in bigCities)
                Console.WriteLine(city);
        }


        class UncasedStringEqualityComparer : IEqualityComparer<string>
        {
            public bool Equals(string x, string y)
            {
                return x.ToUpper() == y.ToUpper();
            }

            public int GetHashCode(string obj)
            {
                return obj.ToUpper().GetHashCode();
            }
        }

    }
}
