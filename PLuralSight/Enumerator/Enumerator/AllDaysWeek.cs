﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Enumerator
{
    public  class AllDaysWeek:IEnumerable<string>
    {
        

        public IEnumerator<string> GetEnumerator()
        {
            Console.WriteLine("Calling generic enumerator");
            yield return "monday";
            yield return "Tuesday";
            yield return "Wednessday";
                
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}