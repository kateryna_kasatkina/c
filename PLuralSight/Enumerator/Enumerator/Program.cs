﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enumerator
{
    class Program
    {
        static void Main(string[] args)
        {
            //string[] daysOfWeek =
            //{
            //    "Monday",
            //    "Tuesday",
            //    "Wednessday",
            //    "Thursday",
            //    "Friday",
            //    "Saturday",
            //    "Sunday"
            //};
            ////DisplayItems("daysOfWeek");

            //AllDaysWeek allDays = new AllDaysWeek();
            //foreach (string day in allDays)
            //    Console.WriteLine(day);


            ///////////////////////////////////////
            //float[,] tempsGrid = new float[4, 3];
            //for(int x=0;x<4;x++)
            //{
            //    for(int y=0;y<3;y++)
            //    {
            //        tempsGrid[x, y] = x + 10 * y;
            //    }
            //}
            //for (int x = 0; x < 4; x++)
            //{
            //    for (int y = 0; y < 3; y++)
            //    {
            //        Console.Write(tempsGrid[x, y] + ", "); 
            //    }
            //    Console.WriteLine();
            //}


            //jugged array

            float[][] tempsGrid = new float[4][];
            for (int x = 0; x < 4; x++)
            {
                tempsGrid[x] = new float[3];
                for (int y = 0; y < 3; y++)
                {
                    tempsGrid[x][ y] = x + 10 * y;
                }
            }
            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    Console.Write(tempsGrid[x][y] + ", ");
                }
                Console.WriteLine();
            }

        }
        

        //under the hood of foreach
        public static void DisplayItems<T>(IEnumerable<T> collection)
        {
            using (IEnumerator<T> Enumerator = collection.GetEnumerator())
            {
                bool moreItems = Enumerator.MoveNext();
                while(moreItems)
                {
                    T item = Enumerator.Current;
                    Console.WriteLine(item);
                    moreItems = Enumerator.MoveNext();
                }
            }
        }
    }
}
