﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public struct ColorieCount:IComparable<ColorieCount>
    {
        private float _value;
        public float Value { get { return _value; } }
        public ColorieCount(float value)
        {
            this._value = value;

        }
        public override string ToString()
        {
            return _value + " cal";
        }

        public int CompareTo(ColorieCount other)
        {
            return this._value.CompareTo(other._value);
        }
        public static bool operator ==(ColorieCount x, ColorieCount y)
        {
            return x._value == y._value;
        }
        public static bool operator !=(ColorieCount x, ColorieCount y)
        {
            return x._value != y._value;
        }
        public static bool operator <(ColorieCount x, ColorieCount y)
        {
            return x._value < y._value;
        }
        public static bool operator >(ColorieCount x, ColorieCount y)
        {
            return x._value > y._value;
        }
        public static bool operator<=(ColorieCount x,ColorieCount y)
        {
            return x._value <= y._value;
        }
        public static bool operator >=(ColorieCount x, ColorieCount y)
        {
            return x._value >= y._value;
        }
        public bool Equals(ColorieCount other)
        {
            return _value == other._value;
        }
        public override bool Equals(object obj)
        {
            if(obj==null)
                return false;
            if (!(obj is ColorieCount))
                return false;
            return _value == ((ColorieCount)obj)._value;


        }
        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }
    }
}
