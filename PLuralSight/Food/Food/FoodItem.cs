﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum FoodGroup { Meat,Fruit,Vegetables,Sweets}

namespace Food
{
    public struct FoodItem : IEquatable<FoodItem>
    {
        private readonly string _name;
        private readonly FoodGroup _group;

        public string Name { get { return _name; } }
        public FoodGroup Group { get { return _group; } }

        public FoodItem(string name, FoodGroup group)
        {
            this._group = group;
            this._name = name;
        }

        

        public bool Equals(FoodItem other)
        {
            return this._name == other.Name && this._group == other._group;
        }

        public override string ToString()
        {
            return _name;
        }
        public override bool Equals(object obj)
        {
            if (obj is FoodItem)
                return Equals((FoodItem)obj);
            else
                return false;
        }
        public static bool operator ==(FoodItem Ihs, FoodItem rhs)
        {
            return Ihs.Equals(rhs);
        }
        public static bool operator !=(FoodItem Ihs, FoodItem rhs)
        {
            return !Ihs.Equals(rhs);
        }
        public override int GetHashCode()
        {
            return _name.GetHashCode() ^ _group.GetHashCode();
        }

    }
}
