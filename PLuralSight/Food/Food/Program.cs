﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Food
{
    class Program
    {
        static void Main(string[] args)
        {
            //Food apple = new Food("apple", FoodGroup.Fruit);
            //CookedFood stewedApple = new CookedFood("stewed", "apple", FoodGroup.Fruit);
            //CookedFood bakedApple = new CookedFood("baked", "apple", FoodGroup.Fruit);
            //CookedFood stewedApple2 = new CookedFood("stewed", "apple", FoodGroup.Fruit);
            //Food apple2 = new Food("apple", FoodGroup.Fruit);

            //Console.WriteLine(apple);
            //Console.WriteLine(stewedApple);

            //DisplayWhetherEqual(apple,stewedApple);
            //DisplayWhetherEqual(stewedApple, bakedApple);
            //DisplayWhetherEqual(stewedApple, stewedApple2);
            //DisplayWhetherEqual(apple, apple2);
            //DisplayWhetherEqual(apple, apple);

            // Food[] list = {new Food("apple",FoodGroup.Fruit),
            //new Food("pear",FoodGroup.Fruit),
            // new CookedFood("Baked","apple",FoodGroup.Fruit)};

            // SortAndShowList(list);
            // Console.WriteLine();
            // Food[] list2 = {new CookedFood("Baked","apple",FoodGroup.Fruit),
            //     new Food("pear",FoodGroup.Fruit),
            //new Food("apple",FoodGroup.Fruit),
            // };

            // SortAndShowList(list2);


            //var FoodItems = new HashSet<FoodItem>(FoodItemEqualityComparer.Instance);
            //FoodItems.Add(new FoodItem("apple", FoodGroup.Fruit));
            //FoodItems.Add(new FoodItem("pear", FoodGroup.Fruit));
            //FoodItems.Add(new FoodItem("pinapple", FoodGroup.Fruit));
            //FoodItems.Add(new FoodItem("Apple", FoodGroup.Fruit));
            //foreach (var Fooditem in FoodItems)
            //    Console.WriteLine(Fooditem);


            #region indexator
            MyMatrix mas = new MyMatrix(2, 2);
           // Food[] mas = new Food[4];
            mas[0,0]=new Food("apple", FoodGroup.Fruit);
            mas[0,1] = new Food("pear", FoodGroup.Fruit);
            mas[1,0] = new Food("pinapple", FoodGroup.Fruit);
            mas[1,1] = new Food("Apple", FoodGroup.Fruit);
            Console.WriteLine(mas[1, 0]);
            for (int x = 0; x < 2; x++)
            {
                for (int y = 0; y < 2; y++)
                {
                    Console.Write(mas[x, y] + ", ");
                }
                Console.WriteLine();
            }
            #endregion
            //string[] arr1 = { "apple", "orange", "pineaple" };
            //string[] arr2 = { "apple", "orange", "pineaple" };
            //var arrayEq = (System.Collections.IStructuralEquatable)arr1;//interface which allowes to test for structural equality
            //bool StructEqual = arrayEq.Equals(arr2, StringComparer.Ordinal);//usin Equals method from interface
            //Console.WriteLine(StructEqual);

            //for(int i=0;i<mas.Length;i++)
            //{
            //    Console.WriteLine(mas[i]);
            //}
            //Console.WriteLine();
            //var comparer = new FoodComparer();

            //Array.Sort(mas, comparer);
            //for (int i = 0; i < mas.Length; i++)
            //{
            //    Console.WriteLine(mas[i]);
            //}
            //Console.WriteLine();

            //Food [] test = Array.FindAll(mas, x => x.Name.Length == 5);
            //for (int i = 0; i <test.Length; i++)
            //{
            //    Console.WriteLine(test[i]);
            //}

            //int indexOfApple = Array.BinarySearch(mas,comparer);
            //Console.WriteLine(indexOfApple);
        }

        private static void SortAndShowList(Food[] list)
        {
            Array.Sort(list, FoodNameComparer.Instance);
            foreach (var item in list)
                Console.WriteLine(item);
        }

        static void DisplayWhetherEqual(Food food1,Food food2)
        {
            if(food1==food2)
            {
                Console.WriteLine(string.Format("{0,12}=={1}",food1,food2));
            }else
                Console.WriteLine(string.Format("{0,12}!={1}", food1, food2));
        }


        public class FoodComparer : IComparer<Food>
        {
            public int Compare(Food x, Food y)
            {

                return x.Name.Length.CompareTo(y.Name.Length);

            }
        }
    }
}
