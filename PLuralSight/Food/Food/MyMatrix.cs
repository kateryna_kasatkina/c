﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Food
{
   public sealed class MyMatrix
    {
       
        private int _n;
        private int _m;
        private Food[,] _matrix;
        private MyMatrix() { }
        public  MyMatrix(int M,int N)
        {
            this._n = N;
            this._m = M;
            _matrix = new Food[_n, _m];
        }
        public Food this[int N,int M]
        {
            get { return _matrix[N, M]; }
            set { _matrix[N, M] = value; }
        }
      
        public Food this[string name]
        {
            get
            {
                for (int x = 0; x < _n; x++)
                {
                    for (int y = 0; y < _m; y++)
                    {
                       if( _matrix[_n, _m].Name==name)
                        {
                            return _matrix[_n, _m];
                        }
                    }
                }
                return new Food("noName", FoodGroup.Fruit);
            }
            set { _matrix[_n, _m] = value; }
        }
    }
}
