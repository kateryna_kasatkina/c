﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] nums = new int[4];
            nums[0] = 1;
            nums[1] = 2;
            nums[2] = 3;
            nums[3] = 5;
           // Console.WriteLine(nums[3]);


            // эти два способа равноценны
            int[] nums2 = new int[] { 1, 2, 3, 5 };

            int[] nums3 = { 1, 2, 3, 5 };


            /////двумерный массив
            int[] num1 = new int[] { 0, 1, 2, 3, 4, 5 };

            int[,] num2 = { { 0, 1, 2 }, { 3, 4, 5 } };


            ///От многомерных массивов надо отличать массив
            ///массивов или так называемый "зубчатый массив":
            int[][] arr = new int[3][];
            arr[0] = new int[2];
            arr[1] = new int[3];
            arr[2] = new int[5];


            //многомерные:
            int[][,] mas = new int[3][,]
            {
                new int[,] { {1,2}, {3,4} },
                new int[,] { {1,2}, {3,6} },
                new int[,] { {1,2}, {3,5}, {8, 13} }
            };
           // Array.Sort(mas);//error!!!!!!!!!!!!!!

            /*Некоторые методы и свойства массивов

Свойство Length: позволяет получить количество элементов массива

Свойство Rank: позволяет получить размерность массива

Метод Array.Reverse: изменяет порядок следования элементов массива на обратный

Метод Array.Sort: сортирует элементы массива*/


//            int[] mas1 = new int[] { 8, 1, 5, 3, 4, 2 };
//            int length = mas1.Length;
//            //Console.WriteLine("количество элементов: {0}", length);
//            int rank = mas1.Rank;
//            //Console.WriteLine("размерность массива: {0}", rank);
//            Array.Reverse(mas1);
//            Array.Sort(mas1);
//          //  Console.WriteLine(mas1[1]);



//            /*Задание (начальное)
//1. Объявить одномерный (5 элементов ) массив с именем
//A и двумерный массив (3 строки, 4 столбца) дробных
//чисел с именем B. Заполнить одномерный массив А
//числами, введенными с клавиатуры пользователем,
//а двумерный массив В случайными числами с помо-
//щью циклов. Вывести на экран значения массивов:
//массива А в одну строку, массива В — в виде матри-
//цы. Найти в данных массивах общий максимальный
//элемент, минимальный элемент, общую сумму всех
//элементов, общее произведение всех элементов, сум-
//му четных элементов м*/


//            // Объявить одномерный (5 элементов ) массив с именем A
//            int[] A = new int[5];
//            for (int i = 0; i < 5; i++)
//            {
//                Console.WriteLine("enter element of array");
//                A[i] = int.Parse(Console.ReadLine());
//            }
//            for (int i = 0; i < 5; i++)
//                Console.Write(A[i] + " ");
//            Console.WriteLine("\r\nPress any key to close.");
//            Console.ReadKey();
//            //Заполнить одномерный массив А числами, введенными с клавиатуры пользователем,


//            //и двумерный массив (3 строки, 4 столбца) дробных чисел с именем B
//            //double[,] B = { { 0, 1, 2 ,6}, { 3, 4, 5,6 }, { 3, 4, 5,6 } };
//            double[,] B = new double[3, 4];
//            Random rd = new Random();
           
//            for (int i = 0; i < 3; i++)
//            {
//                for (int j = 0; j < 4; j++)
//                {

//                    B[i, j] = rd.NextDouble();
//                }
//            }

//            for (int i = 0; i < 3; i++)
//            {
//                for (int j = 0; j < 4; j++)
//                {
//                    Console.Write(B[i,j] + " ");
//                }
//                Console.WriteLine();
//            }

//            Array.Sort(A);
//            Array.Reverse(A);
//            for (int i = 0; i < 5; i++)
//                Console.Write(A[i] + " ");
//            int rank1 = B.Rank;
//            Console.WriteLine(rank1);
        }
    }
}
