���� ������:
1. ��������� (refference type) - ��� ���������� System.Object, ��������� ��� ����������� �������� ������
	-��� ������ (class)
	-��� ������� (array)
	-��� ������ (string)
2. �������� (value type) - ��� ����������  System.ValueType, ��������� � ����� ������ ��� ���������� �������� ������
	-��� ��������� (struct: DateTime, Point)
	-��� "�������" ���� (int, double, float, char)

System.Object - �� ���� �����������:
	- ��� ��������� ���� (������)
	- System.ValueType - �� ���� ������.:
		- ��� �������� ���� (������)

������� ��� ������ ���� � ���� �����:
.GetType()
.ToString()
.GetHashCode()
.Equals()