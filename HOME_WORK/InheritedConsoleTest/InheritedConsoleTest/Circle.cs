﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritedConsoleTest
{
    public class Circle: IMathShape
    {
        double x;
        double y;
        double r;
        private Circle() { }
        public Circle(double x, double y,double r)
        {
            this.x = x;
            this.y = y;
            this.r = r;
        }
        public virtual double calcS()
        {
            return Math.PI*Math.Pow(r,2.0);
        }
        public virtual double calcP()
        {
            return this.calcL();
        }
        public double calcL()
        {
            return 2 * Math.PI * this.r;
        }
    }
}

    