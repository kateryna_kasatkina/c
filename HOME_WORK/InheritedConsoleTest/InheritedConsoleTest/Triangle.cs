﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritedConsoleTest
{
    public class Triangle : Rectangle, IMathShape
    {
        protected double c;
        public double C { get { return c; } set { c = value; } }
        private Triangle() { }
        public  Triangle(double a, double b,double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        //S=sqrt{p*(p-a)*(p-b)*(p-c)}
        public override double calcS()
        {
            double p = calcP();
            return Math.Sqrt( p * (p - a) * (p - b) * (p - c));
        }
        public override double calcP()
        {
            return (this.a + this.b + this.c);
        }
    }
}
