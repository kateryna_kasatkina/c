﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritedConsoleTest
{
    class Program
    {
        public static void Main(string[] args)
        {
            MyPoint p1 = new MyPoint(0.0, 0.0);
            MyPoint p2 = new MyPoint(1.0, 1.0);
            double d = MyLine.calcDistance(p1, p2);

            Square sq1 = new Square(3);
            PrintArea(sq1);
            PrintPerimetr(sq1);

            Rectangle rec1 = new Rectangle(2.0, 3.0);
                                          //Console.WriteLine("Surface of rectangle with side a={0} b={1} equal={2}", rec1.A,rec1.B, recArea);
                                          // Console.WriteLine("With interface ...");

            PrintArea(rec1);
            PrintPerimetr(rec1);


            Triangle tr1 = new Triangle(3, 6, 6);
            PrintArea(tr1);
            PrintPerimetr(tr1);

            Circle cir1 = new Circle(2, 4, 6);
            PrintArea(cir1);
            PrintPerimetr(cir1);

            Console.WriteLine("press any key ...");
            Console.ReadKey();


        }
        public static void PrintArea(IMathShape shape)
        {
            double area = shape.calcS();
            Console.WriteLine("surface of shape ({0}={1})",shape,area);
        }
        public static void PrintPerimetr(IMathShape shape)
        {
            double perimetr = shape.calcP();
            Console.WriteLine("perimetr of shape ({0}={1})", shape, perimetr);
        }
    }

    
}
