﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritedConsoleTest
{
    public class MyPoint
    {
        private double x;
        private double y;
        public MyPoint(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }
        private MyPoint()
        {

        }

        public double X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        public double Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        public override string ToString()
        {

            return String.Format("[{0},{1}]", this.x, this.y);
        }
    }
}
