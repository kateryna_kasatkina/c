﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritedConsoleTest
{
    public class Rectangle:Square, IMathShape
    {
        protected double b;
        public double B { get { return b; } set { b = value; } }
        protected Rectangle() { }
        public Rectangle(double a, double b)
        {
            this.a = a;
            this.b = b;
        }
        public override double calcS()
        {
            return this.a * this.b;
        }
        public override double calcP()
        {
            return (this.a +this.b)*2;
        }
    }
}
