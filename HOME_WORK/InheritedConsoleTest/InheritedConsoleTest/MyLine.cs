﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritedConsoleTest
{
    public class MyLine
    {
        MyPoint p1;
        MyPoint p2;


        #region Конструкторы
        protected MyLine() { }
        public MyLine(MyPoint p1, MyPoint p2)
        {
            this.p1 = p1;
            this.p2 = p2;
        }
        public MyLine(double x1, double y1, double x2, double y2)
        {
            MyPoint p1 = new MyPoint(x1, y1);
            MyPoint p2 = new MyPoint(x2, y2);
            this.p1 = p1;
            this.p2 = p2;
        }
        #endregion
        #region static methods
        public static double calcDistance(double x1, double y1, double x2, double y2)
        {
            double d = Math.Sqrt(Math.Pow((x2 - x1), 2.0) + Math.Pow((y2 - y1), 2.0));
            return d;
        }
        public static double calcDistance(MyPoint p1, MyPoint p2)
        {
            return calcDistance(p1.X, p1.Y, p2.X, p2.Y);
        }
        #endregion
    }
}
