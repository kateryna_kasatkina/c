﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritedConsoleTest
{
    public class Square : IMathShape
    {
     
        protected  double a;
        public double A
        {
            get{ return a;}

            set{ a = value;}
        }
       
        protected Square() { }
        public  Square(double a)
        {
            this.a = a;
        }

        public virtual double calcS()//internal like public but could be watched from my exe
        {
            return this.a * this.a;
        }

        public virtual double calcP()//internal like public but could be watched from my exe
        {
            return (this.a *4);
        }
        



    }
}
