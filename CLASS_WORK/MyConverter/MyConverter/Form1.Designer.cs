﻿namespace MyConverter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCm = new System.Windows.Forms.TextBox();
            this.txtBoxGradusIn = new System.Windows.Forms.TextBox();
            this.txtBoxRadianIn = new System.Windows.Forms.TextBox();
            this.txtMilesIn = new System.Windows.Forms.TextBox();
            this.btnCmToInch = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBoxGradusOut = new System.Windows.Forms.TextBox();
            this.txtRadianOut = new System.Windows.Forms.TextBox();
            this.txtInch = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.txtKmIn = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMilesOut = new System.Windows.Forms.TextBox();
            this.txtKmOut = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtCm
            // 
            this.txtCm.Location = new System.Drawing.Point(87, 63);
            this.txtCm.Name = "txtCm";
            this.txtCm.Size = new System.Drawing.Size(120, 20);
            this.txtCm.TabIndex = 0;
            this.txtCm.TextChanged += new System.EventHandler(this.txtCm_TextChanged);
            this.txtCm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCm_KeyPress);
            // 
            // txtBoxGradusIn
            // 
            this.txtBoxGradusIn.Location = new System.Drawing.Point(87, 89);
            this.txtBoxGradusIn.Name = "txtBoxGradusIn";
            this.txtBoxGradusIn.Size = new System.Drawing.Size(120, 20);
            this.txtBoxGradusIn.TabIndex = 1;
            this.txtBoxGradusIn.TextChanged += new System.EventHandler(this.txtGradus_TextChanged);
            // 
            // txtBoxRadianIn
            // 
            this.txtBoxRadianIn.Location = new System.Drawing.Point(87, 115);
            this.txtBoxRadianIn.Name = "txtBoxRadianIn";
            this.txtBoxRadianIn.Size = new System.Drawing.Size(120, 20);
            this.txtBoxRadianIn.TabIndex = 2;
            this.txtBoxRadianIn.TextChanged += new System.EventHandler(this.txtRadian_TextChanged);
            // 
            // txtMilesIn
            // 
            this.txtMilesIn.Location = new System.Drawing.Point(75, 29);
            this.txtMilesIn.Name = "txtMilesIn";
            this.txtMilesIn.Size = new System.Drawing.Size(120, 20);
            this.txtMilesIn.TabIndex = 4;
            this.txtMilesIn.Text = "12";
            this.txtMilesIn.TextChanged += new System.EventHandler(this.txtMilesIn_TextChanged);
            // 
            // btnCmToInch
            // 
            this.btnCmToInch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCmToInch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCmToInch.Location = new System.Drawing.Point(213, 60);
            this.btnCmToInch.Name = "btnCmToInch";
            this.btnCmToInch.Size = new System.Drawing.Size(27, 23);
            this.btnCmToInch.TabIndex = 5;
            this.btnCmToInch.Text = "->";
            this.btnCmToInch.UseVisualStyleBackColor = true;
            this.btnCmToInch.Click += new System.EventHandler(this.btnCmToInch_Click);
            // 
            // button2
            // 
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.Enabled = false;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(213, 86);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(27, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "->";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.Enabled = false;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(213, 112);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(27, 23);
            this.button3.TabIndex = 7;
            this.button3.Text = "->";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(201, 0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(27, 23);
            this.button4.TabIndex = 8;
            this.button4.Text = "->";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Location = new System.Drawing.Point(201, 26);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(27, 23);
            this.button5.TabIndex = 9;
            this.button5.Text = "->";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "см";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "градусы";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "радиан";
            // 
            // txtBoxGradusOut
            // 
            this.txtBoxGradusOut.Location = new System.Drawing.Point(246, 114);
            this.txtBoxGradusOut.Name = "txtBoxGradusOut";
            this.txtBoxGradusOut.Size = new System.Drawing.Size(120, 20);
            this.txtBoxGradusOut.TabIndex = 17;
            // 
            // txtRadianOut
            // 
            this.txtRadianOut.Location = new System.Drawing.Point(246, 88);
            this.txtRadianOut.Name = "txtRadianOut";
            this.txtRadianOut.Size = new System.Drawing.Size(120, 20);
            this.txtRadianOut.TabIndex = 16;
            // 
            // txtInch
            // 
            this.txtInch.Location = new System.Drawing.Point(246, 62);
            this.txtInch.Name = "txtInch";
            this.txtInch.Size = new System.Drawing.Size(120, 20);
            this.txtInch.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(376, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "дюймы";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(376, 88);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "радиант";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(376, 114);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "градусы";
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button7.Cursor = System.Windows.Forms.Cursors.No;
            this.button7.Enabled = false;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button7.Location = new System.Drawing.Point(0, -4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(479, 40);
            this.button7.TabIndex = 27;
            this.button7.UseVisualStyleBackColor = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label12.Cursor = System.Windows.Forms.Cursors.No;
            this.label12.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label12.Location = new System.Drawing.Point(152, 7);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(175, 22);
            this.label12.TabIndex = 28;
            this.label12.Text = "Мой КОНВЕРТЕР";
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Location = new System.Drawing.Point(452, 1);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(26, 21);
            this.button6.TabIndex = 29;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // txtKmIn
            // 
            this.txtKmIn.Location = new System.Drawing.Point(75, 3);
            this.txtKmIn.Name = "txtKmIn";
            this.txtKmIn.Size = new System.Drawing.Size(120, 20);
            this.txtKmIn.TabIndex = 3;
            this.txtKmIn.TextChanged += new System.EventHandler(this.txtKmIn_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "км/час";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "мили/час";
            // 
            // txtMilesOut
            // 
            this.txtMilesOut.Location = new System.Drawing.Point(234, 2);
            this.txtMilesOut.Name = "txtMilesOut";
            this.txtMilesOut.Size = new System.Drawing.Size(120, 20);
            this.txtMilesOut.TabIndex = 18;
            // 
            // txtKmOut
            // 
            this.txtKmOut.Location = new System.Drawing.Point(234, 28);
            this.txtKmOut.Name = "txtKmOut";
            this.txtKmOut.Size = new System.Drawing.Size(120, 20);
            this.txtKmOut.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(364, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "мили/час";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(364, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 13);
            this.label11.TabIndex = 24;
            this.label11.Text = "км/ч";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtMilesOut);
            this.panel1.Controls.Add(this.txtKmIn);
            this.panel1.Controls.Add(this.txtMilesIn);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtKmOut);
            this.panel1.Location = new System.Drawing.Point(12, 138);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(434, 57);
            this.panel1.TabIndex = 30;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label13.Location = new System.Drawing.Point(196, 202);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "version 2.0.0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(481, 223);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtBoxGradusOut);
            this.Controls.Add(this.txtRadianOut);
            this.Controls.Add(this.txtInch);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnCmToInch);
            this.Controls.Add(this.txtBoxRadianIn);
            this.Controls.Add(this.txtBoxGradusIn);
            this.Controls.Add(this.txtCm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCm;
        private System.Windows.Forms.TextBox txtBoxGradusIn;
        private System.Windows.Forms.TextBox txtBoxRadianIn;
        private System.Windows.Forms.TextBox txtMilesIn;
        private System.Windows.Forms.Button btnCmToInch;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBoxGradusOut;
        private System.Windows.Forms.TextBox txtRadianOut;
        private System.Windows.Forms.TextBox txtInch;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox txtKmIn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMilesOut;
        private System.Windows.Forms.TextBox txtKmOut;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label13;
    }
}

