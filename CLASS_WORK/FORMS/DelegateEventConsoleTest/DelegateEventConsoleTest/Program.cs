﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateEventConsoleTest
{
    class Program
    {

        static void Main(string[] args)
        {
            #region Account test
            //Console.WriteLine("Account test");
            //Account account = new Account(200, 6);
            ////добавляем обработчик события
            //account.Added += Show_Message;//регистрация обработчика событий, для ссылочных типов не перегружаемый оператор
            //account.WithDraw(100);
            ////удаляем обработчик события
            //account.Withdrowed -= Show_Message;//отказ от обработчика событий
            //account.WithDraw(50);
            //account.Put(150);
            //Console.WriteLine("Press any key");
            //Console.ReadKey();
            #endregion

            #region AccountEx test
            Console.WriteLine("AccountEX test");
            AccountEx account2 = new AccountEx(200, 6);
            //добавляем обработчик события
            account2.Added += Show_Message;//регистрация обработчика событий, для ссылочных типов не перегружаемый оператор
            account2.WithDraw(100);
            //удаляем обработчик события
            account2.Withdrowed -= Show_Message;//отказ от обработчика событий
            account2.WithDraw(50);
            account2.Put(150);
            Console.WriteLine("Press any key");
            Console.ReadKey();
            #endregion


        }
        private static void Show_Message(string message)
        {
            Console.WriteLine(message);
        }
        private static void Show_Message(object sender,AccountEventArgs e)
        {
            Console.WriteLine("Сумма транзакции:{0}",e.sum);
            Console.WriteLine(e.message);
            if (!(sender is AccountEx)) return;
            AccountEx account = sender as AccountEx;
            //устанавливаем белый цвет символов
            Console.ForegroundColor = ConsoleColor.Black;
            //устанавливаем черный цвет фона
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine("Итого на счете:{0}",account.CurrentSum);
            //сбрасываем настройки цвета
            Console.ResetColor();
        }
    }
}
