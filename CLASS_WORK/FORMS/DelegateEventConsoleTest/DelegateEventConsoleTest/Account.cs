﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateEventConsoleTest
{
    public class Account
    {
        public delegate void AccountStateHandler(string message);//pointer to function takes string
        public event AccountStateHandler Withdrowed;//event when many withdrowed
        public event AccountStateHandler Added;//event when many is puted 

        int _sum;
        int _percentage;

        private Account()
        {
            _sum = 0;
            _percentage = 0;
        }
        public Account(int sum,int percentage)
        {
            _sum = sum;
            _percentage = percentage;
        }
        public int CurrentSum
        {
           get { return _sum; }
        }
        public int Percentege
        {
            get { return _percentage; }
        }
        public void Put(int sum)
        {
            _sum += sum;
            if(Added!=null)
            {
                Added("На счет поступило " + sum);//cenerate event Added
            }
        }
        public void WithDraw(int sum)
        {
            if(sum<=_sum)
            {
                _sum -= sum;
                if(Withdrowed != null)
                {
                    Withdrowed("sum" + sum + "was withdroewd");
                }
            }
            else
            {
                if (Withdrowed != null)
                    Withdrowed("not enought many");
            }
        }


    }
   //расширенная версия счета
    /// <summary>
    /// для параметра аргумента функции-делегата
    /// </summary>
    class AccountEventArgs
    {
        //message
        public string message;
        //сумма на которую изменился счет
        public int sum;
        public AccountEventArgs(string aMes,int aSum)
        {
            message = aMes;
            sum = aSum;
        }

    }
    class AccountEx
    {
        //create delegate with two parameters
        public delegate void AccountStateHandler(object sendr, AccountEventArgs e);
        //event occured when many withdrowed
        public event AccountStateHandler Withdrowed;
        //event occured when many Added
        public event AccountStateHandler Added;
        int _sum;
        int _percentage;
        public AccountEx(int sum, int percentage)
        {
            this._sum = sum;
            this._percentage = percentage;
        }
        public int CurrentSum
        {
            get { return _sum; }
        }
        public void Put(int sum)
        {
            _sum += sum;
            if (Added != null)
            {
                Added(this, new AccountEventArgs ("На счет поступило "+sum,sum));//cenerate event Added
            }
        }


        public void WithDraw(int sum)
        {
            if (sum <= _sum)
            {
                _sum -= sum;
                if (Withdrowed != null)
                {
                    Withdrowed(this,new AccountEventArgs ("Cумма "+sum+"снята со счета ",sum));
                }
            }
            else
            {
                if (Withdrowed != null)
                    Withdrowed(this, new AccountEventArgs("Недостаточно денег на счете " , sum));
            }
        }

        public int Percentege
        {
            get { return _percentage; }
        }
    }
}
