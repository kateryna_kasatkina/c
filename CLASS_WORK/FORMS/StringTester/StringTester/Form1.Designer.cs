﻿namespace StringTester
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.S1 = new System.Windows.Forms.Label();
            this.S2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.IsNullOrEmpty = new System.Windows.Forms.Button();
            this.compare = new System.Windows.Forms.Button();
            this.equals = new System.Windows.Forms.Button();
            this.split = new System.Windows.Forms.Button();
            this.CompareOrdinal = new System.Windows.Forms.Button();
            this.Contains = new System.Windows.Forms.Button();
            this.Copy = new System.Windows.Forms.Button();
            this.Clone = new System.Windows.Forms.Button();
            this.Insert = new System.Windows.Forms.Button();
            this.EndsWith = new System.Windows.Forms.Button();
            this.indexOf = new System.Windows.Forms.Button();
            this.Concat = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // S1
            // 
            this.S1.AutoSize = true;
            this.S1.Location = new System.Drawing.Point(25, 13);
            this.S1.Name = "S1";
            this.S1.Size = new System.Drawing.Size(20, 13);
            this.S1.TabIndex = 0;
            this.S1.Text = "S1";
            // 
            // S2
            // 
            this.S2.AutoSize = true;
            this.S2.Location = new System.Drawing.Point(25, 42);
            this.S2.Name = "S2";
            this.S2.Size = new System.Drawing.Size(20, 13);
            this.S2.TabIndex = 1;
            this.S2.Text = "S2";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(75, 13);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(421, 20);
            this.textBox1.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(75, 40);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(421, 20);
            this.textBox2.TabIndex = 3;
            // 
            // IsNullOrEmpty
            // 
            this.IsNullOrEmpty.Location = new System.Drawing.Point(331, 105);
            this.IsNullOrEmpty.Name = "IsNullOrEmpty";
            this.IsNullOrEmpty.Size = new System.Drawing.Size(75, 23);
            this.IsNullOrEmpty.TabIndex = 4;
            this.IsNullOrEmpty.Text = "IsNullOrEmpty";
            this.IsNullOrEmpty.UseVisualStyleBackColor = true;
            this.IsNullOrEmpty.Click += new System.EventHandler(this.IsNullOrEmpty_Click);
            // 
            // compare
            // 
            this.compare.Location = new System.Drawing.Point(50, 105);
            this.compare.Name = "compare";
            this.compare.Size = new System.Drawing.Size(75, 23);
            this.compare.TabIndex = 5;
            this.compare.Text = "compare";
            this.compare.UseVisualStyleBackColor = true;
            this.compare.Click += new System.EventHandler(this.compare_Click);
            // 
            // equals
            // 
            this.equals.Location = new System.Drawing.Point(131, 105);
            this.equals.Name = "equals";
            this.equals.Size = new System.Drawing.Size(75, 23);
            this.equals.TabIndex = 6;
            this.equals.Text = "equals";
            this.equals.UseCompatibleTextRendering = true;
            this.equals.UseVisualStyleBackColor = true;
            this.equals.Click += new System.EventHandler(this.equals_Click);
            // 
            // split
            // 
            this.split.Location = new System.Drawing.Point(230, 105);
            this.split.Name = "split";
            this.split.Size = new System.Drawing.Size(75, 23);
            this.split.TabIndex = 7;
            this.split.Text = "split";
            this.split.UseVisualStyleBackColor = true;
            this.split.Click += new System.EventHandler(this.split_Click);
            // 
            // CompareOrdinal
            // 
            this.CompareOrdinal.Location = new System.Drawing.Point(12, 153);
            this.CompareOrdinal.Name = "CompareOrdinal";
            this.CompareOrdinal.Size = new System.Drawing.Size(126, 23);
            this.CompareOrdinal.TabIndex = 8;
            this.CompareOrdinal.Text = "CompareOrdinal";
            this.CompareOrdinal.UseVisualStyleBackColor = true;
            this.CompareOrdinal.Click += new System.EventHandler(this.CompareOrdinal_Click);
            // 
            // Contains
            // 
            this.Contains.Location = new System.Drawing.Point(50, 203);
            this.Contains.Name = "Contains";
            this.Contains.Size = new System.Drawing.Size(75, 23);
            this.Contains.TabIndex = 9;
            this.Contains.Text = "Contains";
            this.Contains.UseVisualStyleBackColor = true;
            this.Contains.Click += new System.EventHandler(this.Contains_Click);
            // 
            // Copy
            // 
            this.Copy.Location = new System.Drawing.Point(149, 153);
            this.Copy.Name = "Copy";
            this.Copy.Size = new System.Drawing.Size(75, 23);
            this.Copy.TabIndex = 10;
            this.Copy.Text = "Copy";
            this.Copy.UseVisualStyleBackColor = true;
            this.Copy.Click += new System.EventHandler(this.Copy_Click);
            // 
            // Clone
            // 
            this.Clone.Location = new System.Drawing.Point(230, 153);
            this.Clone.Name = "Clone";
            this.Clone.Size = new System.Drawing.Size(75, 23);
            this.Clone.TabIndex = 11;
            this.Clone.Text = "Clone";
            this.Clone.UseVisualStyleBackColor = true;
            this.Clone.Click += new System.EventHandler(this.Clone_Click);
            // 
            // Insert
            // 
            this.Insert.Location = new System.Drawing.Point(331, 153);
            this.Insert.Name = "Insert";
            this.Insert.Size = new System.Drawing.Size(75, 23);
            this.Insert.TabIndex = 12;
            this.Insert.Text = "Insert";
            this.Insert.UseVisualStyleBackColor = true;
            this.Insert.Click += new System.EventHandler(this.Insert_Click);
            // 
            // EndsWith
            // 
            this.EndsWith.Location = new System.Drawing.Point(171, 202);
            this.EndsWith.Name = "EndsWith";
            this.EndsWith.Size = new System.Drawing.Size(75, 23);
            this.EndsWith.TabIndex = 13;
            this.EndsWith.Text = "EndsWith";
            this.EndsWith.UseVisualStyleBackColor = true;
            this.EndsWith.Click += new System.EventHandler(this.EndsWith_Click);
            // 
            // indexOf
            // 
            this.indexOf.Location = new System.Drawing.Point(315, 203);
            this.indexOf.Name = "indexOf";
            this.indexOf.Size = new System.Drawing.Size(75, 23);
            this.indexOf.TabIndex = 14;
            this.indexOf.Text = "indexOf";
            this.indexOf.UseVisualStyleBackColor = true;
            this.indexOf.Click += new System.EventHandler(this.indexOf_Click);
            // 
            // Concat
            // 
            this.Concat.Location = new System.Drawing.Point(75, 276);
            this.Concat.Name = "Concat";
            this.Concat.Size = new System.Drawing.Size(75, 23);
            this.Concat.TabIndex = 15;
            this.Concat.Text = "Concat";
            this.Concat.UseVisualStyleBackColor = true;
            this.Concat.Click += new System.EventHandler(this.Concat_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 426);
            this.Controls.Add(this.Concat);
            this.Controls.Add(this.indexOf);
            this.Controls.Add(this.EndsWith);
            this.Controls.Add(this.Insert);
            this.Controls.Add(this.Clone);
            this.Controls.Add(this.Copy);
            this.Controls.Add(this.Contains);
            this.Controls.Add(this.CompareOrdinal);
            this.Controls.Add(this.split);
            this.Controls.Add(this.equals);
            this.Controls.Add(this.compare);
            this.Controls.Add(this.IsNullOrEmpty);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.S2);
            this.Controls.Add(this.S1);
            this.Name = "mainForm";
            this.Text = "Examles of using type string";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label S1;
        private System.Windows.Forms.Label S2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button IsNullOrEmpty;
        private System.Windows.Forms.Button compare;
        private System.Windows.Forms.Button equals;
        private System.Windows.Forms.Button split;
        private System.Windows.Forms.Button CompareOrdinal;
        private System.Windows.Forms.Button Contains;
        private System.Windows.Forms.Button Copy;
        private System.Windows.Forms.Button Clone;
        private System.Windows.Forms.Button Insert;
        private System.Windows.Forms.Button EndsWith;
        private System.Windows.Forms.Button indexOf;
        private System.Windows.Forms.Button Concat;
    }
}

