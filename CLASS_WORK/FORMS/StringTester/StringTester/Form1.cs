﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StringTester
{
    public partial class mainForm : Form
    {
   
        public mainForm()
        {
            InitializeComponent();
        }

        private void IsNullOrEmpty_Click(object sender, EventArgs e)
        {
            //сравниваются ссылки а не знначения строк, использование кавычек всегда выделение памяти
            string s1 = textBox1.Text;
            //string s1=null;-так не получится
           // string s1 = String.Empty;
            // if(s1.Length==0) стиль .Net 1.1
            if (String.IsNullOrEmpty(s1))
            {
                MessageBox.Show("S1 empty string");
            }
        }

        private void equals_Click(object sender, EventArgs e)
        {
            string str = textBox1.Text;
            string str2 = textBox2.Text;
            if(string.Equals(str,str2))
            {
                MessageBox.Show("Strings are equal");
            }
            else
            {
                MessageBox.Show("Strings are not equal");
            }
        }

        private void compare_Click(object sender, EventArgs e)
        {
            string str = textBox1.Text;
            string str2 = textBox2.Text;
            int result = string.Compare(str, str2);
            MessageBox.Show(result.ToString());
            result = string.Compare(str, str2, true);
            MessageBox.Show(result.ToString());
            switch (result)
            {
                case -1:
                    MessageBox.Show("s1<s2");
                    break;
                case 1:
                    MessageBox.Show("s1>s2");
                    break;
                default:
                    MessageBox.Show("s1==s2");
                    break;

            }
        }

        private void split_Click(object sender, EventArgs e)
        {
            string str = textBox1.Text;
            string[] words = str.Split(' ');
            for(int i=0;i<words.Length;i++)
            {
                MessageBox.Show(words[i]);
            }
        }

        private void CompareOrdinal_Click(object sender, EventArgs e)
        {
            bool  result = false;
            string v1 = textBox1.Text;
            string v2 = textBox2.Text;
            result = (string.CompareOrdinal(v1, v2) > 0);//comparation without national future, but with register
            int res = (String.CompareOrdinal(v1, v2));
            switch (res)
            {
                case -1:
                    MessageBox.Show("s1<s2");
                    break;
                case 1:
                    MessageBox.Show("s1>s2");
                    break;

                default:
                    MessageBox.Show("s1==s2");
                    break;

            }
        }

        private void Contains_Click(object sender, EventArgs e)
        {
            String str = textBox1.Text;
            String str2 = textBox2.Text;
            if(str.Contains(str2)==true)
            {
                MessageBox.Show("'" + str + "'" + "contains" + "'" + str2 + "'");
            }
            else
            {
                MessageBox.Show("\"" + str + "\"" + "doesn't contain " + "'" + str2 + "'");
            }
        }

        private void Clone_Click(object sender, EventArgs e)
        {
            String str = textBox1.Text;
            String clone = (string)str.Clone();//удвоение ссилок,два ссылочных обьекта на те же данные
            MessageBox.Show(str + clone);
        }

        private void Copy_Click(object sender, EventArgs e)
        {
            String str = textBox1.Text;
            String str2 = String.Copy(str);//work lile copy constructor
            MessageBox.Show(str + str2);

        }

        private void Insert_Click(object sender, EventArgs e)
        {
            String str = textBox1.Text;
            String str2 = textBox2.Text;
            string res = str.Insert(5, str2);//creating new string ,counting from 0
           
            StringBuilder sb = new StringBuilder(str);//StringBilder could be changed(redacted)
            sb.Insert(5, str2);
            MessageBox.Show(sb.ToString());
        }

        private void EndsWith_Click(object sender, EventArgs e)
        {
            String str = textBox1.Text;
            String str2 = textBox2.Text;
            if(str.EndsWith(str2)==true)
            {
                MessageBox.Show("'" + str + "'ends with charachter'" + str2 + "'");
            }
            else
            {
                MessageBox.Show("'" + str + "'does not end with charachter'" + str2 + "'");
            }
        }

        private void indexOf_Click(object sender, EventArgs e)
        {
            String str = textBox1.Text;
            String str2 = textBox2.Text;
            int n = str.IndexOf(str2);//если не нашел -1
            MessageBox.Show("'Вхождение'" + str2 + "'с буквы'" + n.ToString());
        }

        private void Concat_Click(object sender, EventArgs e)
        {
            String str = textBox1.Text;
            String str2 = textBox2.Text;
            MessageBox.Show(str + str2 );
        }
    }
}
