﻿namespace GraficTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDrawRect = new System.Windows.Forms.Button();
            this.btnDrawElips = new System.Windows.Forms.Button();
            this.btnDrawCircle = new System.Windows.Forms.Button();
            this.TestDLL = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDrawRect
            // 
            this.btnDrawRect.Location = new System.Drawing.Point(42, 447);
            this.btnDrawRect.Name = "btnDrawRect";
            this.btnDrawRect.Size = new System.Drawing.Size(107, 51);
            this.btnDrawRect.TabIndex = 0;
            this.btnDrawRect.Text = "Прямоугольник";
            this.btnDrawRect.UseVisualStyleBackColor = true;
            this.btnDrawRect.Click += new System.EventHandler(this.btnDrawRect_Click);
            // 
            // btnDrawElips
            // 
            this.btnDrawElips.Location = new System.Drawing.Point(288, 447);
            this.btnDrawElips.Name = "btnDrawElips";
            this.btnDrawElips.Size = new System.Drawing.Size(127, 50);
            this.btnDrawElips.TabIndex = 1;
            this.btnDrawElips.Text = "Элипс";
            this.btnDrawElips.UseVisualStyleBackColor = true;
            this.btnDrawElips.Click += new System.EventHandler(this.btnDrawElips_Click);
            // 
            // btnDrawCircle
            // 
            this.btnDrawCircle.Location = new System.Drawing.Point(597, 447);
            this.btnDrawCircle.Name = "btnDrawCircle";
            this.btnDrawCircle.Size = new System.Drawing.Size(160, 50);
            this.btnDrawCircle.TabIndex = 2;
            this.btnDrawCircle.Text = "button1";
            this.btnDrawCircle.UseVisualStyleBackColor = true;
            this.btnDrawCircle.Click += new System.EventHandler(this.btnDrawCircle_Click);
            // 
            // TestDLL
            // 
            this.TestDLL.Location = new System.Drawing.Point(832, 465);
            this.TestDLL.Name = "TestDLL";
            this.TestDLL.Size = new System.Drawing.Size(75, 23);
            this.TestDLL.TabIndex = 3;
            this.TestDLL.Text = "TestDLL";
            this.TestDLL.UseVisualStyleBackColor = true;
            this.TestDLL.Click += new System.EventHandler(this.TestDLL_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 592);
            this.Controls.Add(this.TestDLL);
            this.Controls.Add(this.btnDrawCircle);
            this.Controls.Add(this.btnDrawElips);
            this.Controls.Add(this.btnDrawRect);
            this.Name = "MainForm";
            this.Text = "тест графических возможностей";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDrawRect;
        private System.Windows.Forms.Button btnDrawElips;
        private System.Windows.Forms.Button btnDrawCircle;
        private System.Windows.Forms.Button TestDLL;
    }
}

