﻿using itstep.kharkov.Tester.MyObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace GraficTest
{
    public partial class MainForm : Form//inherited structure
    {
        public MainForm()
        {
            InitializeComponent();
            btnDrawRect.Text = "Прямоугольник";
            btnDrawElips.Text = "Эллипс";
            btnDrawCircle.Text = "Окружность";
        }

        private void btnDrawRect_Click(object sender, EventArgs e)
        {
            //получаем графический контекст=Холст, на котором рисовать
            //Определится с контуром - Pen
            //Определится с кистью
            //вызываем из контекстф нужную функцию
            Graphics graphics = Graphics.FromHwnd(this.Handle);
            Pen myPen = Pens.Red;
            Brush myBrush = Brushes.DarkBlue;
            int x = 100;
            int y = 150;
            int w = 50;
            int h = 25;
            graphics.DrawRectangle(myPen, x, y, w, h);
        }

        private void btnDrawElips_Click(object sender, EventArgs e)
        {
            Graphics graphics = Graphics.FromHwnd(this.Handle);
            Pen myPen = Pens.Green;
            Brush myBrush = Brushes.Coral;
            int x = 200;
            int y = 150;
            int w = 50;
            int h = 25;
            graphics.DrawEllipse(myPen, x, y, w, h);
        }

        private void btnDrawCircle_Click(object sender, EventArgs e)
        {

        }

        private void TestDLL_Click(object sender, EventArgs e)
        {
           // this.Text = itstep.kharkov.Tester.MyObjects.MyMesseger.formatMSG("Hello");
            MessageBox.Show(MyMesseger.formatMSG("Hello"));
        }
    }
}
