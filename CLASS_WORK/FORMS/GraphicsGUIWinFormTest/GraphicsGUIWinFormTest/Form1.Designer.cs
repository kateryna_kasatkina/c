﻿namespace GraphicsGUIWinFormTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblA = new System.Windows.Forms.Label();
            this.lblB = new System.Windows.Forms.Label();
            this.txtX = new System.Windows.Forms.TextBox();
            this.lblX = new System.Windows.Forms.Label();
            this.btnCheck = new System.Windows.Forms.Button();
            this.TestDLL = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblA
            // 
            this.lblA.AutoSize = true;
            this.lblA.Location = new System.Drawing.Point(104, 116);
            this.lblA.Name = "lblA";
            this.lblA.Size = new System.Drawing.Size(25, 13);
            this.lblA.TabIndex = 0;
            this.lblA.Text = "a=3";
            // 
            // lblB
            // 
            this.lblB.AutoSize = true;
            this.lblB.Location = new System.Drawing.Point(256, 116);
            this.lblB.Name = "lblB";
            this.lblB.Size = new System.Drawing.Size(31, 13);
            this.lblB.TabIndex = 1;
            this.lblB.Text = "b=14";
            // 
            // txtX
            // 
            this.txtX.Location = new System.Drawing.Point(259, 203);
            this.txtX.Name = "txtX";
            this.txtX.Size = new System.Drawing.Size(100, 20);
            this.txtX.TabIndex = 2;
            this.txtX.Text = "0";
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.Location = new System.Drawing.Point(73, 197);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(132, 26);
            this.lblX.TabIndex = 3;
            this.lblX.Text = "Введите целое значение\r\n Х из интервала";
            this.lblX.Click += new System.EventHandler(this.lblX_Click);
            // 
            // btnCheck
            // 
            this.btnCheck.Location = new System.Drawing.Point(171, 298);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(75, 23);
            this.btnCheck.TabIndex = 4;
            this.btnCheck.Text = "определить";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // TestDLL
            // 
            this.TestDLL.Location = new System.Drawing.Point(333, 297);
            this.TestDLL.Name = "TestDLL";
            this.TestDLL.Size = new System.Drawing.Size(75, 23);
            this.TestDLL.TabIndex = 5;
            this.TestDLL.Text = "TestDLL";
            this.TestDLL.UseVisualStyleBackColor = true;
            this.TestDLL.Click += new System.EventHandler(this.TestDLL_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 423);
            this.Controls.Add(this.TestDLL);
            this.Controls.Add(this.btnCheck);
            this.Controls.Add(this.lblX);
            this.Controls.Add(this.txtX);
            this.Controls.Add(this.lblB);
            this.Controls.Add(this.lblA);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblA;
        private System.Windows.Forms.Label lblB;
        private System.Windows.Forms.TextBox txtX;
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.Button TestDLL;
    }
}

