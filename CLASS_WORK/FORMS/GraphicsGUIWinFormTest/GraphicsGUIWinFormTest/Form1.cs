﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using itstep.kharkov.Tester.MyObjects;

namespace GraphicsGUIWinFormTest
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void lblX_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            Pen mypen = new Pen(Color.Black, 1);
            gr.DrawLine(mypen, lblA.Left - 100, lblA.Top - 3, lblB.Left + lblB.Width + 100, lblA.Top - 3);
            gr.DrawLine(mypen, lblB.Left + lblB.Width + 100,
                lblA.Top - 3, lblB.Left + lblB.Width + 100 - 10,
                lblA.Top - 3 + 5);//arraow down
            gr.DrawLine(mypen, lblB.Left + lblB.Width + 100,
                lblA.Top - 3, lblB.Left + lblB.Width + 100 - 10,
                lblA.Top - 3 - 5);
            gr.DrawString("x", this.Font, Brushes.Black,
                lblB.Left + lblB.Width + 100 - 10,
                lblA.Top - 3 + 5);
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            string strValue = txtX.Text;
            // int x;
            //double x;
            #region examples of dangerous converting
            //double x=Convert.ToDouble(strValue);
            // x = Convert.ToInt32(strValue);
            #endregion
            double x;
            try
            {
                x = Convert.ToInt32(strValue);
                //x=Convert.ToDouble(strValue);
            }
            catch(FormatException ex)
            {
                MessageBox.Show(String.Format("Inputed value {0}" +
                    "couldn't be converted to number. " +
                    "details:{1}", strValue, ex.Message), "Error");
                return;
            }
            catch(Exception ex)
            {
                MessageBox.Show("wrong number");
                    return;
            }

            if((x>=3)&&(x<=14))
            {
                MessageBox.Show(String.Format("x={0} belong to "
                    + " iterval", x), "result",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(String.Format("x={0} does not belong to "
                    + " iterval", x), "result",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void TestDLL_Click(object sender, EventArgs e)
        {
           this.Text= itstep.kharkov.Tester.MyObjects.MyMesseger.formatMSG("Hello");
            MessageBox.Show(MyMesseger.formatMSG("Hello"));
        }
    }
}
