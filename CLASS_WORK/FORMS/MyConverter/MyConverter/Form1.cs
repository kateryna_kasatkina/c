﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyConverter
{
    public partial class Form1 : Form
    {
        public delegate double Operation(double op);
        public Form1()
        {
            InitializeComponent();
        }
        private void textChangedAction(TextBox txtBoxFrom,TextBox txtBoxTo, Operation operation)
        {
            if (String.IsNullOrEmpty(txtBoxFrom.Text))
            {
                txtBoxTo.Text = string.Empty;
            }
            if (!String.IsNullOrEmpty(txtBoxFrom.Text))
            {
                double x = Convert.ToDouble(txtBoxFrom.Text);
                string result = string.Empty;
                calcAction(x, operation, out result);
                txtBoxTo.Text = result;
            }
        }
        private void calcAction(double value, Operation operation, out string result)
        {
            result = string.Empty;
            double d = operation.Invoke(value);
            result = Convert.ToString(d);
        }
        private double toInch(double x)
        {
            return x * 2.54;
        }
        private double toRadian(double alpha)
        {
            return alpha * Math.PI / 180.0;
        }
        private double toGradus(double radian)
        {
            return 180.0 * radian/Math.PI;
        }
        private double toMiles(double km)
        {
            return km * 0.621371;
        }
        private double toKilometers(double miles)
        {
            return miles * 1.609344;
        }
        private void txtCm_TextChanged(object sender, EventArgs e)
        {
            Operation myDelegate = new Operation(toInch);
            textChangedAction(txtCm, txtInch, myDelegate);
        }

        private void txtGradus_TextChanged(object sender, EventArgs e)
        {
            Operation myDelegate = new Operation(toRadian);
            textChangedAction(txtBoxGradusIn, txtRadianOut, myDelegate);
        }

        private void txtRadian_TextChanged(object sender, EventArgs e)
        {
            Operation myDelegate = new Operation(toGradus);
            textChangedAction(txtBoxRadianIn, txtBoxGradusOut, myDelegate);
        }

        private void txtKmIn_TextChanged(object sender, EventArgs e)
        {
            Operation myDelegate = new Operation(toMiles);
            textChangedAction(txtKmIn, txtMilesOut, myDelegate);
        }

        private void txtMilesIn_TextChanged(object sender, EventArgs e)
        {
            Operation myDelegate = new Operation(toKilometers);
            textChangedAction(txtMilesIn, txtKmOut, myDelegate);
        }
        
        private void button6_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void txtCm_KeyPress(object sender, KeyPressEventArgs e)
        {
            digitKeyPress(e);
        }

        private static void digitKeyPress(KeyPressEventArgs e)
        {
            string didgits = "0123456789,\b";
            string onlyIntegerDigits = "0123456789";
            string integerDigits = "0123456789\b";
            string Key = e.KeyChar.ToString();
            checkDigitKeyPressAction(e, Key, didgits);
        }

        private static void integerDigitsKeyPress(KeyPressEventArgs e)
        {
            string integerDigits = "0123456789\b";
            string Key = e.KeyChar.ToString();
            checkDigitKeyPressAction(e, Key, integerDigits);
        }

        private static void onlyIntegerKeyPress(KeyPressEventArgs e)
        {
            string onlyIntegerDigits = "0123456789";
            string Key = e.KeyChar.ToString();
            checkDigitKeyPressAction(e, Key, onlyIntegerDigits);
        }

        private static void onlyHexIntegerKeyPress(KeyPressEventArgs e)
        {
            string onlyIntegerDigits = "0123456789ABCDEF";
            string Key = e.KeyChar.ToString();
            Key = Key.ToUpper();
            checkDigitKeyPressAction(e, Key, onlyIntegerDigits);
        }

        private static void checkDigitKeyPressAction(KeyPressEventArgs e, string Key, string correctString)
        {
            if (!correctString.Contains(Key))
            {
                e.KeyChar = (char)0;
            }
        }

        private void btnCmToInch_Click(object sender, EventArgs e)
        {
            // Store integer 10
            int intValue = 10;
            // Convert integer 10 as a hex in a string variable
            string hexValue = intValue.ToString("X");
            // Convert the hex string back to the number
            int intAgain = int.Parse(hexValue, System.Globalization.NumberStyles.HexNumber);
            double d = Math.Sin(1);
        }

    }
}
