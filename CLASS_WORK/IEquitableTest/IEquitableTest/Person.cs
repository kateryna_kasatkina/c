﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEquitableTest
{
    public class Person : IEquatable<Person>,ICloneable
    {
        private string uniqueSsn;
        private string lName;
        public string Name;
        public IdInfo IdInfo;
        #region Constructors
        public string SSN
        {
            get
            {
                return this.uniqueSsn;
            }

            set
            {
                uniqueSsn = value;
            }
        }
       
        public string LastName
        {
            get { return this.lName;
            }
            set
            {
                lName = value;
            }
        }
        private Person() { }
        public Person (string lastName, string ssn)
        {
            this.LastName = lastName;
            this.SSN = ssn;
        }
        #endregion
        #region Equals implentation
        public bool Equals(Person other)
        {
            if (other == null)
                return false;
            if (this.uniqueSsn == other.uniqueSsn)
                return true;
            else
                return false;
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            Person personobj = obj as Person;
            if (personobj == null)
                return false;
            else
                return Equals(personobj);
        }
        public override int GetHashCode()
        {
            return this.SSN.GetHashCode();
        }
        public static bool operator ==(Person p1, Person p2)
        {
            if ((object)p1 == null || (object)p2 == null)
                return Object.Equals(p1, p2);
            return p1.Equals(p2);
        }
        public static bool operator !=(Person p1, Person p2)
        {
            if ((object)p1 == null || (object)p2 == null)
                return !Object.Equals(p1, p2);
            return p1.Equals(p2);
        }
        #endregion
        #region Compare

        #endregion

        public Person ShallowCopy()//удвоение ссылок
        {
            return (Person)this.MemberwiseClone();//не затрагивает ссылочные типы внутри нашего обьекта
        }

        public Person DeepCopy()//затрагивает ссылочные типы, правильная реализация интерфейса
        {
            Person other = (Person)this.MemberwiseClone();
            other.IdInfo = new IdInfo(IdInfo.IdNumber);
            other.Name = String.Copy(Name);
            return other;
        }

        public object Clone()//логика метода  применяется для классов с ссылочными полями
        {
            return DeepCopy();
        }
    }


    //public class Address:ICloneable
    //{

    //}

}
