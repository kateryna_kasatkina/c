﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEquitableTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Person applicant1 = new Person("jon1", "099-12-15");
            Person applicant2 = new Person("jon2", "099-12-25");
            Person applicant3 = new Person("jon3", "099-12-35");


            List<Person> applicants = new List<Person>();
            applicants.Add(applicant1);
            applicants.Add(applicant2);
            applicants.Add(applicant3);

            ArrayList arraylist = new ArrayList();
            arraylist.Add(applicants);
            arraylist.Contains(applicants);//equals from system.object



            Person candidate = new Person("jon3", "099-12-35");
            if(applicants.Contains(candidate))//equals from interface  List<Person> applicants
                Console.WriteLine("Found {0} (SSN{1}).",candidate.LastName,candidate.SSN);
            else
                Console.WriteLine("Not Found {0} (SSN{1}).", candidate.LastName, candidate.SSN);


        }
    }
}
