﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayListTest
{
    class MyArrayList 
    { 
        private ArrayList ar; 
        public MyArrayList()
        {
            this.ar = new ArrayList();
        }
        public void Add(double v) 
        { 
            ar.Add(v); 
        }
        public void Add(int v)
        {
            ar.Add((double)v);
        } 
        public int Count
        {
            get { return ar.Count; }
        }
        public double this[int index] 
        { get 
          { 
            return (double)ar[index]; 
          }  
            set 
            { 
                ar[index] = value; 
            } 
        } 
        //public int this[int index] 
        //{ 
        //    get { return (int)ar[index]; 
        //}   set { ar[index] = value; } 
        //}
    }
}
