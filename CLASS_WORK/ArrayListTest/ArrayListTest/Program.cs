﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayListTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //примеры "языковых" массивов - часть языка C# и прлатформы .Net
            int[] array = new int[10];
            int[,] matrix = new int[3, 3];
            //коллекция System.Object - внутри список, 
            //с доступ как у массива
            //[] - это оператор индексации
            ArrayList arrayList = new ArrayList();
            MyArrayList arrList = new MyArrayList();
            int a = 13;
            double d = 3.14;
            char ch = '1';
            string str = "3.14";
            arrayList.Add(a);
            arrList.Add(d);
            arrList.Add(a);
            //arrList.Add(str);//не пропускает компилятор
            arrayList.Add(d);
            arrayList.Add(ch);
            arrayList.Add(str);
            //вывод на экран
            for(int i =0; i <arrayList.Count; i++)
            {
                Console.WriteLine(arrayList[i]);
                //неявно вызывается ToString()
                //который "хранится" в System.Object
            }
            double sum = 0;
            double sum2 = 0;
            for (int i = 0; i < arrList.Count; i++ )
            {
                sum2 += arrList[i];
            }
            Console.WriteLine("Сумма элементов класса MyArrayList = {0}", sum2);

                for (int i = 0; i < arrayList.Count; i++)
                {
                    try
                    {
                        sum += (double)arrayList[i];
                    }
                    catch (InvalidCastException)
                    {
                        Console.WriteLine("данные {0} нельзя преобразовать в double."
                            + " Фактический их тип {1}", arrayList[i],
                            arrayList[i].GetType().ToString());
                    }

                }
            Console.WriteLine("press any key");
            Console.ReadKey();
        }
    }
}
