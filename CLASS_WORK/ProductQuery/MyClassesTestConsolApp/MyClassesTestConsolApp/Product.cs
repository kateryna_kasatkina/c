﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassesTestConsolApp
{
    
        public class Product : IEquatable<Product>, IComparable, ICloneable
        {
            public static string getAllProductsSelect()
            {
                StringBuilder sb = new StringBuilder();
                /*[PRODUCTS].[PRICE],
[PRODUCTS].[PRODUCTS_NAME],
[PRODUCTS].[PRODUCTS_ID ]
FROM
[dbo].[PRODUCTS]*/
                sb.Append(" select ");
                sb.Append(" [dbo].[PRODUCTS].[PRICE], ");
                sb.Append(" [dbo].[PRODUCTS].[PRODUCTS_NAME] , ");
                sb.Append("[dbo].[PRODUCTS].[PRODUCTS_ID ], ");
                sb.Append(" from  ");
                sb.Append(" [dbo].[PRODUCTS] ");
                return sb.ToString();
            }
            private int products_id;
            public int ID { get { return this.products_id; } }
            private string products_name;
            public string Name
            {
                set { products_name = value; }
                get { return products_name; }
            }
            private int products_price;
            public int Price
            {
                set { products_price = value; }
                get { return products_price; }
            }


            protected  Product() { }
            public Product( int products_id, string products_name, int products_price )
            {
                this.products_id = products_id;
                this.products_name = products_name;
                this.products_price = products_price;
            }
            #region Equals Implementation
            public bool Equals(Product other)//реализация интерфейса IEquatable
            {
                if (other == null)
                    return false;
                if (this.products_id == other.products_id)
                    return true;
                else
                    return false;
            }
            public override bool Equals(Object obj)//реализация System.Object
            {
                if (obj == null)
                    return false;
                Product clientObj = obj as Product;
                if (clientObj == null)
                    return false;
                else
                    return Equals(clientObj);//вызов реализации для интерфейса-строгий тип
            }
            public override int GetHashCode()//из System.Object для Dictionary
            {
                return this.products_id.GetHashCode();
                //должа основываться на полях класса, которые задействованы в Equals
                //из System.Object
            }
            //операторы сравнения "==" и "!=" основываются на Equals из System.Object
            public static bool operator ==(Product pr1, Product pr2)
            {
                if (((object)pr1) == null || ((object)pr2) == null)
                    return Object.Equals(pr1, pr2);

                return pr1.Equals(pr2);
            }
             public static bool operator !=(Product pr1, Product pr2)
            {
                if (((object)pr1) == null || ((object)pr2) == null)
                    return !Object.Equals(pr1, pr2);

                return !pr1.Equals(pr2);
            }
            #endregion
            #region реализация IComparable
            public int CompareTo(object obj)//аналог int res = String.Compare(str, str2);
            {
                if (obj == null) return 1;

                Product otherClient = obj as Product;
                if (otherClient != null)
                    return this.products_id.CompareTo(otherClient.products_id);
                else
                    throw new ArgumentException("Object is not a Product");
            }
            #endregion
            #region Clonable
            public Product ShallowCopy()//аналог String clone = (string)str.Clone();//удвоение ссылок:
            {
                return (Product)this.MemberwiseClone();//не зарагивает ссылочные типы
                                                      //внутри нашего объекта - поддержка клонирования .Net-ом
            }
            public object Clone()
            {
                return ShallowCopy();
            }
            #endregion
        }
    }

