﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyDataBaseClasses;

namespace MyClassesTestConsolApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //тест интерфейса IEquatable
            //это методы Contains
            List<Client> clients = new List<Client>();
            Client client1 = new Client(1, "Test1", "phone1", "mail1");
            clients.Add(client1);
            bool res1 = clients.Contains(client1);
            Client client2 = new Client(1, "Test1", "phone1", "mail1");
            bool res2 = (client1 == client2) ? true : false;
            //тест интерфейса IComparable
            //это метод Sort



            List<Product> products = new List<Product>();
            Product pr1 = new Product(1,"grape",25);
            products.Add(pr1);
            bool result1 = products.Contains(pr1);
            Product pr2 = new Product(1, "apple", 63);
            bool result2 = (pr1 == pr2) ? true : false;
        }
    }
}
