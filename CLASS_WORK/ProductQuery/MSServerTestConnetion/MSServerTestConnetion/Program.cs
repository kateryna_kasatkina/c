﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration;//для работы с файлом App.config - из System.configuration.dll
using System.Data.SqlClient;// из драйвера MS SQL Server-а
using System.Data.SqlTypes;//System.Data.dll

using MyDataBaseClasses;

namespace MSServerTestConnetion
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = @"Data Source=DESKTOP-VTMNLJ6;Initial Catalog=db28pr6;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                // Build the command to SQL
                StringBuilder sqltext = new StringBuilder();
                sqltext.Append(@"SELECT TOP 1000 [PRODUCTS_ID],[PRODUCTS_NAME],[PRICE] FROM[db28pr6].[dbo].[PRODUCTS]");
              
                
                using (SqlCommand command = new SqlCommand(sqltext.ToString(), con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())  // Read line 
                    {
                        // Parse line
                        int productId = reader.GetInt32(0);
                        string productName = reader["PRODUCTS_NAME"].ToString();
                        int price = int.Parse(reader["PRICE"].ToString());

                        Console.WriteLine($"productId = {productId}, productName = {productName}, price = {price}");
                    }
                }

                //string clientsSelect = Client.getAllClientSelect();
                //using (SqlCommand command = new SqlCommand(clientsSelect, con))
                //using (SqlDataReader reader = command.ExecuteReader())
                //{
                //    List<Client> customers = new List<Client>();
                //    while (reader.Read())
                //    {
                //        string name = reader["CLIENTS_NAME"].ToString();//reader.GetString(1);
                //        string phone = reader["CLIENTS_PHONE"].ToString();//reader.GetString(2);
                //        string email = reader["CLIENTS_MAIL"].ToString();//DateTime datevalue = reader.GetDateTime(2);
                //        string id = reader["CLIENTS_ID"].ToString();
                //        int client_id = Convert.ToInt32(id);
                //        Client client = new Client(client_id, name, phone, email);
                //        customers.Add(client);
                //        Console.WriteLine("поля из выборки {0} {1} {2}", id, name, phone);
                //        Console.WriteLine("создан client: {0}", client);
                //    }
                //}
                ////

                // READER + PARAMETER
                //using (SqlConnection conn = new SqlConnection(connectionString))
                //{
                //    SqlCommand cmdSelect = new SqlCommand();
                //    SqlParameter currencyCodeParam = new SqlParameter("@pCultureID", System.Data.SqlDbType.NVarChar);
                //    string cultureIdCode = "ar";
                //    currencyCodeParam.Value = cultureIdCode;
                //    cmdSelect.Parameters.Add(currencyCodeParam);
                //    cmdSelect.CommandText = "SELECT TOP 1000 [CultureID],[Name],[ModifiedDate] FROM [AdventureWorks2008].[Production].[Culture] where [CultureID]=@pCultureID";
                //    cmdSelect.Connection = conn;
                //    conn.Open();
                //    SqlDataReader reader = cmdSelect.ExecuteReader();
                //    while (reader.Read())
                //    {
                //        //int v1 = reader.GetInt32(0);
                //        string v1 = reader.GetString(1);
                //        string v2 = reader.GetString(2);
                //        DateTime v3 = reader.GetDateTime(3);
                //        Console.WriteLine("{0} {1} {2}", v1, v2, v3);
                //    }
                //    reader.Close();
                //    conn.Close();
                //}

                // STORED PROCEDURE WITH PARAMETERS 2
                //using (SqlConnection conn = new SqlConnection(connectionString))
                //{
                //    SqlCommand cmdStoredProc = new SqlCommand();
                //    SqlParameter inParam1 = new SqlParameter("@p1", System.Data.SqlDbType.Int);
                //    SqlParameter inParam2 = new SqlParameter("@p2", System.Data.SqlDbType.Int);
                //    inParam1.Value = "1";
                //    inParam2.Value = "2";

                //    SqlParameter outvalOUT_PARAM1 = new SqlParameter("@out_sum", System.Data.SqlDbType.VarChar, 200);
                //    outvalOUT_PARAM1.Direction = System.Data.ParameterDirection.Output;
                //    cmdStoredProc.Parameters.Add(outvalOUT_PARAM1);

                //    cmdStoredProc.Parameters.Add(inParam1);
                //    cmdStoredProc.Parameters.Add(inParam2);
                //    cmdStoredProc.CommandText = "PROC_FOR_TEST";                            // The name of the STORED PROCEDURE
                //    cmdStoredProc.CommandType = System.Data.CommandType.StoredProcedure;    // !!!! TYPE - STORED PROCEDURE

                //    cmdStoredProc.Connection = conn;
                //    conn.Open();
                //    cmdStoredProc.ExecuteNonQuery();
                //    Console.WriteLine("out value is {0}", outvalOUT_PARAM1.Value);
                //    conn.Close();
                //}

                // INSERT (ADD sales.currency)
                //using (SqlConnection conn = new SqlConnection(connectionString))
                //{
                //    SqlCommand InsertCurrencyCommand = new SqlCommand();
                //    SqlParameter currencyCodeParam = new SqlParameter("@CurrencyCode", System.Data.SqlDbType.NVarChar);
                //    SqlParameter nameParam = new SqlParameter("@Name", System.Data.SqlDbType.NVarChar);
                //    currencyCodeParam.Value = currencyCode;
                //    nameParam.Value = name;
                //    InsertCurrencyCommand.Parameters.Add(currencyCodeParam);
                //    InsertCurrencyCommand.Parameters.Add(nameParam);
                //    InsertCurrencyCommand.CommandText =
                //        "INSERT Sales.Currency (CurrencyCode, Name, ModifiedDate)" +
                //        " VALUES(@CurrencyCode, @Name, GetDate())";
                //    InsertCurrencyCommand.Connection = conn;
                //    conn.Open();
                //    InsertCurrencyCommand.ExecuteNonQuery();    // WE DO NOT NEED RESPONSE, JUST EXECUTE (SO, ADD THE LINE)
                //    conn.Close();
                //}

            }
        }
    }
}
