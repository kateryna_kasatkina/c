﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlTypes;
namespace NullableDBNullTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int? x = 10;
            if (x.HasValue)//if(x!=null)
            {
                System.Console.WriteLine(x.Value);
            }
            else
            {
                System.Console.WriteLine("Undefined");
            }
            #region Преобразования
            // int? n = null;
            //// int m1 = n;//error! compile
            // int m2 = (int)n;//exception
            // int m3 = n.Value;

            //


            #endregion

            int? c = null;
            //d=c? если значчение с null, то в переменную значимого типа d=-1
            int? d = c ?? -1;
            //упаковка в ссылочный тип, процес преобразования в тип совместимый с object
            bool? b = null;
            object o = b;//now is null
            b = false;
            int? i = 44;
            object bBoxed = b;
            object iBoxed = i;
            int Xx = 10;//int 4 bytes
            object xBoxed = Xx;//object 10 bytes




            string connectionString = @"Data Source=PC36-6-Z;Initial Catalog=db28pr6;Integrated Security=True";
            //1.Create connection
            //3.from connection take command or select
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();//!!!!!!!!!
                using (SqlCommand command = new SqlCommand("SELECT * FROM [db28pr6].[dbo].[CUSTOMERS]", con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        string nick = reader["NICK_NAME"].ToString();
                        string phone = reader["PHONE"].ToString();
                        string email = reader["EMAIL"].ToString();
                        string idMan = (string)reader["ID_MEN"];

                        //DateTime v4 = reader.GetDateTime(2);
                        Console.WriteLine($"{nick}, {phone} ,{email},{idMan}");
                    }
                }
            }
        }

        public static class MyExtensions
        {


        }
    }
}
