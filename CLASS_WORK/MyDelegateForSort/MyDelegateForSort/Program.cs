﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDelegateForSort
{
    class Program
    {
        #region simmple delegate
        public delegate bool MyCompareOperation(int a,int b);//объектно-ориентированный аналог
        
        public delegate void MyEventHandler(object sender, EventArgs e);
        //указателя на функцию
        public static bool isGreater(int a,int b)
        {
            return (a<b)?true:false;
        }
        public static void MyBubbleSort(int[] mas, 
            MyCompareOperation comparerOp)
        {
            for (int A = 1; A <= mas.Length - 1; A++ )
                for (int i = 0; i < mas.Length - A - i; i++)
                {
                    if (comparerOp(mas[i], mas[i + 1]))
                    {
                        int t = mas[i];
                        mas[i] = mas[i + 1];
                        mas[i + 1] = t;
                    }
                }
        }
        #endregion




        
        static void Main(string[] args)
        {
            ////int[] array = new int[] { 16, 4, 1, 2, 6, 7, 0, 12 };
            ////MyCompareOperation mydeligare = 
            ////    new MyCompareOperation(isGreater);
            ////MyBubbleSort(array, mydeligare);





            #region Standart delegates
            Action<int, int> op;//принимает до 16 параметров ничего не возвращает
            op = Add;
            Operation(10, 6, op);
            op = Substract;
            Operation(10, 6, op);
            Predicate<int> isPositive = delegate (int x) { return x > 0; };
            Console.WriteLine(isPositive(20));
            Console.WriteLine(isPositive(-20));
            Console.WriteLine();
            Func<int, double> MyFunction = factorial; //принимает до 16 параметров  возвращает результат
            Func<int, int, int, int> MyFunction2 = MySum;
            testFactorial(3, MyFunction);
            testFunctionMySum(1, 2, 3, MyFunction2);

            #endregion

            #region Simple Lambdas 
            //lambdas assings to functor or delegate
            MyFunction = x => x - x;
            testFactorial(3, MyFunction);
            testFunctionMySum(3, 2, 3, (x, y, z) => (x + y + z));

            Action message = () => { Console.WriteLine("Hello wrom Lambdas"); };
            message();

            Action<string, int> message2 = (name, age) => { Console.WriteLine("Hell {0}, are you {1} years old", name, age); };
            message2("Vasya", 12);

            Action<string, int> message3 = (name, age) =>
            {
                Console.WriteLine("Hell {0}", name);
                Console.WriteLine("are you {0}years old", age);
            };

            message3("test3", 12);


            TestAction(() => { Console.WriteLine("Hello wrom Lambdas"); }, (name, age) => { Console.WriteLine("Hell {0}, are you {1} years old", name, age); },
                (name, age) =>
                {
                    Console.WriteLine("Hell {0}", name);
                    Console.WriteLine("are you {0}years old", age);
                });
            #endregion



            //MyPerson person = new MyPerson { Name = "Tom", Age = 25 };
            //MyPerson person2 = new MyPerson() { Name = "Vasa", Age = 25 };

            MyPerson person3 = new MyPerson();
            Men person4 = new Men() { Name = "Petr" };//1.инициализзация всех полей, 
                                                      //2.потом конструктор по умолчанию,
                                                      //3.выполнение setter для тех свойств которые мы перечислили
            person3.Print();




            #region examle of using Lambdas
            //int[] array = new int[] { 1, 2, 5, 7, 8, 9, 0 };
            //printArray(array, x => x == 0);
            //Console.WriteLine();
            //printArray(array, x => x % 2 == 0);
            //Console.WriteLine();
            //printArray(array, x => x % 2 == 1);
            //Console.WriteLine();
            //printArray(array, x => x>5);
            //Console.WriteLine();
            #endregion
            Employee[] employees = new Employee[3];
            employees[0] = new Employee { Name = "Vasa", Age = 20, Salary = 1200.50 };
            employees[1] = new Employee { Name = "Petya", Age = 20, Salary = 1000.50 };
            employees[2] = new Employee { Name = "sergey", Age = 20, Salary = 800.50 };


           double s1= Employee.calcSalary(employees, e => e.Salary > 900);
            Console.WriteLine("Employee.calcSalary(employees, e => e.Salary > 900) :{0}", s1);
           double s2= Employee.calcSalary(employees, e => e.Name.Contains("P"));
            Console.WriteLine(" Employee.calcSalary employees, e => e.Name.Contains P :{0}", s2);

            Console.WriteLine("-----------------------------------------------------");

            MyEmployee[] employees1 = new MyEmployee[3];
            employees1[0] = new MyEmployee { Name = "Vasa", Age = 20, Salary = 1200.50 };
            employees1[1] = new MyEmployee { Name = "Petya", Age = 20, Salary = 1000.50 };
            employees1[2] = new MyEmployee { Name = "sergey", Age = 20, Salary = 800.50 };


            double s11 = MyEmployee.calcSalary(employees1, e => e.Salary > 900);
            Console.WriteLine("Employee.calcSalary(employees, e => e.Salary > 900) :{0}", s1);
            double s21 = MyEmployee.calcSalary(employees1, e => e.Name.Contains("P"));
            Console.WriteLine(" Employee.calcSalary employees, e => e.Name.Contains P :{0}", s2);

















            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
        public static void TestAction(Action message, Action<string, int> message2, Action<string, int> message3)
        {
            message();
            message2("test1", 12);
            message3("test2", 12);
        }

        private static void Substract(int x1, int x2)
        {
            Console.WriteLine("difference of numbers: " + (x1 - x2));
        }

        private static void Add(int x1, int x2)
        {
            Console.WriteLine("Sum of numbers: "+(x1+x2));
        }

        private static void Operation(int x1, int x2, Action<int, int> op)
        {
            if (x1 > x2) op(x1, x2);
        }
        static double factorial(int n)
        {
            double result = 1;
            for(int i=0;i<=n;i++)
            {
                result -= i;

            }
            return result;
        }
        static void testFactorial(int n,Func<int,double>op)
        {
            Console.WriteLine("Factorial {0}={1}",n,op(n));
        }
        static void testFunctionMySum(int a,int b,int c,Func<int,int,int,int>op)
        {
            Console.WriteLine("Factorial {0},{1},{2}={3}", a,b,c, op(a,b,c));
        }
        static int MySum(int a,int b, int c)
        {
            return a + b + c;
        }
        static void printArray(int[]array,Func<int,bool>op)
        {
            foreach(int i in array)
                if(op(i))
                Console.Write(i);
        }
    }

    public class MyPerson {
        private string _name;

        public string Name
        {
            get;
            set;
        }

        private int _age;

        public int Age
        {
            get;
            set;
        }
        //в момент инициализации выполняем инициализацию public свойств 
        public void Print() => Console.WriteLine("Name={0},Age={1}",Name,Age);
    }


    public class Employee:MyPerson
    {
        public double Salary { set; get; }

        public delegate bool MyCompareEmployee(Employee empl);//analog of Predicate<Employee> MyCompareEmployee

        public static double calcSalary(Employee[] list,MyCompareEmployee op)
        {
            double sum=0;
            foreach(Employee e in list)
            {
                if(op(e))
                sum += e.Salary;
            }
            return sum;
        }
    }

    public class MyEmployee:MyPerson
    {
        //with predicate
       // public delegate bool CompareMyEmployee(MyEmployee empl);//analog of Predicate<Employee> MyCompareEmployee
        public double Salary { set; get; }
        public static double calcSalary(MyEmployee[] list, Predicate<MyEmployee> op)
        {
            double sum = 0;
            foreach (MyEmployee e in list)
            {
                if (op(e))
                    sum += e.Salary;
            }
            return sum;
        }

        //public static double calcSalary(MyEmployee[] list, Func<MyEmployee,bool>op)
        //{
        //    double sum = 0;
        //    foreach (MyEmployee e in list)
        //    {
        //        if (op(e))
        //            sum += e.Salary;
        //    }
        //    return sum;
        //}

        
    }

    public class Men
    {
       // private string name = "NoName";
        private string name;
        public Men() { }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
    }

}
