﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICompareTest
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList temperatures = new ArrayList();
            Random rnd = new Random();
            //generate 10 temperatures between a and b
            int N = 10;//quantyty
            int a = 0;//градусы цельсия
            int b = 100;


            for(int ctr=1;ctr<=10;ctr++)
            {
                int degrees = rnd.Next(a,b);
                Temperature temp = new Temperature();
                temp.Fahrenheit = degrees;
                temperatures.Add(temp);

            }
            //для in должен быть реализован IEnumerable
            foreach(Temperature temp in temperatures)
            {
                Console.WriteLine(temp.Fahrenheit);
            }

            Console.WriteLine("-------------------------");
            temperatures.Sort();

            foreach (Temperature temp in temperatures)
            {
                Console.WriteLine(temp.Fahrenheit);
            }

            Console.WriteLine("Press any key");
            Console.ReadKey();
        }
        
    }
}
