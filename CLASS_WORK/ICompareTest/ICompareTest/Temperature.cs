﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICompareTest
{
   public class Temperature:IComparable
    {
        private double temperatureF;

        
        #region
        public  double Fahrenheit
        {
            get
            {
                return this.temperatureF;
            }

            set
            {
                this.TemperatureF = value;
            }
        }

        public double Celsius
        {
            get
            {
                return (this.TemperatureF - 32) * (5.0 / 9);

            }
            set
            {
                this.TemperatureF = (value * 9.0 / 5) + 32;

            }
        }

        public double TemperatureF
        {
            get
            {
                return temperatureF;
            }

            set
            {
                temperatureF = value;
            }
        }

      

        public int CompareTo(object obj)
        {
            if (obj == null)
                return 1;
            Temperature otherTemperature = obj as Temperature;//либо преобразование либо null без ex(преобразует и корректных наследников),   MyClass c=(MyClass)obj вызовет исключительную ситуацию
            if (otherTemperature != null)
                return this.temperatureF.CompareTo(otherTemperature.temperatureF);
            else
                throw new ArgumentException("object is not a temperature");
        }
    }
    #endregion
}
