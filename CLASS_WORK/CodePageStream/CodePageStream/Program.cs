﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CodePageStream
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime timeAction = DateTime.Now;
            StreamWriter stream = new StreamWriter("D:\\MyData1\\test1.txt", true, System.Text.Encoding.UTF8);
            stream.Write(timeAction.Day.ToString() + ".");
            stream.Write(timeAction.Month.ToString() + ".");
            stream.Write(timeAction.Year.ToString() + ".");
            stream.Write(timeAction.Hour.ToString() + ".");
            stream.Write(timeAction.Minute.ToString() + ".");
            stream.Write(timeAction.Second.ToString() + ".");
            stream.WriteLine("Запись в файл состоялась");
            stream.Close();

            string readPath = @"D:\\MyData1\\test1.txt";
            string writePath = @"D:\\MyData1\\newtest1.txt";
            string text = String.Empty;
            try
            {
                using (StreamReader sr = new StreamReader(readPath, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                }
                using (StreamWriter sw = new StreamWriter(writePath, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(text);
                }
                using (StreamWriter sw = new StreamWriter(writePath, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("Дозапись");
                    sw.Write(4.5);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }



            text = "Hello world";
            //write to file
            using (FileStream fstream = new FileStream(@"D:\\MyData1\note.dat", FileMode.OpenOrCreate))
            {
                //convert string to bites
                //ASCIIEncoding encoding = new ASCIIEncoding();
                //UTF32Encoding encoding = new UTF32Encoding();
               // Encoding encoding = Encoding.GetEncoding("iso-8559-1");
                byte[] input = Encoding.Default.GetBytes(text);//здесь кодировка операционной системы
                //write array of bites to file

                fstream.Write(input, 0, input.Length);
                Console.WriteLine("text was written");
                //перемещаем указатель в конец файла, до конца файла 5 байт

                fstream.Seek(-5, SeekOrigin.End);//минус 5 символов с конца потока
                //считаем четыре символа с текущей позиции
                byte[] output = new byte[4];
                fstream.Read(output, 0, output.Length);
                //декодируем байты в строку
                string textFromFile = Encoding.Default.GetString(output);
                Console.WriteLine($"text from file {textFromFile}");
                //заменим слово world for house
                string replaceText = "house";
                fstream.Seek(-5, SeekOrigin.End);//минус - символов с конца записи

                input = Encoding.Default.GetBytes(replaceText);//преобразование строки в бфйты
                fstream.Write(input, 0, input.Length);//запись в поток fstream ,там где был сдвиг
                //read all file
                //go to start of file
                fstream.Seek(0, SeekOrigin.Begin);
                output = new byte[fstream.Length];
                fstream.Read(output, 0, output.Length);
                //декодируем байты в строку
                textFromFile = Encoding.Default.GetString(output);
                Console.WriteLine($"text from file {textFromFile}");


            }

        }
    }
}
