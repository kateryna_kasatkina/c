﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace universalSort
{
    class Program
    {


        public delegate bool MyComparerOperation(int a, int b);

        public static bool is_Greater<T>(T a, T b)
            where T:System.IComparable<T>
        {
            return (a.CompareTo(b)>0) ? true : false;
        }


        public static void MyBubleSort(int[] mas, MyComparerOperation comparerOp)
        {
          

            for (int A = 1; A <= mas.Length - 1; A++)
                for (int i = 0; i < mas.Length - A - i; i++)
                {
                    if (comparerOp(mas[i], mas[i + 1]))
                    {
                       // Swap<int>(ref mas[i],ref  mas[i + 1]);
                        SwapIfGreater<int>(ref mas[i], ref mas[i + 1]);
                    }
                }

        }

        private static void Swap<T>(ref T lv,ref T rv)
        {
            T temp = lv;
            lv = rv;
            rv = temp;
        }

        public static void SwapIfGreater<T>(ref T lv, ref T rv)
            where T:System.IComparable<T>
        {
            T temp = lv;
            lv = rv;
            rv = temp;
        }

        static void Main(string[] args)
        {
            int[] mas=new int[10];
            Random r = new Random();
            for(int i=0;i<10;i++)
            {
                mas[i] = r.Next(1, 20);
            }

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(mas[i]);
            }

            Console.WriteLine();
            MyComparerOperation comparerOp = new MyComparerOperation(is_Greater);
            MyBubleSort(mas, comparerOp);
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(mas[i]);
            }



        }
    }
}
