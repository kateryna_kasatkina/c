﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO; //для работы с файловой системой
using System.Configuration; //для работы с конфигурациями App.config + подключить dll SystemConfiguration
using System.Xml;


namespace DiskDirectoryFileInfoTest
{
    public class Program
    {
        
        static void Main(string[] args)
        {
            //DrivesInfo();
            ////получение строки соединения, хранящейся в App.config:
            //string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            //string providerName = ConfigurationManager.ConnectionStrings[4].ProviderName; //индексация от 1
            //Console.WriteLine(connectionString);
            //Console.WriteLine(providerName);


            //using (XmlReader reader = XmlReader.Create("d:\\myconfig.xml"))
            //{
            //    while (reader.Read())
            //    {
            //        if (reader.IsStartElement())
            //        {
            //            if (reader.IsEmptyElement)
            //                Console.WriteLine("<{0}/>", reader.Name);
            //            else
            //            {
            //                Console.Write("<{0}> ", reader.Name);
            //                reader.Read(); // Read the start tag.
            //                if (reader.IsStartElement())  // Handle nested elements.
            //                    Console.WriteLine("\r\n<{0}>", reader.Name);
            //                if (reader.NodeType == XmlNodeType.Element) //if it is an element node
            //                {
            //                    if (reader.Name == "database")
            //                    {
            //                        string host = reader.GetAttribute("host");
            //                        string user = reader.GetAttribute("user");
            //                        string password = reader.GetAttribute("password");
            //                        Console.WriteLine(host);
            //                        Console.WriteLine(user);
            //                        Console.WriteLine(password);
            //                    }
            //                }
            //                Console.WriteLine(reader.ReadString());  //Read the text content of the element.
            //            }
            //        }
            //    }
            //}

            //using (XmlWriter writer = XmlWriter.Create("d:\\MyData\\myappconfig.xml"))
            //{
            //    writer.WriteStartDocument();
            //    writer.WriteRaw("\n");
            //    writer.WriteStartElement("TEST"); //корневой эл-т
            //    writer.WriteAttributeString("my_atr1", "data1"); //атрибут корневого эл-та
            //    writer.WriteAttributeString("my_atr2", "data2"); //атрибут корневого эл-та
            //    writer.WriteAttributeString("my_atr3", "data3"); //атрибут корневого эл-та
            //    writer.WriteRaw("\n"); 
            //    writer.WriteStartElement("TEST_HEAD"); //эл-т, вложенный в корневой эл-т
            //    writer.WriteElementString("elem1", "elemData1"); //elem1 вложен в TEST_HEAD и содержит текст elemData1
            //    writer.WriteRaw("\n");
            //    writer.WriteEndDocument(); //закрыли корневой эл-т, закончили писать док-т
            //}

            //using (XmlWriter writer = XmlWriter.Create("d:\\MyData\\configMyXML.xml"))
            //{
            //    writer.WriteStartDocument();
            //    writer.WriteRaw("\n");
            //    writer.WriteStartElement("config"); //корневой эл-т 
            //    writer.WriteRaw("\n");
            //    writer.WriteStartElement("database"); //эл-т, вложенный в корневой эл-т
            //    writer.WriteAttributeString("host", "localhost"); //атрибут database
            //    writer.WriteAttributeString("path", "D:\\DB\\test.gdb"); //атрибут database
            //    writer.WriteAttributeString("user", "user1"); //атрибут database
            //    writer.WriteAttributeString("password", "master1"); //атрибут database
            //    writer.WriteAttributeString("dialect", "3"); //атрибут database
            //    writer.WriteAttributeString("role", ""); //атрибут database
            //    writer.WriteAttributeString("coding", ""); //атрибут database
            //    writer.WriteRaw("\n");
            //    writer.WriteEndElement(); //закрываем эл-т database
            //    writer.WriteStartElement("report"); //эл-т, вложенный в корневой эл-т
            //    writer.WriteAttributeString("path", "D:\\Repfiles\\"); //атрибут report
            //    writer.WriteRaw("\n");
            //    writer.WriteEndDocument(); //закрыли корневой эл-т, закончили писать док-т
            //}

            //int[] arr = new int[5];
            //arr[0] = 1;
            //arr[1] = 2;
            //arr[2] = 3;
            //arr[3] = 4;
            //arr[4] = 5;
            
            //int sum = 0;
            //for (int i = 0; i < arr.Length; i++)
            //    sum += arr[i];
            //Console.WriteLine(sum);

            //sum = sumRec(arr);
            //Console.WriteLine(sum);

            //string fileName = "D:\\Step\\test\\files.xml";
            //using (XmlWriter writer = XmlWriter.Create(fileName))
            //{
            //    writer.WriteStartDocument();
            //    GetFiles("D:\\Step\\test", writer);
            //    writer.WriteEndDocument();
            //}
            //string[] files;
            //files = GetFiles("D:\\Step\\test");
            //foreach (string f in files)
            //    Console.WriteLine(f);

            Console.WriteLine("-----------------------------------");
            Console.WriteLine("new DelegateVSInterface.GetFileFilter(DelegateVSInterface.FilterFile)");
            foreach (string file in DelegateVSInterface.GetFiles(@"d:\\MyDataBases",
                new DelegateVSInterface.GetFileFilter(DelegateVSInterface.FilterFile)))
            {
                Console.WriteLine(file);
            }


           
            Console.WriteLine("new DelegateVSInterface.IGetFilesFilterDefRelease");
            foreach (string  file in DelegateVSInterface.GetFiles((@"d:\\MyDataBases"),
                new DelegateVSInterface.IGetFilesFilterDefRelease()))
            {
                Console.WriteLine(file);
            }
        }

        public static int sumRec (int[] arr, int index=0)
        {
            if (index == arr.Length)
                return 0;
            return arr[index] + sumRec (arr, ++index);
        }

        private static void DrivesInfo()
        {
            DriveInfo[] drives = DriveInfo.GetDrives();
            int i = 1;
            foreach (DriveInfo d in drives)
            {
                Console.Write("{0}. ", i);
                Console.WriteLine(string.Format("Имя диска: {0}", d.Name));
                Console.WriteLine(string.Format("Тип диска: {0}", d.DriveType));
                if (d.IsReady)
                {
                    Console.WriteLine(string.Format("Общий размер: {0}", d.TotalSize));
                    Console.WriteLine(string.Format("Свободное место: {0}", d.TotalFreeSpace));
                    Console.WriteLine(string.Format("Название диска: {0}", d.VolumeLabel));
                }
                i++;
            }
        }

        public static string[] GetFiles(string path)
        {
            ArrayList res = new ArrayList();
            foreach (string file in Directory.GetFiles(path))
                res.Add(file);
            foreach (string dir in Directory.GetDirectories(path))
                res.AddRange(GetFiles(dir));
            return (string[])res.ToArray(typeof(string));
        }

        public static string[] GetFiles(string path, XmlWriter writer)
        {
            ArrayList res = new ArrayList();
            writer.WriteRaw("\n");
            string[] rootDirNameParts = path.Split('\\');
            int rootDirNameIndex = rootDirNameParts.Length-1; 
            writer.WriteStartElement(rootDirNameParts[rootDirNameIndex]);
            writer.WriteRaw("\n");
            foreach (string file in Directory.GetFiles(path))
            {
                string[] fileNameParts = file.Split('\\');
                int fileNameIndex = fileNameParts.Length - 1;
                writer.WriteRaw(fileNameParts[fileNameIndex]);
                writer.WriteRaw("\n");
                res.Add(file);
            }
            foreach (string dir in Directory.GetDirectories(path))
                res.AddRange(GetFiles(dir, writer));
            writer.WriteEndElement();
            writer.WriteRaw("\n");
            return (string[])res.ToArray(typeof(string));
        }
    }
}
