﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiskDirectoryFileInfoTest
{
    public class DelegateVSInterface
    {
        public delegate bool GetFileFilter(string filename);
        public static bool FilterFile(string file)
        {
            //return file.GetCreationTime
            return file.Contains(".exe");
        }
        
        public static string[] GetFiles(string path,GetFileFilter filter)
        {
            ArrayList res = new ArrayList();

            foreach (string file in Directory.GetFiles(path))
            {
                if((filter==null||filter(file)))
                res.Add(file);
            }
                
            foreach (string dir in Directory.GetDirectories(path))
                res.AddRange(GetFiles(dir, filter));
            return (string[])res.ToArray(typeof(string));
        }
        public static string[] GetFiles(string path,IGetFilesFilter filter)
        {
            ArrayList res = new ArrayList();
            foreach (string file in Directory.GetFiles(path))
            {
                if ((filter == null || filter.Filter(file)))
                    res.Add(file);
            }

            foreach (string dir in Directory.GetDirectories(path))
                res.AddRange(GetFiles(dir, filter));
            return (string[])res.ToArray(typeof(string));
        }
        public interface IGetFilesFilter
        {
            bool Filter(string file);
        }

        public class IGetFilesFilterDefRelease : IGetFilesFilter
        {
            public virtual bool Filter(string file)
            {
                return file.Contains(".exe");
            }
        }
    }
}
