USE [AdventureWorks2008]

/*1. ������ ����������, �� ������� ���������� ����� �������� ��� � ����� � ������ ����������, �� ������� � ������*/
SELECT
EMPLOYEE.[JobTitle],
EmployeePayHistory.[PayFrequency]
FROM [HumanResources].[Employee] AS EMPLOYEE
LEFT JOIN [Person].[BusinessEntity] AS BusinessEntity
ON BusinessEntity.BusinessEntityID = EMPLOYEE.BusinessEntityID
LEFT JOIN [HumanResources].[EmployeePayHistory] AS EmployeePayHistory
ON EmployeePayHistory.BusinessEntityID = EMPLOYEE.BusinessEntityID
WHERE EmployeePayHistory.[PayFrequency] = 1
UNION 
SELECT
EMPLOYEE.[JobTitle],
EmployeePayHistory.[PayFrequency]
FROM [HumanResources].[Employee] AS EMPLOYEE
LEFT JOIN [Person].[BusinessEntity] AS BusinessEntity
ON BusinessEntity.BusinessEntityID = EMPLOYEE.BusinessEntityID
LEFT JOIN [HumanResources].[EmployeePayHistory] AS EmployeePayHistory
ON EmployeePayHistory.BusinessEntityID = EMPLOYEE.BusinessEntityID
WHERE EmployeePayHistory.[PayFrequency] = 2
ORDER BY EmployeePayHistory.[PayFrequency]