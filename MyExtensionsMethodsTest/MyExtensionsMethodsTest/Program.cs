﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyExtensionsMethodsTest
{
    public static class MyExtensions
    {
        public static string[] MySplit(this string str)
        {
            string[] words = str.Split(' ');
            return words;
        }

        public static int WordCount(this String str)
        {
            return MyStaticClass.WordCount(str);
        }
        public static int MultiplyBy(this int value, int multiplier)
        {
            return MyStaticClass.MultiplyBy(value, multiplier);
        }

        public static string ListToString(this IList list)
        {
            //StringBuilder result = new StringBuilder();
            //if(list.Count>0)
            //{
            //    result.Append(list[0].ToString());
            //    for (int i = 1; i < list.Count; i++)
            //    {
            //        result.AppendFormat(",{0}", list[i].ToString());
            //    }
            //}
            //return result.ToString();
            return MyStaticClass.ListToString(list);
        }

        
    }
    public static class MyStaticClass
    {
        public static string[] MySplit(string str)
        {
            string[] words = str.Split(' ');
            return words;
        }
        public static int WordCount( String str)
        {
            string[] words = str.Split(new char[] { ' ', '.', '?' }, StringSplitOptions.RemoveEmptyEntries);
            return words.Length;
        }
        public static int MultiplyBy(int value,int multiplier)
        {
            return value * multiplier;
        }
        public static string ListToString(IList list)
        {
            StringBuilder result = new StringBuilder();
            if (list.Count > 0)
            {
                result.Append(list[0].ToString());
                for (int i = 1; i < list.Count; i++)
                {
                    result.AppendFormat(",{0}", list[i].ToString());
                }
            }
            return result.ToString();
        }
    }

    public enum myColor
    {
        White,
        Red,
        Green,
        Blue,
        Orange
            //0,1,2,3,4
    }
    public enum MyColor:byte
    {
        White,//значение 0 типа byte
        Red,
        Green,
        Blue,
        Orange
       
    }
    public enum dayWeek
    {
        Mn,
        Tu,
        Wn,
        Th,
        Fr,
        St,
        Sn
        
    }


    class Program
    {
       //TEnum - это имя параметра в Generic(аналог T[])
       public static TEnum[] GetEnumValues<TEnum>()
            where TEnum:struct
        {
            return (TEnum[])Enum.GetValues(typeof(TEnum));
        }
       
        static void Main(string[] args)
        {
            string str = "My string for split";
            string[] words = MyStaticClass.MySplit(str);
            words = str.MySplit();
            int n = MyStaticClass.WordCount(str);
            n = str.WordCount();
            int res = MyStaticClass.MultiplyBy(n, 2);
            Console.WriteLine(res);
            string items = MyStaticClass.ListToString(words);
            Console.WriteLine(items);
            Console.WriteLine("-------------------------");

            myColor color = myColor.Orange;
            Console.WriteLine(color.ToString());
            string colorStr = color.ToString("G");//G-general format
            Console.WriteLine(colorStr);
            Console.WriteLine(color.ToString("D"));//D-decimal format
            Console.WriteLine(Enum.Format(typeof(myColor),3,"G"));

            dayWeek d = dayWeek.Mn;
            Console.WriteLine(d.ToString());
            string dstring = d.ToString("G");
            Console.WriteLine(dstring);
            Console.WriteLine(d.ToString("D"));
            Console.WriteLine(Enum.Format(typeof(dayWeek), 3, "G"));


            Console.WriteLine("Base type of enum{0}",Enum.GetUnderlyingType(typeof(MyColor)));
            Console.WriteLine("Base type of enum{0}", Enum.GetUnderlyingType(typeof(myColor)));
            MyColor[] colors = (MyColor[])Enum.GetValues(typeof(MyColor));//GetValues returns System.Array which should be modified
            foreach (MyColor col in colors)
            {
                Console.WriteLine(col);
            }

            MyColor[] colors2 = GetEnumValues<MyColor>();

            Console.WriteLine("Press any key");
            Console.ReadKey();
        }
    }
}
