﻿using MyObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClientsGListTestEventsConsol
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClient x = new MyClient("Bob", "Phone:12345601", "new1@gmail.com", 65, "Kyiv");
            MyAccount xAccount = new MyAccount(x, 2556.23, "45678");
            x.Accounts.Add(xAccount);
           
            xAccount.Added += AccountInfoHandler;
            xAccount.Withdrowed += AccountInfoHandler;
            xAccount.Puted(456.23);
            xAccount.WasTaken(300.256);
            xAccount.WasTaken(3000.256);

        }
        public static void AccountInfoHandler(string message, double sum,double Account)
        {
            Console.WriteLine(message + "sum: " + sum + " __________account state: " +Math.Round( Account,3));
        }
    }
}
