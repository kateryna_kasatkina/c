﻿using MyObjects;
using System.Collections.Generic;

namespace MyClientsGListTestConsol
{
    interface MyInterface
    {
      void PrintClients(IsClientDelegate del);
    }

    interface MyInterfaceCollection
    {
        void PrintClients(List<MyClient> list, IsClientDelegate del);
    }

    interface MyInterfaceByString
    {
        List<MyClient> PrintClientsByString(string region);
    }

}
