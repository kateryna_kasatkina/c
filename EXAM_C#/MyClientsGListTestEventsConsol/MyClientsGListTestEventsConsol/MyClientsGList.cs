﻿using MyObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MyClientsGListTestConsol
{ 

    public delegate bool IsClientDelegate(MyClient client);
    public delegate List<MyClient> RegionFilterDelegate(string region); 

    public class MyClientsGList : IEnumerable,ICloneable,MyInterface,MyInterfaceCollection,MyInterfaceByString
    {
#region Properties, fields and constructors
        private List<MyClient> _listOfClients = new List<MyClient>();

        public List<MyClient> ListOfClients
        {
            get { return _listOfClients; }
            set { _listOfClients = value; }
        }
        public MyClientsGList() { }
        public MyClientsGList(MyClient client)
        {
            ListOfClients.Add(client);
        }
#endregion
        public void PutTestData()
        {
            MyClient cl1 = new MyClient("Bob", "Phone:12345601", "new1@gmail.com", 65, "Kyiv");
            MyClient cl2 = new MyClient("Nill", "Phone:12345602", "new2@gmail.com", 46, "kharkiv");
            MyClient cl3 = new MyClient("Petya", "Phone:12345603", "new3@gmail.com", 47, "Poltava");
            MyClient cl4 = new MyClient("Alex", "Phone:12345604", "new4@gmail.com", 58, "Lviv");
            MyClient cl5 = new MyClient("Alex", "Phone:12345605", "new5@gmail.com", 18, "kharkiv");
            MyClient cl6 = new MyClient("Olga", "Phone:12345606", "new6@gmail.com", 51, "Kyiv");
            MyClient cl7 = new MyClient("Sara", "Phone:12345607", "new7@gmail.com", 38, "Kyiv");
            ListOfClients.Add(cl1);
            ListOfClients.Add(cl2);
            ListOfClients.Add(cl3);
            ListOfClients.Add(cl4);
            ListOfClients.Add(cl5);
            ListOfClients.Add(cl6);
            ListOfClients.Add(cl7);
        }
        public void Add(MyClient cl)
        {
            ListOfClients.Add(cl);
        }
        public void RemoveClient(MyClient cl)
        {
            ListOfClients.Remove(cl);
        }
        public List<MyClient> GetMyClients(int number)
        {
            if (number >= ListOfClients.Count||number<0)
                throw new IndexOutOfRangeException("collection does not contain member with such index");
            List<MyClient> methodList = new List<MyClient>();
            for(int i=0;i<number;i++)
            {
                methodList.Add(ListOfClients[i]);
            }
            return methodList;
        }
        public IEnumerator GetEnumerator()
        {
            return new MyClientEnumerator(ListOfClients);
        }
        public MyClientsGList ShallowCopy()
        {
            return (MyClientsGList)this.MemberwiseClone();

        }
        public object Clone()
        {
            return ShallowCopy();
        }
        public void PrintClients(IsClientDelegate del)
        {
            for (int i = 0; i <ListOfClients.Count; i++)
            {
                if (del(ListOfClients[i]))
                {
                    Console.WriteLine(ListOfClients[i]);
                }
            }
        }
        void MyInterface.PrintClients(IsClientDelegate del)
        {
             PrintClients(del);
        }
        public void PrintClients(List<MyClient> list, IsClientDelegate del)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (del(list[i]))
                {
                    Console.WriteLine(list[i]);
                }
            }
        }
        public List<MyClient> PrintClientsByString(string region)
        {
            List<MyClient> y = new List<MyClient>();
            for (int i = 0; i < ListOfClients.Count; i++)
            {
                if (String.Equals(region, ListOfClients[i].RegionInfo))
                {
                    Console.WriteLine(ListOfClients[i]);
                    y.Add(ListOfClients[i]);
                }
            }
            return y;
        }
        public void SortByName()
        {
            var test2 =ListOfClients
                            .OrderBy(c => c.Name);
            foreach (var item in test2)
            {
                Console.WriteLine(item);
            }
        }
        public void SortByNameAndAge()
        {
            var test2 = ListOfClients
                            .OrderBy(c => c.Name)
                            .OrderBy(c => c.Age);
            foreach (var item in test2)
            {
                Console.WriteLine(item);
            }
        }
        public void SortByPhone()
        {
            var test2 =ListOfClients
                            .OrderBy(c => c.Phone);
            foreach (var item in test2)
            {
                Console.WriteLine(item);
            }
        }
        public int AverageAgeSum()
        {
            int x=ListOfClients.Sum(c =>c.Age);
            int result = x / (ListOfClients.Count);
            Console.WriteLine(result);
            return result;
        }
        public double AverageAge()
        {
            double x = ListOfClients.Average(c => c.Age);
            Console.WriteLine(Math.Round(x, 2));
            return x;
        }
        public double AverageAgeInRegion(string region)
        {
            var x = ListOfClients.Where(c => c.RegionInfo == region);
            double result= x.Average(c => c.Age);
            Console.WriteLine(Math.Round(result, 2));
            return result;
        }
    }



    public class MyClientEnumerator : IEnumerator<MyClient>
    {
        private List<MyClient> _collection;
        private int curIndex;
        private MyClient curClient;

        public MyClientEnumerator(List<MyClient> aCollection)
        {
            _collection = aCollection;
        }
        public MyClient Current
        {
            get { return curClient; }
        }

        object IEnumerator.Current
        {
            get { return Current; }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool MoveNext()
        {
            //Avoids going beyond the end of the collection.
            if (++curIndex >= _collection.Count)
            {
                return false;
            }
            else
            {
                // Set current box to next item in collection.
                curClient = _collection[curIndex];
            }
            return true;
        }


        public void Reset()
        {
            curIndex = -1;
        }
    }
}
