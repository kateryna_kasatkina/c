﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassesTestConsolApp
{
    class OrderLineRepository:IList<OrderLine>
    {
        private List<OrderLine> _orderLines;

        public OrderLineRepository()
        {
            _orderLines = new List<OrderLine>();
        }

        public List<OrderLine> OrderLines { get => _orderLines; set => _orderLines = value; }

        public OrderLine this[int index]
        {
            get { return _orderLines[index]; }
            set { _orderLines[index] = value; }
        }

        public int Count
        {
            get { return _orderLines.Count; }
        }

        public bool IsReadOnly
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public void Add(OrderLine item)
        {
            _orderLines.Add(item);
        }


        public void Clear()
        {
            _orderLines.Clear();
        }

        public bool Contains(OrderLine item)
        {
            return _orderLines.Contains(item);
        }

        public void CopyTo(OrderLine[] array, int arrayIndex)
        {
            _orderLines.CopyTo(array, arrayIndex);
        }

        public IEnumerator<OrderLine> GetEnumerator()
        {
            return _orderLines.GetEnumerator();
        }

        public int IndexOf(OrderLine item)
        {
            return _orderLines.IndexOf(item);
        }

        public void Insert(int index, OrderLine item)
        {
            _orderLines.Insert(index, item);
        }

        public bool Remove(OrderLine item)
        {
            return _orderLines.Remove(item);
        }

        public void RemoveAt(int index)
        {
            _orderLines.RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }


        public OrderLineRepository RetrieveOrderLines(OrderLineRepository olr)
        {
            string connectionString = @"Data Source=DESKTOP-VTMNLJ6;Initial Catalog=db28pr6;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                // Build the command to SQL
                StringBuilder sqltext = new StringBuilder();
                sqltext.Append(@"SELECT TOP 1000 [ORDERS_POSITIONS_ID],[ORDERS_ID],[PRODUCTS_ID],[PRICE],[ITEM_COUNT] FROM[db28pr6EX].[dbo].[ORDERS_POSITIONS]");


                using (SqlCommand command = new SqlCommand(sqltext.ToString(), con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())  // Read line 
                    {
                        // Parse line
                        int orderPositionId = reader.GetInt32(0);
                        int ordersId = reader.GetInt32(0);
                        int productId = reader.GetInt32(0);
                        int price = reader.GetInt32(0);
                        int itemCount = reader.GetInt32(0);
                        
                        OrderLine nextPr = new OrderLine(orderPositionId, ordersId, productId, price, itemCount);
                        olr.Add(nextPr);
                       
                    }
                }
            }
            return olr;
        }


        public void ShowOrderLinesRepository()
        {
            for (int i = 0; i < _orderLines.Count; i++)
                Console.WriteLine(_orderLines[i]);
        }
    }
}
