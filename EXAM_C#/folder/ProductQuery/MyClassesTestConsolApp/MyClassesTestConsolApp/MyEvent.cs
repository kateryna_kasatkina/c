﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassesTestConsolApp
{
   public class MyEvent
    {

        //обьявление использует делегат EventHandler
        
        static void handlerAdd(object sender, EventArgs e)
        {
            Console.WriteLine("Событие произошло");
            Console.WriteLine("the object of: " + sender + " was added");
        }
       

    }
}
