﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataBaseClasses
{
    public class Client : IEquatable<Client>, IComparable, ICloneable
    {

        private int _clientsId;
        public int ID
        {
            get { return _clientsId; }
            set { _clientsId = value; }
        }
        private string _clientsName;
        public string Name
        {
            get { return _clientsName; }
            set { _clientsName = value; }
        }
        private string _clientsPhone;
        public string Phone
        {
            get { return _clientsPhone; }
            set { _clientsPhone = value; }
        }
        private string _clientsMail;
        public string Mail
        {
            get { return _clientsMail; }
            set { _clientsMail = value; }
        }
        protected Client() { }
        public Client(int clients_id, string clients_name, string clients_phone, string clients_mail)
        {
            this._clientsId = clients_id;
            this._clientsName = clients_name;
            this._clientsPhone = clients_phone;
            this._clientsMail = clients_mail;
        }
      
        #region Equals Implementation
        public bool Equals(Client other)//реализация интерфейса IEquatable
        {
            if (other == null)
                return false;
            if (this._clientsId == other._clientsId)
                return true;
            else
                return false;
        }
        public override bool Equals(Object obj)//реализация System.Object
        {
            if (obj == null)
                return false;
            Client clientObj = obj as Client;
            if (clientObj == null)
                return false;
            else
                return Equals(clientObj);//вызов реализации для интерфейса-строгий тип
        }
        public override int GetHashCode()//из System.Object для Dictionary
        {
            return this._clientsId.GetHashCode();
            //должа основываться на полях класса, которые задействованы в Equals
            //из System.Object
        }
        //операторы сравнения "==" и "!=" основываются на Equals из System.Object
        public static bool operator ==(Client client1, Client client2)
        {
            if (((object)client1) == null || ((object)client2) == null)
                return Object.Equals(client1, client2);

            return client1.Equals(client2);
        }
        public static bool operator !=(Client client1, Client client2)
        {
            if (((object)client1) == null || ((object)client2) == null)
                return !Object.Equals(client1, client2);

            return !(client1.Equals(client2));
        }
        #endregion
        #region реализация IComparable
        public int CompareTo(object obj)//аналог int res = String.Compare(str, str2);
        {
            if (obj == null) return 1;

            Client otherClient = obj as Client;
            if (otherClient != null)
                return this._clientsId.CompareTo(otherClient._clientsId);
            else
                throw new ArgumentException("Object is not a Client");
        }
        #endregion
        #region Clonable
        public Client ShallowCopy()//аналог String clone = (string)str.Clone();//удвоение ссылок:
        {
            return (Client)this.MemberwiseClone();//не зарагивает ссылочные типы
            //внутри нашего объекта - поддержка клонирования .Net-ом
        }
        public object Clone()
        {
            return ShallowCopy();
        }
        #endregion

        public static string getAllClientSelect()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select ");
            sb.Append(" [dbo].[CLIENTS].[CLIENTS_ID], ");
            sb.Append(" [dbo].[CLIENTS].[CLIENTS_NAME] , ");
            sb.Append(" [dbo].[CLIENTS].[CLIENTS_PHONE] , ");
            sb.Append(" [dbo].[CLIENTS].[CLIENTS_MAIL] ");
            sb.Append(" from  ");
            sb.Append(" [dbo].[CLIENTS] ");
            return sb.ToString();
        }

        public override string ToString()
        {
            return String.Format($"{_clientsId}, {_clientsName}, {_clientsPhone}, {_clientsMail}");
        }
    }
}
