﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassesTestConsolApp
{
    class ProductsRepository : IList<Product>
    {
        private List<Product> _products;

        public ProductsRepository()
        {
            _products = new List<Product>();
        }

        public List<Product> Products { get => _products; set => _products = value; }

        public Product this[int index]
        {
            get { return _products[index]; }
            set { _products[index] = value; }
        }

        public int Count
        {
            get { return _products.Count; }
        }

        public bool IsReadOnly
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public void Add(Product item)
        {
            _products.Add(item);
        }


        public void Clear()
        {
            _products.Clear();
        }

        public bool Contains(Product item)
        {
            return _products.Contains(item);
        }

        public void CopyTo(Product[] array, int arrayIndex)
        {
            _products.CopyTo(array, arrayIndex);
        }

        public IEnumerator<Product> GetEnumerator()
        {
            return _products.GetEnumerator();
        }

        public int IndexOf(Product item)
        {
            return _products.IndexOf(item);
        }

        public void Insert(int index, Product item)
        {
            _products.Insert(index, item);
        }

        public bool Remove(Product item)
        {
            return _products.Remove(item);
        }

        public void RemoveAt(int index)
        {
            _products.RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }




        public ProductsRepository RetrieveProducts(ProductsRepository pr)
        {
            //ProductsRepository pr = new ProductsRepository();
            string connectionString = @"Data Source=DESKTOP-VTMNLJ6;Initial Catalog=db28pr6;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                // Build the command to SQL
                StringBuilder sqltext = new StringBuilder();
                sqltext.Append(@"SELECT TOP 1000 [PRODUCTS_ID],[PRODUCTS_NAME],[PRICE] FROM[db28pr6].[dbo].[PRODUCTS]");


                using (SqlCommand command = new SqlCommand(sqltext.ToString(), con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())  // Read line 
                    {
                        // Parse line
                        int productId = reader.GetInt32(0);
                        string productName = reader["PRODUCTS_NAME"].ToString();
                        int price = int.Parse(reader["PRICE"].ToString());
                        Product nextPr = new Product(productId, productName, price);
                        pr.Add(nextPr);
                       // Console.WriteLine($"productId = {productId}, productName = {productName}, price = {price}");
                    }
                }
            }
            return pr;
        }


        public void ShowProductsRepository()
        {
            for(int i=0;i<_products.Count;i++)
                Console.WriteLine(_products[i]);
        }
    }
}
