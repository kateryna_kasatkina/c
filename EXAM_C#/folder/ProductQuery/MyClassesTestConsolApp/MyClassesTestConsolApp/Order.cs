﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassesTestConsolApp
{
    public class Order: IEquatable<Order>, IComparable, ICloneable
    {
        
        private string _orderDescription;
        public string OrderDescription
        {
            get { return _orderDescription; }
            set { _orderDescription = value; }
        }

        private string _orderDate;
        public string OrderDate
        {
            get { return _orderDate; }
            set { _orderDate = value; }
        }

        private int _totalCost;
        public int TotalCost
        {
            get { return _totalCost; }
            set { _totalCost = value; }
        }
        private int _clientId;
        public int ClientId
        {
            get { return _clientId; }
            set { _clientId = value; }
        }

        private int _orderId;
        public int OrderId
        {
            get { return _orderId; }
            set { _orderId = value; }
        }
        

        protected Order() { }
        public Order(int aOrderId, string aOrderDescription, string aOrderDate, int aTotalCost, int aClientId)
        {
            this._orderId = aOrderId;
            this._orderDescription = aOrderDescription;
            this._orderDate = aOrderDate;
            this._totalCost = aTotalCost;
            this._clientId = aClientId;
        }

        public bool Equals(Order other)
        {
            if (other == null)
                return false;
            if (this._orderId == other._orderId)
                return true;
            else
                return false;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            Product clientObj = obj as Product;
            if (clientObj == null)
                return false;
            else
                return Equals(clientObj);
        }
        public override int GetHashCode()
        {
            return this._orderId.GetHashCode();
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Order otherClient = obj as Order;
            if (otherClient != null)
                return this._orderId.CompareTo(otherClient._orderId);
            else
                throw new ArgumentException("Object is not a Order");
        }

        

        public static bool operator ==(Order or1, Order or2)
        {
            if (((object)or1) == null || ((object)or2) == null)
                return Object.Equals(or1, or2);

            return or1.Equals(or2);
        }
        public static bool operator !=(Order or1, Order or2)
        {
            if (((object)or1) == null || ((object)or2) == null)
                return Object.Equals(or1, or2);

            return or1.Equals(or2);
        }
        public Order ShallowCopy()
        {
            return (Order)this.MemberwiseClone();

        }
        public object Clone()
        {
            return ShallowCopy();
        }


        public static string getAllOrderSelect()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select ");
            sb.Append(" [dbo].[ORDERS].[ORDERS_ID], ");
            sb.Append(" [dbo].[ORDERS].[DESCRIPTION] , ");
            sb.Append("[dbo].[ORDERS].[ORDERS_DATE], ");
            sb.Append("[dbo].[ORDERS].[TOTAL_COSTS], ");
            sb.Append("[dbo].[ORDERS].[CLIENTS_ID], ");
            sb.Append(" from  ");
            sb.Append(" [dbo].[ORDERS] ");
            return sb.ToString();
        }

        public override string ToString()
        {
            return String.Format($"{_orderId}, {_orderDescription}, {_orderDate}, {_totalCost}, {_clientId}");
        }
    }
}
