﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassesTestConsolApp
{
   public class Acount: IEquatable<Acount>, IComparable, ICloneable
    {
        private int _accountId;
        private string _description;
        private int _bankId;
        private int _accountSum;
        private int _account;
        
        protected Acount() { }
        public Acount(int aAccountId, string aDescription, int aBankId, int aAccountSum, int aAccount)
        {
            this._accountId = aAccountId;
            this._description = aDescription;
            this._bankId = aBankId;
            this._accountSum = aAccountSum;
        }

        public int AccountId { get; set; }
        public string Description { get; set; }
        public int BankId { get; set; }
        public int AccountSum { get; set; }
        public int Account { get; set; }

        public bool Equals(Acount other)
        {
            if (other == null)
                return false;
            if (this._accountId == other._accountId)
                return true;
            else
                return false;
        }

        public override bool Equals(Object obj)//реализация System.Object
        {
            if (obj == null)
                return false;
            Acount bankObj = obj as Acount;
            if (bankObj == null)
                return false;
            else
                return Equals(bankObj);
        }
        public override int GetHashCode()
        {
            return this._bankId.GetHashCode();

        }

        public static bool operator ==(Acount a1, Acount a2)
        {
            if (((object)a1) == null || ((object)a2) == null)
                return Object.Equals(a1, a2);

            return a1.Equals(a2);
        }
        public static bool operator !=(Acount a1, Acount a2)
        {
            if (((object)a1) == null || ((object)a2) == null)
                return Object.Equals(a1, a2);

            return !a1.Equals(a2);
        }
        public int CompareTo(object obj)
        {
            if (obj == null) return 1;
            Acount otherAccount = obj as Acount;
            if (otherAccount != null)
                return this._accountId.CompareTo(otherAccount._accountId);
            else
                throw new ArgumentException("Object is not a Acount");
        }
        public Acount ShallowCopy()
        {
            return (Acount)this.MemberwiseClone();
        }
        public object Clone()
        {
            return ShallowCopy();
        }
    }
}
