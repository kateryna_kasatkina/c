﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassesTestConsolApp
{
    class OrdersRepository:IList<Order>
    {

        

        private List<Order> _orders;

        public OrdersRepository()
        {
            _orders = new List<Order>();
        }

        public List<Order> Orders { get => _orders; set => _orders = value; }

        public Order this[int index]
        {
            get { return _orders[index]; }
            set { _orders[index] = value; }
        }

        public int Count
        {
            get { return _orders.Count; }
        }

        public bool IsReadOnly
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public void Add(Order item)
        {
            _orders.Add(item);
        }


        public void Clear()
        {
            _orders.Clear();
        }

        public bool Contains(Order item)
        {
            return _orders.Contains(item);
        }

        public void CopyTo(Order[] array, int arrayIndex)
        {
            _orders.CopyTo(array, arrayIndex);
        }

        public IEnumerator<Order> GetEnumerator()
        {
            return _orders.GetEnumerator();
        }

        public int IndexOf(Order item)
        {
            return _orders.IndexOf(item);
        }

        public void Insert(int index, Order item)
        {
            _orders.Insert(index, item);
        }

        public bool Remove(Order item)
        {
            return _orders.Remove(item);
        }

        public void RemoveAt(int index)
        {
            _orders.RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }




        public OrdersRepository RetrieveOrders(OrdersRepository or)
        {
            string connectionString = @"Data Source=DESKTOP-VTMNLJ6;Initial Catalog=db28pr6;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                // Build the command to SQL
                StringBuilder sqltext = new StringBuilder();
                sqltext.Append(@"SELECT TOP 1000 [ORDERS_ID],[DESCRIPTION],[ORDERS_DATE],[TOTAL_COSTS],[CLIENTS_ID] FROM[db28pr6EX].[dbo].[ORDERS]");
                using (SqlCommand command = new SqlCommand(sqltext.ToString(), con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())  // Read line 
                    {
                        // Parse line
                        int orderId = reader.GetInt32(0);
                        string description = reader["DESCRIPTION"].ToString();
                        string date = reader["ORDERS_DATE"].ToString();
                        int totalCosts = int.Parse(reader["TOTAL_COSTS"].ToString());
                        int clientId = int.Parse(reader["CLIENTS_ID"].ToString());
                        Order nextPr = new Order(orderId, description, date, totalCosts, clientId);
                        or.Add(nextPr);
                    }
                }
            }
            return or;
        }

        public void AddOrder( Order orderObject)
        {
            string connectionString = @"Data Source=DESKTOP-VTMNLJ6;Initial Catalog=db28pr6EX;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                // Build the command to SQL
                var sqltext = $"insert into [ORDERS] (DESCRIPTION,ORDERS_DATE,TOTAL_COSTS,CLIENTS_ID) " +
                              $"values ('{orderObject.OrderDescription}', '{orderObject.OrderDate}','{orderObject.TotalCost}','{orderObject.ClientId}')";

                using (SqlCommand command = new SqlCommand(sqltext, con))
                {
                    int insertedCount = command.ExecuteNonQuery();
                }
                
            }
           
            ClassEvent += new EventHandler(handlerAdd);
            OnSomeEvent();
        }

       
        public void DeleteOrder(int  id)
        {
            string connectionString = @"Data Source=DESKTOP-VTMNLJ6;Initial Catalog=db28pr6EX;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                // Build the command to SQL
                var sqltext = $"delete from [ORDERS] where ORDERS_ID={id}; ";

                using (SqlCommand command = new SqlCommand(sqltext, con))
                {
                    int insertedCount = command.ExecuteNonQuery();
                }

            }
            ClassEvent += new EventHandler(handlerDeleted);
            OnSomeEvent();
        }
        public void ShowOrdersRepository()
        {
            for (int i = 0; i < _orders.Count; i++)
                Console.WriteLine(_orders[i]);
        }

        public event EventHandler ClassEvent;
        public void OnSomeEvent()
        {
            if (ClassEvent != null)
                ClassEvent(this, EventArgs.Empty);
        }
        private void handlerAdd(object sender, EventArgs e)
        {
            Console.WriteLine("the order of: " + sender + " was added");
        }
        private void handlerDeleted(object sender, EventArgs e)
        {
            Console.WriteLine("the order of: " + sender + " was deleted");
        }
    }
}
