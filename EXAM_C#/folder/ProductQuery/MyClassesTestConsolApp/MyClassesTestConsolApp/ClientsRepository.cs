﻿using MyDataBaseClasses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassesTestConsolApp
{
    class ClientsRepository : IList<Client>
    {
        private List<Client> _clients;

        public ClientsRepository()
        {
            _clients = new List<Client>();
        }

        public List<Client> Clients { get => _clients; set => _clients = value; }

        public Client this[int index]
        {
            get { return _clients[index]; }
            set { _clients[index] = value; }
        }

        public int Count
        {
            get { return _clients.Count; }
        }

        public bool IsReadOnly
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public void Add(Client item)
        {
            _clients.Add(item);
        }


        public void Clear()
        {
            _clients.Clear();
        }

        public bool Contains(Client item)
        {
            return _clients.Contains(item);
        }

        public void CopyTo(Client[] array, int arrayIndex)
        {
            _clients.CopyTo(array, arrayIndex);
        }

        public IEnumerator<Client> GetEnumerator()
        {
            return _clients.GetEnumerator();
        }

        public int IndexOf(Client item)
        {
            return _clients.IndexOf(item);
        }

        public void Insert(int index, Client item)
        {
            _clients.Insert(index, item);
        }

        public bool Remove(Client item)
        {
            return _clients.Remove(item);
        }

        public void RemoveAt(int index)
        {
            _clients.RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /* public static string getAllClientSelect()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select ");
            sb.Append(" [dbo].[CLIENTS].[CLIENTS_ID], ");
            sb.Append(" [dbo].[CLIENTS].[CLIENTS_NAME] , ");
            sb.Append(" [dbo].[CLIENTS].[CLIENTS_PHONE] , ");
            sb.Append(" [dbo].[CLIENTS].[CLIENTS_MAIL] ");
            sb.Append(" from  ");
            sb.Append(" [dbo].[CLIENTS] ");
            return sb.ToString();
        }*/


        public ClientsRepository RetrieveClients(ClientsRepository cr)
        {
            //ProductsRepository pr = new ProductsRepository();
            string connectionString = @"Data Source=DESKTOP-VTMNLJ6;Initial Catalog=db28pr6;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                // Build the command to SQL
                StringBuilder sqltext = new StringBuilder();
                sqltext.Append(@"SELECT TOP 1000 [CLIENTS_ID],[CLIENTS_NAME],[CLIENTS_PHONE],[CLIENTS_MAIL] FROM[db28pr6].[dbo].[CLIENTS]");


                using (SqlCommand command = new SqlCommand(sqltext.ToString(), con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())  // Read line 
                    {
                        // Parse line
                        int clientId = reader.GetInt32(0);
                        string clientName = reader["CLIENTS_NAME"].ToString();
                        string clientPhone = reader["CLIENTS_PHONE"].ToString();
                        string clientMail = reader["CLIENTS_MAIL"].ToString();

                        Client nextCl = new Client(clientId, clientName, clientPhone, clientMail);
                        cr.Add(nextCl);
                        // Console.WriteLine($"productId = {productId}, productName = {productName}, price = {price}");
                    }
                }
            }
            return cr;
        }


        public void ShowClientsRepository()
        {
            for (int i = 0; i < _clients.Count; i++)
                Console.WriteLine(_clients[i]);
        }
    }
}
