﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassesTestConsolApp
{
    //IComparable, IComparer ,IEquatable<T>, операторы сравнения («==», «<», и обратные к ним), ICloneable. 
    public class OrderLine: IEquatable<OrderLine>, IComparable, ICloneable
    {
        private int _orderPositionId;
        public int OrderPositionId
        {
            get { return _orderPositionId; }
            set { _orderPositionId = value; }
        }

        private int _orderId;
        public int OrderId
        {
            get { return _orderId; }
            set { _orderId = value; }
        }

        private int _productId;
        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }

        private int _price;
        public int Price
        {
            get { return _price; }
            set { _price = value; }
        }

        private int _itemCount;
        public int ItemCount
        {
            get { return _itemCount; }
            set { _itemCount = value; }
        }
        private OrderLine() { }
        public OrderLine(int aOrderPositionId ,int aOrderId ,int aProductId ,int aPrice, int aItemCount)
        {
            this._orderPositionId = aOrderPositionId;
            this._orderId = aOrderId;
            this._productId = aProductId;
            this._price = aPrice;
            this._itemCount = aItemCount;
        }
        public bool Equals(OrderLine other)
        {

            if (other == null)
                return false;
            if (this._orderPositionId == other._orderPositionId)
                return true;
            else
                return false;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            OrderLine clientObj = obj as OrderLine;
            if (clientObj == null)
                return false;
            else
                return Equals(clientObj);
        }
        public override int GetHashCode()
        {
            return this._orderPositionId.GetHashCode();
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            OrderLine otherOrLine= obj as OrderLine;
            if (otherOrLine != null)
                return this._orderPositionId.CompareTo(otherOrLine._orderPositionId);
            else
                throw new ArgumentException("Object is not a OrderLine");
        }

        public OrderLine ShallowCopy()
        {
            return (OrderLine)this.MemberwiseClone();

        }

        public object Clone()
        {
            return ShallowCopy();
        }

        public static bool operator ==(OrderLine ol1, OrderLine ol2)
        {
            if (((object)ol1) == null || ((object)ol2) == null)
                return Object.Equals(ol1, ol2);

            return ol1.Equals(ol2);
        }
        public static bool operator !=(OrderLine ol1, OrderLine ol2)
        {
            if (((object)ol1) == null || ((object)ol2) == null)
                return Object.Equals(ol1, ol2);

            return !ol1.Equals(ol2);
        }

        public static string getAllOrderLineSelect()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select ");
            sb.Append(" [dbo].[ORDERS_POSITIONS].[ORDERS_POSITIONS_ID], ");
            sb.Append(" [dbo].[ORDERS_POSITIONS].[ORDERS_ID], ");
            sb.Append("[dbo].[ORDERS_POSITIONS].[PRODUCTS_ID], ");
            sb.Append("[dbo].[ORDERS_POSITIONS].[PRICE], ");
            sb.Append("[dbo].[ORDERS_POSITIONS].[ITEM_COUNT], ");
            sb.Append(" from  ");
            sb.Append(" [dbo].[ORDERS_POSITIONS] ");
            return sb.ToString();
        }


        public override string ToString()
        {
            return String.Format($"{_orderPositionId}, {_orderId}, {_productId}, {_price}, {_itemCount}");
        }
    }
}
