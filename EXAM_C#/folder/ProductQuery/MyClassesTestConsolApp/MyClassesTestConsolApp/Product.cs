﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassesTestConsolApp
{

    public class Product : IEquatable<Product>, IComparable, ICloneable
    {
        private int _productsId;
        public int ID
        {
            get { return _productsId; }
            set { _productsId = value; }
        }
        private string _productsName;
        public string Name
        {
            get { return _productsName; }
            set { _productsName = value; }
        }
        private int _productsPrice;
        public int Price
        {
            get { return _productsPrice; }
            set { _productsPrice = value; }
        }
        protected Product() { }
        public Product(int products_id, string products_name, int products_price)
        {
            this._productsId = products_id;
            this._productsName = products_name;
            this._productsPrice = products_price;
        }
        #region Equals Implementation
        public bool Equals(Product other)
        {
            if (other == null)
                return false;
            if (this._productsId == other._productsId)
                return true;
            else
                return false;
        }
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            Product clientObj = obj as Product;
            if (clientObj == null)
                return false;
            else
                return Equals(clientObj);
        }
        public override int GetHashCode()
        {
            return this._productsId.GetHashCode();
        }
        public static bool operator ==(Product pr1, Product pr2)
        {
            if (((object)pr1) == null || ((object)pr2) == null)
                return Object.Equals(pr1, pr2);

            return pr1.Equals(pr2);
        }
        public static bool operator !=(Product pr1, Product pr2)
        {
            if (((object)pr1) == null || ((object)pr2) == null)
                return !Object.Equals(pr1, pr2);

            return !pr1.Equals(pr2);
        }
        #endregion
        #region реализация IComparable
        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Product otherClient = obj as Product;
            if (otherClient != null)
                return this._productsId.CompareTo(otherClient._productsId);
            else
                throw new ArgumentException("Object is not a Product");
        }
        #endregion
        #region Clonable
        public Product ShallowCopy()
        {
            return (Product)this.MemberwiseClone();
                                                   
        }
        public object Clone()
        {
            return ShallowCopy();
        }
        #endregion

        public static string getAllProductsSelect()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select ");
            sb.Append(" [dbo].[PRODUCTS].[PRICE], ");
            sb.Append(" [dbo].[PRODUCTS].[PRODUCTS_NAME] , ");
            sb.Append("[dbo].[PRODUCTS].[PRODUCTS_ID ], ");
            sb.Append(" from  ");
            sb.Append(" [dbo].[PRODUCTS] ");
            return sb.ToString();
        }


        public override string ToString()
        {
            return String.Format($"{_productsId}, {_productsName}, {_productsPrice}");
        }
    }
}

