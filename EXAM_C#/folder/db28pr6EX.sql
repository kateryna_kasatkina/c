use master
IF EXISTS(SELECT 'True' FROM sys.databases WHERE name LIKE 'db28pr6EX') DROP DATABASE db28pr6EX
go
CREATE DATABASE db28pr6EX
go
use [db28pr6EX]


go
CREATE TABLE [dbo].[CLIENTS](
	[CLIENTS_ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[CLIENTS_NAME] [nvarchar](50) NOT NULL,
	[CLIENTS_PHONE] [nchar](20) NOT NULL,
	[CLIENTS_MAIL] [nchar](50) NOT NULL,
 )

 go
insert into [dbo].[CLIENTS] (CLIENTS_NAME,CLIENTS_PHONE, CLIENTS_MAIL) values (N'����� ��������',N'+380661596783',N'smolenko@e-mail.com');
insert into [dbo].[CLIENTS] (CLIENTS_NAME,CLIENTS_PHONE, CLIENTS_MAIL) values (N'���� ���',N'+380661596784',N'bah@e-mail.com');
insert into [dbo].[CLIENTS] (CLIENTS_NAME,CLIENTS_PHONE, CLIENTS_MAIL) values (N'������� ����',N'+380661596785',N'sudrov@e-mail.com');

 go
CREATE TABLE [dbo].[BANKS](
	[BANKS_ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[BANKS_NAME] [nvarchar](50) NOT NULL,
	[BANKS_INFO] [nvarchar](50) NOT NULL,
	
 )

 go
insert into [dbo].[BANKS] (BANKS_NAME,BANKS_INFO) values (N'�����', N'�������, �������');
insert into [dbo].[BANKS] (BANKS_NAME,BANKS_INFO) values (N'��������', N'�������, ����');
insert into [dbo].[BANKS] (BANKS_NAME,BANKS_INFO) values (N'������', N'�������, �������');
insert into [dbo].[BANKS] (BANKS_NAME,BANKS_INFO) values (N'DB����', N'�������, �������');


 go 
 CREATE TABLE [dbo].[ACCOUNTS](
 [ACCOUNTS_ID] [int] IDENTITY(1,1) primary key NOT NULL,
 [DESCRIPTION][nvarchar](100) NOT NULL,
 [BANKS_ID] [int]  NOT NULL,
 [ACCOUNTS_SUM] [int] NOT NULL,
 [ACCOUNT][int] NOT NULL
 )
 go
ALTER TABLE [dbo].[ACCOUNTS] ADD CONSTRAINT FK_ACCOUNTS_BANKS_ID 
FOREIGN KEY (BANKS_ID) REFERENCES [dbo].[BANKS](BANKS_ID);
 go
insert into [dbo].[ACCOUNTS] (DESCRIPTION,BANKS_ID,ACCOUNTS_SUM,ACCOUNT) values (N'���������� ����',1,5000,1253);
insert into [dbo].[ACCOUNTS] (DESCRIPTION,BANKS_ID,ACCOUNTS_SUM,ACCOUNT) values (N'������� ����',2,1255,521);
insert into [dbo].[ACCOUNTS] (DESCRIPTION, BANKS_ID,ACCOUNTS_SUM,ACCOUNT) values (N'��������� ����',3,1700,4878);
insert into [dbo].[ACCOUNTS] (DESCRIPTION, BANKS_ID,ACCOUNTS_SUM,ACCOUNT) values (N'���������� ����',1,740,5689);



 go 
 CREATE TABLE [dbo].[ACCOUNTS_TO_CLIENTS](
 [ACCOUNTS_TO_CLIENTS_ID][int] IDENTITY(1,1) primary key NOT NULL,
 [ACCOUNTS_ID] [int]  NOT NULL,
 [CLIENTS_ID] [int]  NOT NULL,
 [INDICATION][int]  NOT NULL,
 )
 go
ALTER TABLE [dbo].[ACCOUNTS_TO_CLIENTS] ADD CONSTRAINT FK_ACCOUNTS_TO_CLIENTS_ACCOUNTS_ID 
FOREIGN KEY (ACCOUNTS_ID) REFERENCES [dbo].[ACCOUNTS](ACCOUNTS_ID);
ALTER TABLE [dbo].[ACCOUNTS_TO_CLIENTS] ADD CONSTRAINT FK_ACCOUNTS_TO_CLIENTS_CLIENTS_ID
FOREIGN KEY (CLIENTS_ID) REFERENCES [dbo].[CLIENTS](CLIENTS_ID);
go 
insert into [dbo].[ACCOUNTS_TO_CLIENTS] (ACCOUNTS_TO_CLIENTS_ID,ACCOUNTS_ID, CLIENTS_ID,INDICATION) values (1,1);
insert into [dbo].[ACCOUNTS_TO_CLIENTS] (ACCOUNTS_TO_CLIENTS_ID,ACCOUNTS_ID, CLIENTS_ID,INDICATION) values (1,2);



 go 
 CREATE TABLE [dbo].[PRODUCTS](
 [PRODUCTS_ID][int] IDENTITY(1,1) primary key NOT NULL,
 [PRODUCTS_NAME] [nvarchar](50) NOT NULL,
 [PRICE] [int]  NOT NULL,
 )

 go
insert into [dbo].[PRODUCTS] (PRODUCTS_NAME, PRICE) 
values (N'�����',5);				 		
insert into [dbo].[PRODUCTS] (PRODUCTS_NAME, PRICE) 
values (N'������',30);				 		
insert into [dbo].[PRODUCTS] (PRODUCTS_NAME, PRICE) 
values (N'����',125);				 		
insert into [dbo].[PRODUCTS] (PRODUCTS_NAME, PRICE) 
values (N'����',450);				 		
insert into [dbo].[PRODUCTS] (PRODUCTS_NAME, PRICE) 
values (N'��������',45);				 				

 go 
 CREATE TABLE [dbo].[ORDERS](
 [ORDERS_ID][int] IDENTITY(1,1) primary key NOT NULL,
 [DESCRIPTION] [nvarchar](50) NOT NULL,
 [ORDERS_DATE] [date]  NOT NULL,
 [TOTAL_COSTS][int] NOT NULL,
 [CLIENTS_ID] [int] NOT NULL,
 )
 go
ALTER TABLE [dbo].[ORDERS] ADD CONSTRAINT FK_ORDERS_ACCOUNTS_CLIENTS_ID 
FOREIGN KEY (CLIENTS_ID) REFERENCES [dbo].[CLIENTS](CLIENTS_ID);
go
insert into [dbo].[ORDERS] (DESCRIPTION,ORDERS_DATE,TOTAL_COSTS,CLIENTS_ID) values (N'��������������� �����', '2017-12-03',1009,2);
insert into [dbo].[ORDERS] (DESCRIPTION,ORDERS_DATE,TOTAL_COSTS,CLIENTS_ID) values (N'���������� �����', '2017-12-04',758,3);
insert into [dbo].[ORDERS] (DESCRIPTION, ORDERS_DATE,TOTAL_COSTS,CLIENTS_ID) values (N'���������� �����','2017-12-12',2005,2);
insert into [dbo].[ORDERS] (DESCRIPTION,ORDERS_DATE,TOTAL_COSTS,CLIENTS_ID) values (N'���������� �����','2017-12-30',198,3);





 go 
 CREATE TABLE [dbo].[ORDERS_POSITIONS](
 [ORDERS_POSITIONS_ID][int] IDENTITY(1,1) primary key NOT NULL,
 [ORDERS_ID][int]  NOT NULL,
 [PRODUCTS_ID][int]  NOT NULL,
 [PRICE] [int]  NOT NULL,
 [ITEM_COUNT] [int]  NOT NULL,
 )

 go
ALTER TABLE [dbo].[ORDERS_POSITIONS] ADD CONSTRAINT FK_ORDERS_POSITIONS_ORDERS_ID
FOREIGN KEY (ORDERS_ID) REFERENCES [dbo].[ORDERS](ORDERS_ID);
ALTER TABLE [dbo].[ORDERS_POSITIONS] ADD CONSTRAINT FK_ORDERS_POSITIONS_PRODUCTS_ID
FOREIGN KEY (PRODUCTS_ID) REFERENCES [dbo].[PRODUCTS](PRODUCT_ID);
go

insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (1,1,5,40);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (1,2,30,4);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (1,3,125,3);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (1,4,450,2);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (1,5,45,1);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (2,1,5,10);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (2,2,30,40);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (3,3,40,20);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (3,4,70,20);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (3,5,40,20);


  go 
 CREATE TABLE [dbo].[DEPARTMENTS](
  [DEPARTMENTS_ID][int] IDENTITY(1,1) primary key NOT NULL,
  [DEPARTMENTS_NAME] [nvarchar](50) NOT NULL,
  [REGION_INFO] [nvarchar](50) NOT NULL,
 )
 go
insert into [dbo].[DEPARTMENTS](DEPARTMENTS_NAME,REGION_INFO) values (N'�����������', N'����������� �������');
insert into [dbo].[DEPARTMENTS](DEPARTMENTS_NAME,REGION_INFO) values (N'�������', N'����������� �������');
insert into [dbo].[DEPARTMENTS](DEPARTMENTS_NAME,REGION_INFO) values (N'��������', N'����������� �������');


 go 
 CREATE TABLE [dbo].[EMPLOYEES](
  [EMPLOYEES_ID][int] IDENTITY(1,1) primary key NOT NULL,
  [EMPLOYEES_NAME] [nvarchar](50) NOT NULL,
  [PHONE] [nchar](20) NOT NULL,
  [MAIL] [nchar](50) NOT NULL,
  [SALARY][int]NOT NULL,
  [DEPARTMENTS_ID][int]  NOT NULL,
  [POSITIONS_ID][int]  NOT NULL
 )

 go
ALTER TABLE [dbo].[EMPLOYEES] ADD CONSTRAINT FK_EMPLOYEES_DEPARTMENTS_ID
FOREIGN KEY (DEPARTMENTS_ID) REFERENCES [dbo].[DEPARTMENTS](DEPARTMENTS_ID);
ALTER TABLE [dbo].[EMPLOYEES] ADD CONSTRAINT FK_EMPLOYEES_POSITIONS_ID
FOREIGN KEY (POSITIONS_ID) REFERENCES [dbo].[POSITIONS](POSITIONS_ID);
go
insert into [dbo].[EMPLOYEES](EMPLOYEES_NAME,PHONE,MAIL,SALARY,DEPARTMENTS_ID,POSITIONS_ID) values (N'��������� ����',N'+38066456879',N'mail1@gmail.com',15000,1,1);
insert into [dbo].[EMPLOYEES](EMPLOYEES_NAME,PHONE,MAIL,SALARY,DEPARTMENTS_ID,POSITIONS_ID) values (N'������ �������',N'+38066456878',N'mail2@gmail.com',7000,2,2);
insert into [dbo].[EMPLOYEES](EMPLOYEES_NAME,PHONE,MAIL,SALARY,DEPARTMENTS_ID,POSITIONS_ID) values (N'��������� ���������',N'+38066456877',N'mail3@gmail.com',4800,3,3);
insert into [dbo].[EMPLOYEES](EMPLOYEES_NAME,PHONE,MAIL,SALARY,DEPARTMENTS_ID,POSITIONS_ID) values (N'������ ���',N'+38066456876',N'mail4@gmail.com',10000,2,4);



 go 
 CREATE TABLE [dbo].[POSITIONS](
  [POSITIONS_ID][int] IDENTITY(1,1) primary key NOT NULL,
  [POSITIONS_NAME] [nvarchar](50) NOT NULL
 )

 go
insert into [dbo].[POSITIONS](POSITIONS_NAME) values (N'��������');
insert into [dbo].[POSITIONS](POSITIONS_NAME) values (N'����������');
insert into [dbo].[POSITIONS](POSITIONS_NAME) values (N'��������');
insert into [dbo].[POSITIONS](POSITIONS_NAME) values (N'���������� ����');