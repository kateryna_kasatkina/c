﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CesarEncoderDecoderTestConsol
{
    class CesarDecoder
    {
        private int _step;
        public int Step
        {
            get { return _step; }
            set { _step = value; }
        }
        private CesarDecoder() { }
        public CesarDecoder(int aStep)
        {
            this.Step = aStep;
        }
        public void Decode(string nameFile)
        {
            //take data from file
            string text = String.Empty;
            //take data from file
            try
            {

                using (StreamReader sr = new StreamReader(nameFile, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }


            using (FileStream fstream = new FileStream(nameFile, FileMode.OpenOrCreate))
            {
                byte[] output = Encoding.Default.GetBytes(text);
                //Encoding
                for (int i = 0; i < output.Length; i++)
                {
                    Console.Write((char)output[i] + " " + output[i] + "-");
                    output[i] -= (byte)Step;
                    Console.WriteLine((char)output[i] + " " + output[i]);
                }

                // декодируем байты в строку
                string textFromFile = Encoding.Default.GetString(output);
                fstream.Close();
                //пишем строку в файл
                try
                {
                    using (StreamWriter sw = new StreamWriter(nameFile, false, System.Text.Encoding.Default))
                    {
                        sw.WriteLine(textFromFile);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                Console.WriteLine("Текст записан в файл");
            }
        }
    }
}
