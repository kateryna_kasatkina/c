﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CesarEncoderDecoderTestConsol
{
    public class CesarEncoder
    {
        private int _step;

        public int Step
        {
            get { return _step; }
            set { _step = value; }
        }
        private CesarEncoder() { }
        public CesarEncoder(int aStep)
        {
            this.Step = aStep;
        }
        public void Encode(string nameFile)
        {
            string text = String.Empty;
            //take data from file
            try
            {
               
                using (StreamReader sr = new StreamReader(nameFile, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }



            using (FileStream fstream = new FileStream(nameFile, FileMode.OpenOrCreate))
            {
                byte[] input = Encoding.Default.GetBytes(text);//Здесь Default это кодировка OC
                //Encoding
                for(int i=0;i<input.Length;i++)
                {
                    Console.Write((char)input[i]+" "+ input[i]+"-");
                    input[i] += (byte)Step;
                    Console.WriteLine((char)input[i] + " " + input[i]);
                }
                // запись массива байтов в файл
                fstream.Write(input, 0, input.Length);
                Console.WriteLine("Текст записан в файл");
            }
        }

    }
}
