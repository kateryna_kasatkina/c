﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciSequenceTestConsol
{
    class Program
    {
        static void Main(string[] args)
        {
            FibonacciSequence fibonacci = new FibonacciSequence(15);

            foreach ( int f in fibonacci)
            {
                Console.Write(f+" ");
            }
            Console.WriteLine();

            Console.WriteLine("========================================");
            foreach (int f in fibonacci.GetFirst(3))
            {
                Console.Write(f + " ");
            }
            Console.WriteLine();
        }
    }
}
