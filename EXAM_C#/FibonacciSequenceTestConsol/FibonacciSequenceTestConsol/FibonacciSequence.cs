﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciSequenceTestConsol
{
    class FibonacciSequence:IEnumerable
    {
        public int[] sequence;
        public int quantity { get; set; }

        public object Current => throw new NotImplementedException();

        private FibonacciSequence() { }
        public FibonacciSequence(int aQuantity)
        {
            int first = 1;
            int next = 1;
            this.quantity = aQuantity;
            sequence = new int[quantity];
            sequence[0] = 1;
            for(int i=1;i< quantity;i++)
            {
                int temp = next;
                sequence[i] = next;
                next = first + next;
                first = temp;
            }
        }
        public int[] GetFirst(int number)
        {
            int[] array = new int[number];
            for (int i = 0; i < number; i++)
                array[i] = sequence[i];
            return array;
        }
        public IEnumerator GetEnumerator()
        {
            return new FibonacciEnum(sequence);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

       
    }


    public class FibonacciEnum : IEnumerator
    {
        public int[] Sequence;
        int position = -1;

        public FibonacciEnum(int[] list)
        {
            Sequence = list;
        }

        public bool MoveNext()
        {
            position++;
            return (position < Sequence.Length);
        }

        public void Reset()
        {
            position = -1;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public int Current
        {
            get
            {
                try
                {
                    return Sequence[position];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }
    }
}
