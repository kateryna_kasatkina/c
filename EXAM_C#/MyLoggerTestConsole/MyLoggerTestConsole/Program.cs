﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MyLoggerTestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            MyTxtLogger logger2 = new MyTxtLogger();
            Console.WriteLine(logger2["D"]);
            Console.WriteLine(logger2[1]);
            logger2.WriteProtocol(logger2["D"], DateTime.Now.ToString(), "ТЕСТ ЛОГА", " MyLoggerTestConsole: Program.cs : Main", " Запись в лог на диске d");
            logger2.WriteProtocol(logger2[1], DateTime.Now.ToString(), "ТЕСТ ЛОГА", " MyLoggerTestConsole: Program.cs : Main", " Запись в лог на диске d");

            MyXmlLogger test1 = new MyXmlLogger();
            //test1.USE_D = true;
            test1.WriteProtocol(test1[1], DateTime.Now.ToString(), "ТЕСТ ЛОГА", " MyLoggerTestConsole: Program.cs : Main", " Запись в лог на диске d");
            test1.WriteProtocol(test1["D"], DateTime.Now.ToString(), "ТЕСТ ЛОГА", " MyLoggerTestConsole: Program.cs : Main", " Запись в лог на диске d");

        }
    }
}
