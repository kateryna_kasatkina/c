﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLoggerTestConsole
{
    interface IMyLogger
    {
        bool USE_C { get; set; }
       
        bool USE_D { get; set; }

        bool USE_E { get; set; }

        bool USE_F { get; set; }

        int GetLog(string value);

        string GetLog(int value);

        int this[string val]
        {
            get;
        }
        string this[int val]
        {
            get;
        }
        void WriteProtocol(string logname,string date_time, string action, string code_line, string log_text);

    }
}
