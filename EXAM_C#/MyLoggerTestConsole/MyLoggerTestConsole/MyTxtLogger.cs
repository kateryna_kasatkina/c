﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace MyLoggerTestConsole
{
    public class MyTxtLogger:IMyLogger,IDisposable
    {
        string[] logs = { @"c:\mylog.txt", @"d:\mylog.txt", @"e:\mylog.txt", @"f:\mylog.txt"};
        public bool USE_C
        {
            get { return USE_C; }
            set
            {
                USE_C = true;
                USE_D = false;
                USE_E = false;
                USE_F = false;
            }
        }
        public bool USE_D
        {
            get { return USE_D; }
            set
            {
                USE_C = false;
                USE_D = true;
                USE_E = false;
                USE_F = false;
            }
        }
        public bool USE_E
        {
            get { return USE_E; }
            set 
            {
                USE_C = false;
                USE_D = false;
                USE_E = true;
                USE_F = false;
            }
        }
        public bool USE_F
        {
            get { return USE_F; }
            set
            {
                USE_C = false;
                USE_D = false;
                USE_E = false;
                USE_F = true;
            }
        }
        public MyTxtLogger() { }
        // This method finds the log or returns -1
        private int GetLog(string value)
        {
            if (value.Equals("C"))
                return 0;
            if (value.Equals("D"))
                return 1;
            if (value.Equals("E"))
                return 2;
            if (value.Equals("F"))
                return 3;
            else
                throw new ArgumentOutOfRangeException("Such value was not found");
        }

        private string GetLog(int value)
        {
            if(value>(logs.Length-1)||value<0)
                throw new ArgumentOutOfRangeException("Such value was not found");
            else
                return logs[value];
        }

        int IMyLogger.GetLog(string value)
        {
            if (value.Equals("C"))
                return 0;
            if (value.Equals("D"))
                return 1;
            if (value.Equals("E"))
                return 2;
            if (value.Equals("F"))
                return 3;
            else
                throw new ArgumentOutOfRangeException("Such value was not found");
        }

        string IMyLogger.GetLog(int value)
        {
            if (value > (logs.Length - 1) || value < 0)
                throw new ArgumentOutOfRangeException("Such value was not found");
            else
                return logs[value];
        }
        // The get accessor returns an integer for a given string
        public int this[string val]
        {
            get
            {
                return (GetLog(val));
            }
        }

        public string this[int val]
        {
            get
            {
                return (GetLog(val));
            }
        }

        public void WriteProtocol(string date_time, string action, string code_line, string log_text)
        {
            string nameFile = String.Empty;
            if (USE_C == true)
            { nameFile = logs[0]; }
            if (USE_D == true)
            { nameFile = logs[1]; }
            if (USE_E == true)
            { nameFile = logs[2]; }
            if (USE_F== true)
            { nameFile = logs[3]; }
            try
            {
                using (StreamWriter sw = new StreamWriter(nameFile, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine(date_time);
                    sw.WriteLine(action);
                    sw.WriteLine(code_line);
                    sw.WriteLine(log_text);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }


        public void WriteProtocol(int number, string date_time, string action, string code_line, string log_text)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(logs[number], true, System.Text.Encoding.Default))
                {
                    sw.WriteLine(date_time);
                    sw.WriteLine(action);
                    sw.WriteLine(code_line);
                    sw.WriteLine(log_text);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public void WriteProtocol(string nameFile, string date_time, string action, string code_line, string log_text)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(nameFile, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine(date_time);
                    sw.WriteLine(action);
                    sw.WriteLine(code_line);
                    sw.WriteLine(log_text);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
