﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MyLoggerTestConsole
{
    public class MyXmlLogger : IMyLogger
    {
        string[] logs = { @"c:\mylog.xml", @"d:\mylog.xml", @"e:\mylog.xml", @"f:\mylog.xml" };
        private int counter = 1;
        public bool USE_C
        {
            get { return USE_C; }
            set
            {
                USE_C = true;
                USE_D = false;
                USE_E = false;
                USE_F = false;
            }
        }
        public bool USE_D
        {
            get { return USE_C; }
            set
            {
                USE_C = false;
                USE_D = true;
                USE_E = false;
                USE_F = false;
            }
        }
        public bool USE_E
        {
            get { return USE_C; }
            set
            {
                USE_C = false;
                USE_D = false;
                USE_E = true;
                USE_F = false;
            }
        }
        public bool USE_F
        {
            get { return USE_C; }
            set
            {
                USE_C = false;
                USE_D = false;
                USE_E = false;
                USE_F = true;
            }
        }
        public MyXmlLogger() { }

        // This method finds the log or returns -1
        int IMyLogger.GetLog(string value)
        {
            if (value.Equals("C"))
                return 0;
            if (value.Equals("D"))
                return 1;
            if (value.Equals("E"))
                return 2;
            if (value.Equals("F"))
                return 3;
            else
                throw new ArgumentOutOfRangeException("Such value was not found");
        }

        string IMyLogger.GetLog(int value)
        {
            if (value > (logs.Length - 1) || value < 0)
                throw new ArgumentOutOfRangeException("Such value was not found");
            else
                return logs[value];
        }

        private int GetLog(string value)
        {
            if (value.Equals("C"))
                return 0;
            if (value.Equals("D"))
                return 1;
            if (value.Equals("E"))
                return 2;
            if (value.Equals("F"))
                return 3;
            else
                throw new ArgumentOutOfRangeException("Such value was not found");
        }

        private string GetLog(int value)
        {
            if (value > (logs.Length - 1) || value < 0)
                throw new ArgumentOutOfRangeException("Such value was not found");
            else
                return logs[value];
        }

        public int this[string val]
        {
            get
            {
                return (GetLog(val));
            }
        }

        public string this[int val]
        {
            get
            {
                return (GetLog(val));
            }
        }


        public void WriteProtocol(string logname, string date_time, string action, string code_line, string log_text)
        {
            try
            {
                using (XmlWriter writer = XmlWriter.Create(logname))
                {
                    writer.WriteStartElement("log");
                    writer.WriteStartElement("log_row");
                    writer.WriteAttributeString("code_row", code_line);
                    writer.WriteAttributeString("action", action);
                    writer.WriteAttributeString("date_time", date_time);
                    writer.WriteAttributeString("rownum", counter.ToString());
                    writer.WriteString(log_text);
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
        }

        public void WriteProtocol(int number, string date_time, string action, string code_line, string log_text)
        {
            try
            {
                using (XmlWriter writer = XmlWriter.Create(logs[number]))
                {
                    writer.WriteStartElement("log");
                    writer.WriteStartElement("log_row");
                    writer.WriteAttributeString("code_row", code_line);
                    writer.WriteAttributeString("action", action);
                    writer.WriteAttributeString("date_time", date_time);
                    writer.WriteAttributeString("rownum", counter.ToString());
                    writer.WriteString(log_text);
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
    }
}
