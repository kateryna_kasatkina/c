﻿using MyObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MyClientsGListTestConsol.Program;

namespace MyClientsGListTestConsol
{
    interface MyInterface
    {
      void PrintClients(IsClientDelegate del);
    }

    interface MyInterfaceCollection
    {
        void PrintClients(List<MyClient> list, IsClientDelegate del);
    }

    interface MyInterfaceByString
    {
        List<MyClient> PrintClientsByString(string region);
    }

}
