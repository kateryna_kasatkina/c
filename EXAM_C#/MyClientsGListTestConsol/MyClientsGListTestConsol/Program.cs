﻿using MyObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClientsGListTestConsol
{
    class Program
    {

        static void Main(string[] args)
        {
            MyClientsGList list = new MyClientsGList();
            list.PutTestData();
            Console.WriteLine("Print first three clients:");
            foreach (MyClient item in list.GetMyClients(3))
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("===================================================================");
            Console.WriteLine("Print clients with location in Kharkiv using Lambda:");
            IsClientDelegate d1 = x => x.RegionInfo == "kharkiv";
            list.PrintClients(d1);
            Console.WriteLine();
            Console.WriteLine("===================================================================");
            Console.WriteLine("Print members of List<MyClient> using Lambda");
            list.PrintClients(list.ListOfClients, d1);
            Console.WriteLine();
            Console.WriteLine("===================================================================");
            Console.WriteLine("Creating an instance of delegate, using this delegate to receive List<MyClient> where regionInfo==string: ");
            RegionFilterDelegate del2 = new RegionFilterDelegate(list.PrintClientsByString);
            List<MyClient> test = del2("Poltava");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("===================================================================");
            Console.WriteLine("Sorting by name:");
            list.SortByName();
            Console.WriteLine();
            Console.WriteLine("===================================================================");
            Console.WriteLine("Sort by phone : ");
            list.SortByPhone();
            Console.WriteLine();
            Console.WriteLine("===================================================================");
            Console.WriteLine("Sorting by name and age:");
            list.SortByNameAndAge();
            Console.WriteLine();
            Console.WriteLine("===================================================================");
            Console.WriteLine("Average age of customers (use Sum): ");
            list.AverageAgeSum();
            Console.WriteLine();
            Console.WriteLine("===================================================================");
            Console.WriteLine("Average age of customers (use Average): ");
            list.AverageAge();
            Console.WriteLine();
            Console.WriteLine("===================================================================");
            Console.WriteLine("Average age of customers in Kyiv: ");
            list.AverageAgeInRegion("Kyiv");

        }

        
    }
   
}


