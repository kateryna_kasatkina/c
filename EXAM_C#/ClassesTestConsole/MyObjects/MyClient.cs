﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyObjects
{
   public class MyClient:Object
    {
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _phone;

        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }
        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        private int _age;
        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }
        private List<MyAccount> _accounts;

        public List<MyAccount> Accounts
        {
            get { return _accounts; }
            set { _accounts = value; }
        }
        
        private string _regionInfo;

        public string RegionInfo
        {
            get { return _regionInfo; }
            set { _regionInfo = value; }
        }

        private MyClient() { }
        public MyClient(string aName, string aPhone, string aEmail, int aAge, List<MyAccount> aAccounts, string aRegionInfo)
        {
            _name = aName;
            _phone = aPhone;
            _email = aEmail;
            _age = aAge;
            _accounts = aAccounts;
            _regionInfo = aRegionInfo;
        }
        public MyClient(string aName, string aPhone, string aEmail, int aAge,  string aRegionInfo)
        {
            _name = aName;
            _phone = aPhone;
            _email = aEmail;
            _age = aAge;
            _accounts = new List<MyAccount>();
            _regionInfo = aRegionInfo;
        }
        public bool Equals(MyClient other)
        {
            return (Equals(Name,other.Name)) && (Equals(Phone,other.Phone)) && (Equals(Email,other.Email))&&Age==other.Age&&Accounts==other.Accounts&&(Equals(RegionInfo,other.RegionInfo));
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            MyClient clientobj = obj as MyClient;
            if (clientobj == null)
                return false;
            else
                return Equals(clientobj);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ Phone.GetHashCode() ^ Email.GetHashCode()^Age.GetHashCode()^Accounts.GetHashCode();
        }
        public override string ToString()
        {
            return String.Format($"{Name},{Phone},{Email},{Age},{Accounts},{RegionInfo}");
        }
    }
}

