﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyObjects
{
    public class MyOrdersPosition
    {
        private MyProduct _product;
        public MyProduct Product
        {
            get { return _product; }
            set
            {
                if (value == null)
                    throw new ArgumentException(nameof(Product) + " cannot be null", nameof(Product));
                _product = value;
            }
        }
        private double _price;
        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }
        private int _count=0;
        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }
        private MyClient _client;
        public MyClient Client
        {
            get { return _client; }
            set
            {
                if (value == null)
                    throw new ArgumentException(nameof(Client) + " cannot be null", nameof(Client));
                _client = value;
            }
        }
        private MyOrdersPosition() { }
        public MyOrdersPosition(MyProduct aProduct, double aPrice, int aCount, MyClient aClient)
        {
            _product = aProduct;
            _price = aPrice;
            _count = aCount;
            _client = aClient;
        }


        public bool Equals(MyOrdersPosition other)
        {
            return Product == other.Product && Price == other.Price && Count == other.Count && Client == other.Client ;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            MyOrdersPosition MyOrdersPositionobj = obj as MyOrdersPosition;
            if (MyOrdersPositionobj == null)
                return false;
            else
                return Equals(MyOrdersPositionobj);
        }

        public override int GetHashCode()
        {
            return Product.GetHashCode() ^ Price.GetHashCode() ^ Count.GetHashCode() ^ Client.GetHashCode() ^ Client.GetHashCode();
        }
        public override string ToString()
        {
            return String.Format($"{Product},{Price},{Count},{Client}");
        }
    }
}
