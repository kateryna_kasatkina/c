﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyObjects
{
    public class MyProduct:Object
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                Trace.TraceInformation("Changing Name of Product from {0} to {1}", _name, value);
                _name = value;
            }
        }
        private double _price;
        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }
        private MyProduct() { }
        public MyProduct(string aName, double aPrice)
        {
            _name = aName;
            _price = aPrice;
        }


        public bool Equals(MyProduct other)
        {
            return (Equals(Name,other.Name))&& Price == other.Price;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            MyProduct MyProductobj = obj as MyProduct;
            if (MyProductobj == null)
                return false;
            else
                return Equals(MyProductobj);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ Price.GetHashCode();
        }
        public override string ToString()
        {
            return String.Format($"{Name},{Price}");
        }
    }
}
