﻿using System;
using System.Diagnostics;
namespace MyObjects
{
   
    public  class MyAccount:Object
    {
        public delegate void AccountStateHandler(string message,double sum,double Accounnt); //declaration of delegate
        public event AccountStateHandler Withdrowed; //declaration of event
        public event AccountStateHandler Added; //declaration of event


        private MyClient _client;
        public MyClient Client
        {
            get { return _client; }
            set { _client = value; }
        }
        private double _account;
        public double Account
        {
            get { return _account; }
            set { _account = value; }
        }
        private string _accountNumber;
        public string AccountNumber
        {
            get { return _accountNumber; }
            set
            {
                Trace.TraceInformation("Changing value of a from {0} to {1}", AccountNumber, value);
                _accountNumber = value;
            }
        }
        private MyAccount() { }
        public MyAccount(MyClient aClient, double aAccount, string aAccountNumber)
        {
            this._client = aClient;
            this._account = aAccount;
            this._accountNumber = aAccountNumber;
           
        }
        #region methods work with events
        public void Puted(double sum)
        {
            _account += sum;
            string str = "Money was puted :";
            if (Added != null)
            {
                Added(str,sum,Account); //generate event puted
            }
        }
        public void WasTaken(double  sum)
        {
            string str = String.Empty;
            if (_account >= sum)
            {
                _account -= sum;
                 str = "Money was withdrowed: ";
                if (Withdrowed != null)
                {
                    Withdrowed(str, sum, Account); //generate event withdrowed
                }
            }
            else
            {
                str= "Not enough money on account: ";
                if (Withdrowed != null)
                {
                    Withdrowed(str, sum, Account); //generate event withdrowed
                }
            }
        }
        #endregion

        public bool Equals(MyAccount other)
        {
            return this.Account== other.Account && this.Client == other.Client&&this.AccountNumber==other.AccountNumber;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (!(obj is MyAccount))
                return false;
            return Account == ((MyAccount)obj).Account && this.Client == ((MyAccount)obj).Client && this.AccountNumber == ((MyAccount)obj).AccountNumber;
        }

        public override int GetHashCode()
        {
            return Account.GetHashCode() ^ Client.GetHashCode()^AccountNumber.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format($"{Client},{Account},{AccountNumber}");
        }


    }
}
