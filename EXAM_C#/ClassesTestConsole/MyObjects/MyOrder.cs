﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyObjects
{
    public class MyOrder
    {
        private string _description;
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        private string _ordersDate;
        public string OrdersDate
        {
            get { return _ordersDate; }
            set { _ordersDate = value; }
        }
        private double _totalCost;
        public double TotalCost
        {
            get { return _totalCost; }
            set { _totalCost = value; }
        }
        private List<MyOrdersPosition> _myOrdersPositions;
        public List<MyOrdersPosition> MyOrdersPositions
        {
            get { return _myOrdersPositions; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException(nameof(MyOrdersPositions) + " cannot be null", nameof(MyOrdersPositions));
                }

                _myOrdersPositions = value;
            }
        }
        private MyClient _client;
        public MyClient Client
        {
            get { return _client; }
            set
            {
                if (value == null)
                    throw new ArgumentException(nameof(Client) + " cannot be null", nameof(Client));
                _client = value;
            }
        }

        private MyOrder() { }
        public MyOrder(string aDescription, string aOrdersDate, double aTotalCost, List<MyOrdersPosition> aMyOrdersPositions, MyClient aClient)
        {
            _description = aDescription;
            _ordersDate = aOrdersDate;
            _totalCost = aTotalCost;
            _myOrdersPositions = aMyOrdersPositions;
            _client = aClient;
        }


        public bool Equals(MyOrder other)
        {
            return (Equals(Description,other.Description)) && (Equals(OrdersDate,other.OrdersDate)) && TotalCost == other.TotalCost && MyOrdersPositions== other.MyOrdersPositions && Client== other.Client;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            MyOrder Orderobj = obj as MyOrder;
            if (Orderobj == null)
                return false;
            else
                return Equals(Orderobj);
        }

        public override int GetHashCode()
        {
            return Description.GetHashCode() ^ OrdersDate.GetHashCode() ^ TotalCost.GetHashCode() ^ MyOrdersPositions.GetHashCode() ^ Client.GetHashCode();
        }
        public override string ToString()
        {
            return String.Format($"{Description},{OrdersDate},{TotalCost},{MyOrdersPositions},{Client}");
        }
    }
}
