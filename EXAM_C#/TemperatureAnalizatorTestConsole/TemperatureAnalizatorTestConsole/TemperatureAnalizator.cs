﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemperatureAnalizatorTestConsole
{
    public class TemperatureAnalizator<T>
    {
        private List<T> _listOfTemperatures;
        public List<T> ListOfTemperatures
        {
            get { return _listOfTemperatures; }
            set { _listOfTemperatures = value; }
        }
        public TemperatureAnalizator()
        {
            _listOfTemperatures = new List<T>();

        }
        public void Add(T obj)
        {
            ListOfTemperatures.Add(obj);
        }
        public void Remove(T obj)
        {
            ListOfTemperatures.Remove(obj);
        }
        public void objectType()
        {
            Console.WriteLine("Type of object: " + typeof(T));
        }
        public bool FindValue(T x)
        {
            if (ListOfTemperatures.Contains(x))
                return true;
            return false;
        }
        public void FindMax(string fileName)
        {
            string[] arr = GetTextFromFile(fileName);
            double number;
            double[] w = new double[arr.Length - 1];
            for (int i = 0; i < arr.Length - 1; i++)
            {
                Double.TryParse(arr[i], out number);
                w[i] = number;
            }

            Console.WriteLine(w.Max());
        }
        public void FindMostRepetitiveElement(string fileName)
        {
            string[] arr = GetTextFromFile(fileName);
            IEnumerable<string> result = arr.GroupBy(a => a).OrderByDescending(x => x.Count()).First();
            foreach (var group in result)
            {
                Console.Write(group+"  ");
            }
            Console.WriteLine();

        }
        //public void findStableTemperature(string fileName)
        //{

        //}
        private string[] GetTextFromFile(string fileName)
        {
            string text = String.Empty;
            try
            {
                using (StreamReader sr = new StreamReader(fileName, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            string[] arr = text.Split(';');//gets array of strings from file
            return arr;
        }
        public void FindMin(string fileName)
        {
            string[] arr = GetTextFromFile(fileName);
            double number;
            double[] w = new double[arr.Length - 1];
            for (int i = 0; i < arr.Length - 1; i++)
            {
                Double.TryParse(arr[i], out number);
                w[i] = number;
            }

            Console.WriteLine(w.Min());
        }
        public override string ToString()
        {
            string str = String.Empty;
            for (int i = 0; i < ListOfTemperatures.Count; i++)
            {
                str += ListOfTemperatures[i].ToString() + "   ";
            }
            str += "\n";
            return str;
        }
    }
}
