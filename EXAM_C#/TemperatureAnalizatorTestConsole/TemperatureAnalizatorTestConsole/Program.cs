﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemperatureAnalizatorTestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileNameInt =@" D:\C#\EXAM_C#\TemperatureAnalizatorTestConsole\TemperatureAnalizatorTestConsole\inputInt.dat";
            TemperatureAnalizator<int> test1 = GetTemperaturesInt(fileNameInt);
            Console.WriteLine(test1);
            Console.WriteLine("FindValue(10) "+test1.FindValue(10));
            Console.WriteLine("FindValue(9) "+test1.FindValue(9));
            Console.WriteLine("Find max :");
            test1.FindMax(fileNameInt);
            Console.WriteLine("Find min :");
            test1.FindMin(fileNameInt);
            Console.WriteLine("Find the most repetitive element: ");
            test1.FindMostRepetitiveElement(fileNameInt);
            

            Console.WriteLine("=======================================================================");
            string fileNameDouble = @"D:\C#\EXAM_C#\TemperatureAnalizatorTestConsole\TemperatureAnalizatorTestConsole\Input.dat";
            TemperatureAnalizator<double> test2 = GetTemperaturesDouble(fileNameDouble);
            Console.WriteLine(test2);
            Console.WriteLine("FindValue(12.05) " + test2.FindValue(12.05));
            Console.WriteLine("FindValue(122.05) "+test2.FindValue(122.05));
            Console.WriteLine("Find max :");
            test2.FindMax(fileNameDouble);
            Console.WriteLine("Find min :");
            test2.FindMin(fileNameDouble);
            Console.WriteLine("Find the most repetitive element: ");
            test2.FindMostRepetitiveElement(fileNameDouble);

            Console.WriteLine("=======================================================================");
            string fileName = @"D:\C#\EXAM_C#\TemperatureAnalizatorTestConsole\TemperatureAnalizatorTestConsole\inputString.dat";
            TemperatureAnalizator<string> test3 = GetTemperaturesString(fileName);
            Console.WriteLine(test3);
            Console.WriteLine("FindValue('12-00') "+test3.FindValue("12-00"));
            Console.WriteLine("FindValue('99-00') "+test3.FindValue("99-00"));
            Console.WriteLine("Find max :");
            test3.FindMax(fileName);
            Console.WriteLine("Find min :");
            test3.FindMin(fileName);
            Console.WriteLine("Find the most repetitive element: ");
            test3.FindMostRepetitiveElement(fileName);

            

        }
        
        public static TemperatureAnalizator<double> GetTemperaturesDouble(string Filename)
        {
            string[] arr = GetTextFromFile(Filename);
            TemperatureAnalizator<double> x = new TemperatureAnalizator<double>();
            double number1;
            for (int i = 0; i < arr.Length - 1; i++)
            {
                Double.TryParse(arr[i], out number1);
                x.Add(number1);
            }
            return x;
        }

        public static TemperatureAnalizator<int> GetTemperaturesInt(string Filename)
        {
            string[] arr = GetTextFromFile(Filename);
            TemperatureAnalizator<int> x = new TemperatureAnalizator<int>();
            int number;
            for (int i = 0; i < arr.Length - 1; i++)
            {
                int.TryParse(arr[i], out number);
                x.Add(number);
            }
            return x;
        }

        public static TemperatureAnalizator<string> GetTemperaturesString(string Filename)
        {
            string[] arr = GetTextFromFile(Filename);
            TemperatureAnalizator<string> x = new TemperatureAnalizator<string>();
            for (int i = 0; i < arr.Length - 1; i++)
            {
                x.Add(arr[i]);
            }
            return x;
        }

        private static string[] GetTextFromFile(string Filename)
        {
            string text = String.Empty;
            try
            {
                using (StreamReader sr = new StreamReader(Filename, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            string[] arr = text.Split(';');//gets array of strings from file
            return arr;
        }
    }
}


