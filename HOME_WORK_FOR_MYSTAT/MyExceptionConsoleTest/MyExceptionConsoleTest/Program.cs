﻿using System;

using System.Collections;
using System.IO;
using MyExceptionConsoleTest.Exceptions;
using ExcptionsInheritance.Exceptions;

namespace MyExceptionConsoleTest
{
    class Program
    {
        /// <summary>
        /// Заполняет массив значениями из файла
        /// </summary>
        /// <param name="fileName">полное имя файл (включая путь к нему), который содержит данные для массива</param>
        /// <returns>Возвращает созданный и заполненный массив</returns>
        /// <exception cref="System.IO.FileNotFoundException">В случае если файл с заданным именем не найден</exception>
        /// <exception cref="System.ArrayTypeMismatchException">В случае, если файл содержит данные, которые не могут быть перобразованы к Double</exception>
        /// <exception cref="System.ArgumentNullException">В случае, если в качестве fileName пришел null (ДЗ)</exception>
        /// <exception cref="System.Exception">В остальных случаях (ДЗ)</exception>
        public static double[] getArrayFromFile(string fileName)
        {

            if (String.IsNullOrEmpty(fileName))
            {
                throw new MyException("String is null or empty!");
            }
            else
            {
                ArrayList array = new ArrayList();
                try
                {
                    StreamReader reader = new StreamReader(fileName);
                    string line = String.Empty;
                    while ((line = reader.ReadLine()) != null)
                    {
                        array.Add(line); // Add to list.
                    }
                }
                catch (System.IO.FileNotFoundException ex)
                {
                    Console.WriteLine(String.Format("Файл {0} не найден. Подробности об иcключении : {1}", ex.FileName, ex.Message));
                    return new double[0];
                }
                double[] d = new double[array.Count];
                int i =0;
                foreach(object item in array)
                {
                    try
                    {
                        d[i++] = Convert.ToDouble((string)item);
                    }
                    catch(System.FormatException ex)
                    {
                        Console.WriteLine(String.Format("Преобразование значения {0} к типу Double не выполнено. Подробности об иcключении : {1}", item.ToString(), ex.Message));
                        return new double[0];
                    }
                    catch(System.ArrayTypeMismatchException ex)
                    {
                        Console.WriteLine(String.Format("Преобразование типа {0} к типу Double не выполнено. Подробности об иcключении : {1}", item.GetType(), ex.Message));
                        return new double[0];
                        
                    }
                }
                return d;
            }
        }
        public class CalendarEntry
        {
            private DateTime date;
            public string day;
            public DateTime Date
            {
                get
                {
                    return date;
                }
                set
                {
                    // Set some reasonable boundaries for likely birth dates.
                    if (value.Year > 1900 && value.Year <= DateTime.Today.Year)
                    {
                        date = value;
                    }
                    else
                        throw new ArgumentOutOfRangeException();
                }

            }
            public void SetDate(string dateString)
            {
                DateTime dt = Convert.ToDateTime(dateString);
                if (dt.Year > 1900 && dt.Year <= DateTime.Today.Year)
                {
                    date = dt;
                }
                else //использование собственного класса исключения - наследника НЕнаследника Exception
                    throw new MyCalendarEntyYearOutOfRangeException("SetDate", String.Format("Устанавливаемое значение {0} для года (Year) не принадлежит допустимому интервалу от 1900 до {1}", dt.Year, DateTime.Today.Year));
            }

            public TimeSpan GetTimeSpan(string dateString)
            {
                DateTime dt = Convert.ToDateTime(dateString);

                if (dt != null && dt.Ticks < date.Ticks)
                {
                    return date - dt;
                }
                else
                    throw new MyException("Argument out of range");
            }
        }
        
        static void Main(string[] args)
        {
            string filename = "";
            double[] arrDouble = getArrayFromFile(filename);
            Console.ReadKey(true);
        }
    }


    abstract class MyAbstractClass
    {
        protected int x;
        public virtual double Area(){return 0;}
        public MyAbstractClass() { }
    }
}
