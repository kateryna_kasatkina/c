﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcptionsInheritance.Exceptions
{
    public class MyArgumentNullException : ArgumentNullException
    {
        public MyArgumentNullException() { }
        public MyArgumentNullException(string message) : base(message) { }
        public override string ToString()
        {
            return Message;
        }

    }
}
