﻿namespace HomeWorkExeption
{
    partial class MyForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.A = new System.Windows.Forms.Label();
            this.B = new System.Windows.Forms.Label();
            this.GetResult = new System.Windows.Forms.Button();
            this.tbA = new System.Windows.Forms.TextBox();
            this.tbB = new System.Windows.Forms.TextBox();
            this.tbR = new System.Windows.Forms.TextBox();
            this.mod = new System.Windows.Forms.RadioButton();
            this.multiplication = new System.Windows.Forms.RadioButton();
            this.division = new System.Windows.Forms.RadioButton();
            this.subtraction = new System.Windows.Forms.RadioButton();
            this.addition = new System.Windows.Forms.RadioButton();
            this.operationPanel = new System.Windows.Forms.Panel();
            this.errorMessage = new System.Windows.Forms.Label();
            this.operationPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // A
            // 
            this.A.AutoSize = true;
            this.A.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.A.Location = new System.Drawing.Point(146, 27);
            this.A.Name = "A";
            this.A.Size = new System.Drawing.Size(49, 32);
            this.A.TabIndex = 0;
            this.A.Text = "a=";
            // 
            // B
            // 
            this.B.AutoSize = true;
            this.B.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.B.Location = new System.Drawing.Point(520, 27);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(49, 32);
            this.B.TabIndex = 1;
            this.B.Text = "b=";
            // 
            // GetResult
            // 
            this.GetResult.Location = new System.Drawing.Point(650, 75);
            this.GetResult.Name = "GetResult";
            this.GetResult.Size = new System.Drawing.Size(75, 23);
            this.GetResult.TabIndex = 7;
            this.GetResult.Text = "=";
            this.GetResult.UseVisualStyleBackColor = true;
            this.GetResult.Click += new System.EventHandler(this.GetResult_Click);
            // 
            // tbA
            // 
            this.tbA.Location = new System.Drawing.Point(88, 74);
            this.tbA.Name = "tbA";
            this.tbA.Size = new System.Drawing.Size(139, 22);
            this.tbA.TabIndex = 13;
            // 
            // tbB
            // 
            this.tbB.Location = new System.Drawing.Point(486, 74);
            this.tbB.Name = "tbB";
            this.tbB.Size = new System.Drawing.Size(132, 22);
            this.tbB.TabIndex = 14;
            // 
            // tbR
            // 
            this.tbR.Location = new System.Drawing.Point(789, 75);
            this.tbR.Name = "tbR";
            this.tbR.Size = new System.Drawing.Size(100, 22);
            this.tbR.TabIndex = 15;
            // 
            // mod
            // 
            this.mod.AutoSize = true;
            this.mod.Location = new System.Drawing.Point(20, 124);
            this.mod.Name = "mod";
            this.mod.Size = new System.Drawing.Size(56, 21);
            this.mod.TabIndex = 12;
            this.mod.Text = "mod";
            this.mod.UseVisualStyleBackColor = true;
            // 
            // multiplication
            // 
            this.multiplication.AutoSize = true;
            this.multiplication.Location = new System.Drawing.Point(20, 97);
            this.multiplication.Name = "multiplication";
            this.multiplication.Size = new System.Drawing.Size(34, 21);
            this.multiplication.TabIndex = 11;
            this.multiplication.Text = "*";
            this.multiplication.UseVisualStyleBackColor = true;
            // 
            // division
            // 
            this.division.AutoSize = true;
            this.division.Location = new System.Drawing.Point(20, 70);
            this.division.Name = "division";
            this.division.Size = new System.Drawing.Size(33, 21);
            this.division.TabIndex = 10;
            this.division.Text = "/";
            this.division.UseVisualStyleBackColor = true;
            // 
            // subtraction
            // 
            this.subtraction.AutoSize = true;
            this.subtraction.Location = new System.Drawing.Point(20, 43);
            this.subtraction.Name = "subtraction";
            this.subtraction.Size = new System.Drawing.Size(34, 21);
            this.subtraction.TabIndex = 9;
            this.subtraction.Text = "-";
            this.subtraction.UseVisualStyleBackColor = true;
            // 
            // addition
            // 
            this.addition.AutoSize = true;
            this.addition.Location = new System.Drawing.Point(20, 16);
            this.addition.Name = "addition";
            this.addition.Size = new System.Drawing.Size(37, 21);
            this.addition.TabIndex = 8;
            this.addition.Text = "+";
            this.addition.UseVisualStyleBackColor = true;
            // 
            // operationPanel
            // 
            this.operationPanel.Controls.Add(this.addition);
            this.operationPanel.Controls.Add(this.subtraction);
            this.operationPanel.Controls.Add(this.division);
            this.operationPanel.Controls.Add(this.multiplication);
            this.operationPanel.Controls.Add(this.mod);
            this.operationPanel.Location = new System.Drawing.Point(298, 12);
            this.operationPanel.Name = "operationPanel";
            this.operationPanel.Size = new System.Drawing.Size(110, 158);
            this.operationPanel.TabIndex = 16;
            // 
            // errorMessage
            // 
            this.errorMessage.AutoSize = true;
            this.errorMessage.ForeColor = System.Drawing.Color.Red;
            this.errorMessage.Location = new System.Drawing.Point(88, 217);
            this.errorMessage.Name = "errorMessage";
            this.errorMessage.Size = new System.Drawing.Size(0, 17);
            this.errorMessage.TabIndex = 17;
            // 
            // MyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 440);
            this.Controls.Add(this.errorMessage);
            this.Controls.Add(this.operationPanel);
            this.Controls.Add(this.tbR);
            this.Controls.Add(this.tbB);
            this.Controls.Add(this.tbA);
            this.Controls.Add(this.GetResult);
            this.Controls.Add(this.B);
            this.Controls.Add(this.A);
            this.Name = "MyForm";
            this.Text = "Действия над ЦЕЛЫМИ числами";
            this.operationPanel.ResumeLayout(false);
            this.operationPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label A;
        private System.Windows.Forms.Label B;
        private System.Windows.Forms.Button GetResult;
        private System.Windows.Forms.TextBox tbA;
        private System.Windows.Forms.TextBox tbB;
        private System.Windows.Forms.TextBox tbR;
        private System.Windows.Forms.RadioButton mod;
        private System.Windows.Forms.RadioButton multiplication;
        private System.Windows.Forms.RadioButton division;
        private System.Windows.Forms.RadioButton subtraction;
        private System.Windows.Forms.RadioButton addition;
        private System.Windows.Forms.Panel operationPanel;
        private System.Windows.Forms.Label errorMessage;
    }
}

