﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcptionsInheritance
{
    public class MyExeption : Exception
    {
        public MyExeption() { }
        public MyExeption(string message) : base(message) { }
        public override string ToString()
        {
            return Message;
        }

    }
}
