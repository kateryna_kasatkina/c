﻿using ExcptionsInheritance;
using System;
using System.Windows.Forms;

namespace HomeWorkExeption
{
    public partial class MyForm : Form
    {
        public MyForm()
        {
            InitializeComponent();
        }
        private void GetResult_Click(object sender, EventArgs e)
        {
            // Catch all possible exception and print proper message to the user
            try
            {
                var a = tbA.Text;
                var b = tbB.Text;

                //A and B must be populated
                if (string.IsNullOrEmpty(a))
                    throw new MyExeption("Invalid number A");
                if (string.IsNullOrEmpty(b))
                    throw new MyExeption("Invalid number B");

                int aInt, bInt;
                // A and B must be integers
                if (!Int32.TryParse(a, out aInt))
                    throw new MyExeption("A is not Integer");
                if (!Int32.TryParse(b, out bInt))
                    throw new MyExeption("B is not Integer");

                int result = 0;
                if (addition.Checked == true)
                {
                    result = aInt + bInt;
                }
                else if (subtraction.Checked == true)
                {
                    result = aInt - bInt;
                }
                else if (division.Checked == true)
                {
                    // Division is not possible
                    if (bInt == 0)
                        throw new MyExeption("Divide by zero  !");

                    result = aInt / bInt;
                }
                else if (multiplication.Checked == true)
                {
                    result = aInt * bInt;
                }
                else if (mod.Checked == true)
                {
                    result = aInt % bInt;
                }
                else
                {
                    // Operation must be selected
                    throw new MyExeption("Operation was not selected");
                }

                // Print result
                tbR.Text = result.ToString();
                // Clean the previous error messages if success
                errorMessage.Text = ""; 
            }
            catch (MyExeption exc)
            {
                errorMessage.Text = exc.Message;
            }
            catch (Exception ex)
            {
                errorMessage.Text = ex.Message;
            }
        }
    }
}
