﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Person x = new Person();
            Person[] persons = new Person[2];
            persons[0] = new Person();
            persons[1] = new Person();
          //  persons[2] = new Person();

            foreach (Person p in persons)
            {
             p.Dispose();//public void Dispose();
            }

            using (Person pers = new Person())
            {
                //автоматический вызов , реализация интерфейса IDisposable
            }
            GC.Collect(2);
            GC.WaitForPendingFinalizers();
            GC.Collect(2);
            GC.GetTotalMemory(true);

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
    #region Версия когда мы не используем   GC.WaitForPendingFinalizers();
    public class Person : IDisposable
    {
        public void Dispose()
        {
            //закрытия соеденений (к файлу к базе данных)
            //соеденения созданные при помощи обращения к неуправляемому коду
            Print(ConsoleColor.Red,"Dispose");
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        void IDisposable.Dispose()
        {
            Print(ConsoleColor.Blue, "Dispose");
            Console.Beep();//автоматически вызываемый метод
        }
        protected void Print(ConsoleColor color,string message)//Вызывается в using
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ResetColor();
        }


        ~Person()//Финализация передается компилятором в Finalization ("Деструктор")
            //вызывается в GC в момент очищения
        {
            Print(ConsoleColor.Green, "~Person()");
            Console.Beep();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if(disposing)
                {

                }
                //освобождаем неуправляемые обьекты-методы из DLL обвертки
                Console.Beep();
                disposed = true;
               
            }
        }
    }
    #endregion
}
